import SubjectTimeTrackingBase from './base/SubjectTimeTrackingBase';

/**
 * @class SubjectTimeTracking
 * @extends SubjectTimeTrackingBase
 */
class SubjectTimeTracking extends SubjectTimeTrackingBase {

}

export default SubjectTimeTracking;
