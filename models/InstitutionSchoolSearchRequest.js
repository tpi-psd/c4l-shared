import InstitutionSchoolSearchRequestBase from './base/InstitutionSchoolSearchRequestBase';

/**
 * @class InstitutionSchoolSearchRequest
 * @extends InstitutionSchoolSearchRequestBase
 */
class InstitutionSchoolSearchRequest extends InstitutionSchoolSearchRequestBase {

}

export default InstitutionSchoolSearchRequest;
