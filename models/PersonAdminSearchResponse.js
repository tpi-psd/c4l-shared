import PersonAdminSearchResponseBase from './base/PersonAdminSearchResponseBase';

/**
 * @class PersonAdminSearchResponse
 * @extends PersonAdminSearchResponseBase
 */
class PersonAdminSearchResponse extends PersonAdminSearchResponseBase {

}

export default PersonAdminSearchResponse;
