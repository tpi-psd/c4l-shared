import PersonLoginRequestBase from './base/PersonLoginRequestBase';

/**
 * @class PersonLoginRequest
 * @extends PersonLoginRequestBase
 */
class PersonLoginRequest extends PersonLoginRequestBase {

}

export default PersonLoginRequest;
