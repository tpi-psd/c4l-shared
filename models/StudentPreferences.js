import StudentPreferencesBase from './base/StudentPreferencesBase';

/**
 * @class StudentPreferences
 * @extends StudentPreferencesBase
 */
class StudentPreferences extends StudentPreferencesBase {

}

export default StudentPreferences;
