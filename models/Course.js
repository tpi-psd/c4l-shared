import CourseBase from './base/CourseBase';

/**
 * @class Course
 * @extends CourseBase
 */
class Course extends CourseBase {

}

export default Course;
