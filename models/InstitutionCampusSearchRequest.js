import InstitutionCampusSearchRequestBase from './base/InstitutionCampusSearchRequestBase';

/**
 * @class InstitutionCampusSearchRequest
 * @extends InstitutionCampusSearchRequestBase
 */
class InstitutionCampusSearchRequest extends InstitutionCampusSearchRequestBase {

}

export default InstitutionCampusSearchRequest;
