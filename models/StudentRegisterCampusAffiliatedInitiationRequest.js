import StudentRegisterCampusAffiliatedInitiationRequestBase from './base/StudentRegisterCampusAffiliatedInitiationRequestBase';

/**
 * @class StudentRegisterCampusAffiliatedInitiationRequest
 * @extends StudentRegisterCampusAffiliatedInitiationRequestBase
 */
class StudentRegisterCampusAffiliatedInitiationRequest extends StudentRegisterCampusAffiliatedInitiationRequestBase {

}

export default StudentRegisterCampusAffiliatedInitiationRequest;
