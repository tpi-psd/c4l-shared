import CourseProgressDetailBase from './base/CourseProgressDetailBase';

/**
 * @class CourseProgressDetail
 * @extends CourseProgressDetailBase
 */
class CourseProgressDetail extends CourseProgressDetailBase {

}

export default CourseProgressDetail;
