import CourseProgressBase from './base/CourseProgressBase';
import CourseProgressStatueEnum from './enum/CourseProgressStatusEnum';
/**
 * @class CourseProgress
 * @extends CourseProgressBase
 */
class CourseProgress extends CourseProgressBase {
	isPassed() {
		return [
			CourseProgressStatueEnum.PASSED,
			CourseProgressStatueEnum.PASSED_VIA_CHALLENGE_EXAM,
			CourseProgressStatueEnum.PASSED_VIA_TRANSFER
		].includes(this.status);
	}

	isComplete() {
		return [
			CourseProgressStatueEnum.PASSED,
			CourseProgressStatueEnum.PASSED_VIA_CHALLENGE_EXAM,
			CourseProgressStatueEnum.PASSED_VIA_TRANSFER,
			CourseProgressStatueEnum.FAILED
		].includes(this.status);
	}
}

export default CourseProgress;
