const NoteNoteTypeEnum = Object.freeze({
	ENTITY_CREATED: 'EntityCreated',
	ENTITY_MODIFIED: 'EntityModified',
	NOTE: 'Note',
});

export default NoteNoteTypeEnum;
