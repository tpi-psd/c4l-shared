const PersonAdminSearchRequestFilterByRolesEnum = Object.freeze({
	C4_L_MANAGER: 'C4LManager',
	C4_L_STAFF: 'C4LStaff',
	INSTITUTION_MANAGER: 'InstitutionManager',
	INSTITUTION_STAFF: 'InstitutionStaff',
});

export default PersonAdminSearchRequestFilterByRolesEnum;
