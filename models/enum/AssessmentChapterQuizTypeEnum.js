const AssessmentChapterQuizTypeEnum = Object.freeze({
	NONE: 'None',
	STANDARD: 'Standard',
	VIDEO: 'Video',
	WRITTEN: 'Written',
	LEARNING_INVENTORY_STUDENT_TYPE: 'LearningInventoryStudentType',
	LEARNING_INVENTORY_LEARNING_STYLE: 'LearningInventoryLearningStyle',
	COMMUNICATION_STYLE: 'CommunicationStyle',
	CAREER_INVENTORY_INTEREST_PROFILER: 'CareerInventoryInterestProfiler',
	CAREER_INVENTORY_JOB_ZONE: 'CareerInventoryJobZone',
});

export default AssessmentChapterQuizTypeEnum;
