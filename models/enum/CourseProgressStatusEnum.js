const CourseProgressStatusEnum = Object.freeze({
	NOT_YET_STARTED: 'NotYetStarted',
	IN_PROGRESS: 'InProgress',
	PENDING_UNLOCKED_EXAM: 'PendingUnlockedExam',
	GRADE_PENDING: 'GradePending',
	PASSED_VIA_TRANSFER: 'PassedViaTransfer',
	PASSED_VIA_CHALLENGE_EXAM: 'PassedViaChallengeExam',
	PASSED: 'Passed',
	FAILED: 'Failed',
});

export default CourseProgressStatusEnum;
