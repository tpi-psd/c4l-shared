const TranscriptRequestSearchRequestFilterByStatusEnum = Object.freeze({
	PENDING: 'Pending',
	PRINTED: 'Printed',
	SENT: 'Sent',
	NOT_SENT: 'NotSent',
});

export default TranscriptRequestSearchRequestFilterByStatusEnum;
