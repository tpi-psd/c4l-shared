const IssueEditRequestStatusEnum = Object.freeze({
	OPEN: 'Open',
	RESOLVED: 'Resolved',
	CLOSED: 'Closed',
});

export default IssueEditRequestStatusEnum;
