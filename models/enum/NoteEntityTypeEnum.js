const NoteEntityTypeEnum = Object.freeze({
	PERSON: 'Person',
	SUBJECT: 'Subject',
	COURSE: 'Course',
	QUESTION: 'Question',
	ANNOUNCEMENT: 'Announcement',
	ISSUE: 'Issue',
});

export default NoteEntityTypeEnum;
