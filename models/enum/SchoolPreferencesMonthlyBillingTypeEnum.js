const SchoolPreferencesMonthlyBillingTypeEnum = Object.freeze({
	FULL: 'Full',
	HALF: 'Half',
});

export default SchoolPreferencesMonthlyBillingTypeEnum;
