const IssueSearchRequestFilterByStatusEnum = Object.freeze({
	OPEN: 'Open',
	RESOLVED: 'Resolved',
	CLOSED: 'Closed',
	NOT_CLOSED: 'NotClosed',
});

export default IssueSearchRequestFilterByStatusEnum;
