const TranscriptRequestStatusEnum = Object.freeze({
	PAYMENT_FAILED: 'PaymentFailed',
	PENDING: 'Pending',
	PRINTED: 'Printed',
	SENT: 'Sent',
});

export default TranscriptRequestStatusEnum;
