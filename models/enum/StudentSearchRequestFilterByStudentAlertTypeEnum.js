const StudentSearchRequestFilterByStudentAlertTypeEnum = Object.freeze({
	NEVER_LOGGED_IN: 'NeverLoggedIn',
	STUDENT_OF_INTEREST: 'StudentOfInterest',
	HAS_OPEN_ISSUE: 'HasOpenIssue',
	HAS_PENDING_TRANSCRIPT: 'HasPendingTranscript',
	INACTIVE7_DAYS: 'Inactive7Days',
});

export default StudentSearchRequestFilterByStudentAlertTypeEnum;
