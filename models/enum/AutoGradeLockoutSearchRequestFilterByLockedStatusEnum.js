const AutoGradeLockoutSearchRequestFilterByLockedStatusEnum = Object.freeze({
	LOCKED: 'Locked',
	UNLOCKED: 'Unlocked',
	RELOCKED: 'Relocked',
});

export default AutoGradeLockoutSearchRequestFilterByLockedStatusEnum;
