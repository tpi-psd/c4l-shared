const CourseAssessmentTypeEnum = Object.freeze({
	NONE: 'None',
	STANDARD: 'Standard',
	LEARNING_INVENTORY_RESULTS: 'LearningInventoryResults',
	CAREER_INVENTORY_RESULTS: 'CareerInventoryResults',
});

export default CourseAssessmentTypeEnum;
