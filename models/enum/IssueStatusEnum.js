const IssueStatusEnum = Object.freeze({
	OPEN: 'Open',
	RESOLVED: 'Resolved',
	CLOSED: 'Closed',
});

export default IssueStatusEnum;
