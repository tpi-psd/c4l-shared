const StudentSearchRequestFilterByStatusEnum = Object.freeze({
	PENDING: 'Pending',
	ENROLLED: 'Enrolled',
	ACTIVE: 'Active',
	GRADUATED: 'Graduated',
	CANCELLED: 'Cancelled',
	EXPIRED: 'Expired',
});

export default StudentSearchRequestFilterByStatusEnum;
