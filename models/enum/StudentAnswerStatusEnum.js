const StudentAnswerStatusEnum = Object.freeze({
	CORRECT: 'Correct',
	INCORRECT: 'Incorrect',
	TIME_EXPIRED: 'TimeExpired',
	UNANSWERED: 'Unanswered',
});

export default StudentAnswerStatusEnum;
