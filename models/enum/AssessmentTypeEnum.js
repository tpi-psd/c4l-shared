const AssessmentTypeEnum = Object.freeze({
	QUIZ: 'Quiz',
	CHALLENGE_EXAM: 'ChallengeExam',
	FINAL_EXAM: 'FinalExam',
});

export default AssessmentTypeEnum;
