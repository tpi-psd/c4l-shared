const StudentAlertsEnum = Object.freeze({
	STUDENT_OF_INTEREST: 'StudentOfInterest',
	HAS_OPEN_ISSUE: 'HasOpenIssue',
	HAS_PENDING_TRANSCRIPT: 'HasPendingTranscript',
});

export default StudentAlertsEnum;
