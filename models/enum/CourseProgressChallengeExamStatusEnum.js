const CourseProgressChallengeExamStatusEnum = Object.freeze({
	AVAILABLE: 'Available',
	UNAVAILABLE: 'Unavailable',
	PASSED: 'Passed',
	FAILED: 'Failed',
});

export default CourseProgressChallengeExamStatusEnum;
