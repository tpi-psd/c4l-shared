const StudentGenderEnum = Object.freeze({
	MALE: 'Male',
	FEMALE: 'Female',
	NONBINARY: 'Nonbinary',
	PREFER_NOT_TO_DISCLOSE: 'PreferNotToDisclose',
});

export default StudentGenderEnum;
