const FileAssetFileAssetTypeEnum = Object.freeze({
	NOTE: 'Note',
	TRANSFER_TRANSCRIPT: 'TransferTranscript',
	FINAL_OFFICIAL_TRANSCRIPT: 'FinalOfficialTranscript',
	SECTION_IMAGE: 'SectionImage',
	QUESTION_IMAGE: 'QuestionImage',
});

export default FileAssetFileAssetTypeEnum;
