const StudentPreferencesMaritalStatusEnum = Object.freeze({
	SINGLE: 'Single',
	MARRIED: 'Married',
	DIVORCED: 'Divorced',
	WIDOWED: 'Widowed',
});

export default StudentPreferencesMaritalStatusEnum;
