const StudentSearchAssessmentRequestFilterByTypeArrayEnum = Object.freeze({
	QUIZ: 'Quiz',
	CHALLENGE_EXAM: 'ChallengeExam',
	FINAL_EXAM: 'FinalExam',
});

export default StudentSearchAssessmentRequestFilterByTypeArrayEnum;
