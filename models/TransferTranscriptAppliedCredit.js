import TransferTranscriptAppliedCreditBase from './base/TransferTranscriptAppliedCreditBase';

/**
 * @class TransferTranscriptAppliedCredit
 * @extends TransferTranscriptAppliedCreditBase
 */
class TransferTranscriptAppliedCredit extends TransferTranscriptAppliedCreditBase {

}

export default TransferTranscriptAppliedCredit;
