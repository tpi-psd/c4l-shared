import PaymentRequestBase from './base/PaymentRequestBase';

/**
 * @class PaymentRequest
 * @extends PaymentRequestBase
 */
class PaymentRequest extends PaymentRequestBase {

}

export default PaymentRequest;
