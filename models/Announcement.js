import AnnouncementBase from './base/AnnouncementBase';

/**
 * @class Announcement
 * @extends AnnouncementBase
 */
class Announcement extends AnnouncementBase {

}

export default Announcement;
