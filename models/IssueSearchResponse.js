import IssueSearchResponseBase from './base/IssueSearchResponseBase';

/**
 * @class IssueSearchResponse
 * @extends IssueSearchResponseBase
 */
class IssueSearchResponse extends IssueSearchResponseBase {

}

export default IssueSearchResponse;
