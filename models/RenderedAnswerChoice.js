import RenderedAnswerChoiceBase from './base/RenderedAnswerChoiceBase';

/**
 * @class RenderedAnswerChoice
 * @extends RenderedAnswerChoiceBase
 */
class RenderedAnswerChoice extends RenderedAnswerChoiceBase {

}

export default RenderedAnswerChoice;
