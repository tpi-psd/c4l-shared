import TransferTranscriptBase from './base/TransferTranscriptBase';

/**
 * @class TransferTranscript
 * @extends TransferTranscriptBase
 */
class TransferTranscript extends TransferTranscriptBase {

}

export default TransferTranscript;
