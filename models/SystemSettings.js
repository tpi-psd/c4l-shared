import { arrayToOject } from '../Utilities';
import SystemSettingsBase from './base/SystemSettingsBase';

/**
 * @class SystemSettings
 * @extends SystemSettingsBase
 */
class SystemSettings extends SystemSettingsBase {
	/**
	 * Get subject and course objects by course Id
	 * @param {number} courseId
	 * @returns {subject, course}
	 */
	getSubjectCourseByCourseId(courseId) {
		const result = { subject: null, course: null };
		this.subjects?.forEach(subject => {
			const courseObj = arrayToOject(subject.courses, 'id');
			if (courseObj[courseId]) {
				result.subject = subject;
				result.course = courseObj[courseId];
			}
		});
		return result;
	}

	/**
	 * Gets an array of live schools
	 * @returns School[]
	 */
	getLiveSchools() {
		const result = [];
		this.schools?.forEach(school => {
			if (school.activeFlag) result.push(school);
		});
		return result;
	}
}

export default SystemSettings;
