import AnswerBase from './base/AnswerBase';

/**
 * @class Answer
 * @extends AnswerBase
 */
class Answer extends AnswerBase {

}

export default Answer;
