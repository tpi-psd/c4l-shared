import StudentSearchResponseBase from './base/StudentSearchResponseBase';

/**
 * @class StudentSearchResponse
 * @extends StudentSearchResponseBase
 */
class StudentSearchResponse extends StudentSearchResponseBase {

}

export default StudentSearchResponse;
