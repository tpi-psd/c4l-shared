import InstitutionSchoolSearchResponseBase from './base/InstitutionSchoolSearchResponseBase';

/**
 * @class InstitutionSchoolSearchResponse
 * @extends InstitutionSchoolSearchResponseBase
 */
class InstitutionSchoolSearchResponse extends InstitutionSchoolSearchResponseBase {

}

export default InstitutionSchoolSearchResponse;
