import TranscriptRequestBase from './base/TranscriptRequestBase';

/**
 * @class TranscriptRequest
 * @extends TranscriptRequestBase
 */
class TranscriptRequest extends TranscriptRequestBase {

}

export default TranscriptRequest;
