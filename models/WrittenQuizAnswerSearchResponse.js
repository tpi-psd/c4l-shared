import WrittenQuizAnswerSearchResponseBase from './base/WrittenQuizAnswerSearchResponseBase';

/**
 * @class WrittenQuizAnswerSearchResponse
 * @extends WrittenQuizAnswerSearchResponseBase
 */
class WrittenQuizAnswerSearchResponse extends WrittenQuizAnswerSearchResponseBase {

}

export default WrittenQuizAnswerSearchResponse;
