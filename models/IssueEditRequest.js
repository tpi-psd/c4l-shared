import IssueEditRequestBase from './base/IssueEditRequestBase';

/**
 * @class IssueEditRequest
 * @extends IssueEditRequestBase
 */
class IssueEditRequest extends IssueEditRequestBase {

}

export default IssueEditRequest;
