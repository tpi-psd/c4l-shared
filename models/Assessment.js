import AssessmentBase from './base/AssessmentBase';

/**
 * @class Assessment
 * @extends AssessmentBase
 */
class Assessment extends AssessmentBase {
	get scoreInPercentage() {
		// assessmentScore could be 0
		if (this.assessmentScore !== undefined) {
			return this.assessmentScore + '%';
		} else return 'Pending';
	}

	get isSpecialAssessmentPassed() {
		return (this.passFlag && !this.assessmentScore);
	}

	get isGraded() {
		return this.gradedByPerson ? true : false;
	}
}

export default Assessment;
