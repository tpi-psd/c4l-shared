import SchoolPreferencesBase from './base/SchoolPreferencesBase';

/**
 * @class SchoolPreferences
 * @extends SchoolPreferencesBase
 */
class SchoolPreferences extends SchoolPreferencesBase {

}

export default SchoolPreferences;
