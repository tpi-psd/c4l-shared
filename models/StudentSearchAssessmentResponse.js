import StudentSearchAssessmentResponseBase from './base/StudentSearchAssessmentResponseBase';

/**
 * @class StudentSearchAssessmentResponse
 * @extends StudentSearchAssessmentResponseBase
 */
class StudentSearchAssessmentResponse extends StudentSearchAssessmentResponseBase {

}

export default StudentSearchAssessmentResponse;
