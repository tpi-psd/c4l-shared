import PersonBase from './base/PersonBase';
import studentStatusEnum from './enum/StudentStatusEnum';

/**
 * @class Person
 * @extends PersonBase
 */
class Person extends PersonBase {
	isSubscriptionExpired() {
		return this.student?.status === studentStatusEnum.EXPIRED;
	}
	isSubscriptionCancelled() {
		return this.student?.status === studentStatusEnum.CANCELLED;
	}
	isPendingExpiration() {
		if (
			this.student?.isSchoolStudent() ||
			this.isSubscriptionCancelled() ||
			this.isSubscriptionExpired()
		)
			return false;
		return this.student?.currentSubscription?.dateCancelled !== null;
	}
	get subscriptionEndDate() {
		return this.student?.currentSubscription?.datePaidUntil || '';
	}
	get subscriptionCancelledDate() {
		return this.student?.currentSubscription?.dateCancelled || '';
	}
}

export default Person;
