import CurriculumQuestionSearchRequestBase from './base/CurriculumQuestionSearchRequestBase';

/**
 * @class CurriculumQuestionSearchRequest
 * @extends CurriculumQuestionSearchRequestBase
 */
class CurriculumQuestionSearchRequest extends CurriculumQuestionSearchRequestBase {

}

export default CurriculumQuestionSearchRequest;
