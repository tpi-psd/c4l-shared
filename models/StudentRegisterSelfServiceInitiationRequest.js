import StudentRegisterSelfServiceInitiationRequestBase from './base/StudentRegisterSelfServiceInitiationRequestBase';

/**
 * @class StudentRegisterSelfServiceInitiationRequest
 * @extends StudentRegisterSelfServiceInitiationRequestBase
 */
class StudentRegisterSelfServiceInitiationRequest extends StudentRegisterSelfServiceInitiationRequestBase {

}

export default StudentRegisterSelfServiceInitiationRequest;
