import StudentCourseProgressResponseBase from './base/StudentCourseProgressResponseBase';

/**
 * @class StudentCourseProgressResponse
 * @extends StudentCourseProgressResponseBase
 */
class StudentCourseProgressResponse extends StudentCourseProgressResponseBase {
  getCourseProgressById = (courseId) => {
    const courseProgressArray = this.courseProgressArray;
    
  if (courseProgressArray)
     return courseProgressArray.find((obj) => obj.courseId === courseId);
     return {}
  };
}

export default StudentCourseProgressResponse;
