import IssueSearchRequestBase from './base/IssueSearchRequestBase';

/**
 * @class IssueSearchRequest
 * @extends IssueSearchRequestBase
 */
class IssueSearchRequest extends IssueSearchRequestBase {

}

export default IssueSearchRequest;
