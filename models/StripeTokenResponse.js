import StripeTokenResponseBase from './base/StripeTokenResponseBase';

/**
 * @class StripeTokenResponse
 * @extends StripeTokenResponseBase
 */
class StripeTokenResponse extends StripeTokenResponseBase {

}

export default StripeTokenResponse;
