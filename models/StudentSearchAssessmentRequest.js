import StudentSearchAssessmentRequestBase from './base/StudentSearchAssessmentRequestBase';

/**
 * @class StudentSearchAssessmentRequest
 * @extends StudentSearchAssessmentRequestBase
 */
class StudentSearchAssessmentRequest extends StudentSearchAssessmentRequestBase {

}

export default StudentSearchAssessmentRequest;
