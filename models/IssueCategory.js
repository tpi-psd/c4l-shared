import IssueCategoryBase from './base/IssueCategoryBase';

/**
 * @class IssueCategory
 * @extends IssueCategoryBase
 */
class IssueCategory extends IssueCategoryBase {

}

export default IssueCategory;
