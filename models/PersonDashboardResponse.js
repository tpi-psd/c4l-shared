import PersonDashboardResponseBase from './base/PersonDashboardResponseBase';

/**
 * @class PersonDashboardResponse
 * @extends PersonDashboardResponseBase
 */
class PersonDashboardResponse extends PersonDashboardResponseBase {

}

export default PersonDashboardResponse;
