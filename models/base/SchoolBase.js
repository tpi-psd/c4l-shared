import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import School from "../School";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class SchoolBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {string} name
 * @property {string} address1
 * @property {string} address2
 * @property {string} city
 * @property {string} state
 * @property {string} zip
 * @property {string} phone
 * @property {number} userCount (integer)
 * @property {number} campusCount (integer)
 * @property {number} studentActiveCount (integer)
 * @property {SchoolPreferences} preferences
 * @property {[Campus]} campuses
 * @property {boolean} activeFlag
 * @property {Date} dateCreated (date and time)
 */
class SchoolBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of School based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {School}
	 */
	static create(genericObject) {
		const newSchool = new School();
		newSchool.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newSchool;
	}

	/**
	 * Instantiates a new array of School based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[School]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newSchoolArray = [];
		genericArray.forEach(genericObject => {
			newSchoolArray.push(School.create(genericObject));
		});
		return newSchoolArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('name', 'string'),
	ModelBaseClass.createModelProperty('address1', 'string'),
	ModelBaseClass.createModelProperty('address2', 'string'),
	ModelBaseClass.createModelProperty('city', 'string'),
	ModelBaseClass.createModelProperty('state', 'string'),
	ModelBaseClass.createModelProperty('zip', 'string'),
	ModelBaseClass.createModelProperty('phone', 'string'),
	ModelBaseClass.createModelProperty('userCount', 'integer'),
	ModelBaseClass.createModelProperty('campusCount', 'integer'),
	ModelBaseClass.createModelProperty('studentActiveCount', 'integer'),
	ModelBaseClass.createModelProperty('preferences', 'SchoolPreferences'),
	ModelBaseClass.createModelProperty('campuses', '[Campus]'),
	ModelBaseClass.createModelProperty('activeFlag', 'boolean'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
];

export default SchoolBase;
