import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import InstitutionSchoolSearchRequest from "../InstitutionSchoolSearchRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class InstitutionSchoolSearchRequestBase
 * @extends ModelBaseClass
 * @property {boolean} includeInactiveFlag
 * @property {ResultParameter} resultParameter [Id, Name, CampusCount, UserCount, StudentActiveCount, DateCreated]
 */
class InstitutionSchoolSearchRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of InstitutionSchoolSearchRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {InstitutionSchoolSearchRequest}
	 */
	static create(genericObject) {
		const newInstitutionSchoolSearchRequest = new InstitutionSchoolSearchRequest();
		newInstitutionSchoolSearchRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newInstitutionSchoolSearchRequest;
	}

	/**
	 * Instantiates a new array of InstitutionSchoolSearchRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[InstitutionSchoolSearchRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newInstitutionSchoolSearchRequestArray = [];
		genericArray.forEach(genericObject => {
			newInstitutionSchoolSearchRequestArray.push(InstitutionSchoolSearchRequest.create(genericObject));
		});
		return newInstitutionSchoolSearchRequestArray;
	}
}

/**
 * @type {string} OrderById
 */
InstitutionSchoolSearchRequestBase.OrderById = 'id';

/**
 * @type {string} OrderByName
 */
InstitutionSchoolSearchRequestBase.OrderByName = 'name';

/**
 * @type {string} OrderByCampusCount
 */
InstitutionSchoolSearchRequestBase.OrderByCampusCount = 'campuscount';

/**
 * @type {string} OrderByUserCount
 */
InstitutionSchoolSearchRequestBase.OrderByUserCount = 'usercount';

/**
 * @type {string} OrderByStudentActiveCount
 */
InstitutionSchoolSearchRequestBase.OrderByStudentActiveCount = 'studentactivecount';

/**
 * @type {string} OrderByDateCreated
 */
InstitutionSchoolSearchRequestBase.OrderByDateCreated = 'datecreated';

const _modelDefinition = [
	ModelBaseClass.createModelProperty('includeInactiveFlag', 'boolean'),
	ModelBaseClass.createModelProperty('resultParameter', 'ResultParameter'),
];

export default InstitutionSchoolSearchRequestBase;
