import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentRegisterCampusAffiliatedCompletionRequest from "../StudentRegisterCampusAffiliatedCompletionRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentRegisterCampusAffiliatedCompletionRequestBase
 * @extends ModelBaseClass
 * @property {string} password
 * @property {Person} person
 */
class StudentRegisterCampusAffiliatedCompletionRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentRegisterCampusAffiliatedCompletionRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentRegisterCampusAffiliatedCompletionRequest}
	 */
	static create(genericObject) {
		const newStudentRegisterCampusAffiliatedCompletionRequest = new StudentRegisterCampusAffiliatedCompletionRequest();
		newStudentRegisterCampusAffiliatedCompletionRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentRegisterCampusAffiliatedCompletionRequest;
	}

	/**
	 * Instantiates a new array of StudentRegisterCampusAffiliatedCompletionRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentRegisterCampusAffiliatedCompletionRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentRegisterCampusAffiliatedCompletionRequestArray = [];
		genericArray.forEach(genericObject => {
			newStudentRegisterCampusAffiliatedCompletionRequestArray.push(StudentRegisterCampusAffiliatedCompletionRequest.create(genericObject));
		});
		return newStudentRegisterCampusAffiliatedCompletionRequestArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('password', 'string'),
	ModelBaseClass.createModelProperty('person', 'Person'),
];

export default StudentRegisterCampusAffiliatedCompletionRequestBase;
