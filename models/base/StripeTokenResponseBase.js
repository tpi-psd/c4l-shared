import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StripeTokenResponse from "../StripeTokenResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StripeTokenResponseBase
 * @extends ModelBaseClass
 * @property {string} token
 */
class StripeTokenResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StripeTokenResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StripeTokenResponse}
	 */
	static create(genericObject) {
		const newStripeTokenResponse = new StripeTokenResponse();
		newStripeTokenResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStripeTokenResponse;
	}

	/**
	 * Instantiates a new array of StripeTokenResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StripeTokenResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStripeTokenResponseArray = [];
		genericArray.forEach(genericObject => {
			newStripeTokenResponseArray.push(StripeTokenResponse.create(genericObject));
		});
		return newStripeTokenResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('token', 'string'),
];

export default StripeTokenResponseBase;
