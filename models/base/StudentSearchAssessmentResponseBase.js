import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentSearchAssessmentResponse from "../StudentSearchAssessmentResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentSearchAssessmentResponseBase
 * @extends ModelBaseClass
 * @property {[Assessment]} assessments
 * @property {number} totalItemCount (int64)
 */
class StudentSearchAssessmentResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentSearchAssessmentResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentSearchAssessmentResponse}
	 */
	static create(genericObject) {
		const newStudentSearchAssessmentResponse = new StudentSearchAssessmentResponse();
		newStudentSearchAssessmentResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentSearchAssessmentResponse;
	}

	/**
	 * Instantiates a new array of StudentSearchAssessmentResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentSearchAssessmentResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentSearchAssessmentResponseArray = [];
		genericArray.forEach(genericObject => {
			newStudentSearchAssessmentResponseArray.push(StudentSearchAssessmentResponse.create(genericObject));
		});
		return newStudentSearchAssessmentResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('assessments', '[Assessment]'),
	ModelBaseClass.createModelProperty('totalItemCount', 'integer'),
];

export default StudentSearchAssessmentResponseBase;
