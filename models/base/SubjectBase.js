import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Subject from "../Subject";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class SubjectBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {string} name
 * @property {number} orderNumber (integer)
 * @property {boolean} introductionFlag will be true only for the one subject that is deemed the 'Introduction' subject
 * @property {boolean} requirementsLockedFlag will be true only for subjects where requirements are tied to grades
 * @property {number} courseCount (integer)
 * @property {number} chapterCount (integer)
 * @property {boolean} activeFlag
 * @property {Date} dateCreated (date and time)
 * @property {[Course]} courses
 * @property {[Requirement]} requirements
 */
class SubjectBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Subject based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Subject}
	 */
	static create(genericObject) {
		const newSubject = new Subject();
		newSubject.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newSubject;
	}

	/**
	 * Instantiates a new array of Subject based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Subject]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newSubjectArray = [];
		genericArray.forEach(genericObject => {
			newSubjectArray.push(Subject.create(genericObject));
		});
		return newSubjectArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('name', 'string'),
	ModelBaseClass.createModelProperty('orderNumber', 'integer'),
	ModelBaseClass.createModelProperty('introductionFlag', 'boolean'),
	ModelBaseClass.createModelProperty('requirementsLockedFlag', 'boolean'),
	ModelBaseClass.createModelProperty('courseCount', 'integer'),
	ModelBaseClass.createModelProperty('chapterCount', 'integer'),
	ModelBaseClass.createModelProperty('activeFlag', 'boolean'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
	ModelBaseClass.createModelProperty('courses', '[Course]'),
	ModelBaseClass.createModelProperty('requirements', '[Requirement]'),
];

export default SubjectBase;
