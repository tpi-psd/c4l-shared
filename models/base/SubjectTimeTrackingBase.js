import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import SubjectTimeTracking from "../SubjectTimeTracking";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class SubjectTimeTrackingBase
 * @extends ModelBaseClass
 * @property {number} subjectId (int64)
 * @property {number} readingElapsedTime (integer) total time, in seconds
 * @property {number} quizzesElapsedTime (integer) total time, in seconds
 * @property {number} examsElapsedTime (integer) total time, in seconds
 * @property {number} overallElapsedTime (integer) total time, in seconds
 */
class SubjectTimeTrackingBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of SubjectTimeTracking based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {SubjectTimeTracking}
	 */
	static create(genericObject) {
		const newSubjectTimeTracking = new SubjectTimeTracking();
		newSubjectTimeTracking.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newSubjectTimeTracking;
	}

	/**
	 * Instantiates a new array of SubjectTimeTracking based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[SubjectTimeTracking]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newSubjectTimeTrackingArray = [];
		genericArray.forEach(genericObject => {
			newSubjectTimeTrackingArray.push(SubjectTimeTracking.create(genericObject));
		});
		return newSubjectTimeTrackingArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('subjectId', 'integer'),
	ModelBaseClass.createModelProperty('readingElapsedTime', 'integer'),
	ModelBaseClass.createModelProperty('quizzesElapsedTime', 'integer'),
	ModelBaseClass.createModelProperty('examsElapsedTime', 'integer'),
	ModelBaseClass.createModelProperty('overallElapsedTime', 'integer'),
];

export default SubjectTimeTrackingBase;
