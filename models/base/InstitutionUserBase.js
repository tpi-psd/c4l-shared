import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import InstitutionUser from "../InstitutionUser";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class InstitutionUserBase
 * @extends ModelBaseClass
 * @property {number} schoolId (int64)
 * @property {boolean} allCampusFlag
 * @property {[number]} campusIdArray
 */
class InstitutionUserBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of InstitutionUser based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {InstitutionUser}
	 */
	static create(genericObject) {
		const newInstitutionUser = new InstitutionUser();
		newInstitutionUser.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newInstitutionUser;
	}

	/**
	 * Instantiates a new array of InstitutionUser based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[InstitutionUser]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newInstitutionUserArray = [];
		genericArray.forEach(genericObject => {
			newInstitutionUserArray.push(InstitutionUser.create(genericObject));
		});
		return newInstitutionUserArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('schoolId', 'integer'),
	ModelBaseClass.createModelProperty('allCampusFlag', 'boolean'),
	ModelBaseClass.createModelProperty('campusIdArray', '[integer]'),
];

export default InstitutionUserBase;
