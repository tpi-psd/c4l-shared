import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentAnswer from "../StudentAnswer";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentAnswerBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {number} assessmentId (int64)
 * @property {number} questionId (int64)
 * @property {number} answerId (int64) can be set to NULL if the answer was left unanswered / time had expired (must be NULL if a written quiz)
 * @property {'Correct'|'Incorrect'|'TimeExpired'|'Unanswered'} status only set when viewing results (must be NULL if a written quiz)
 * @property {number} correctAnswerId (int64) only set when viewing results (must be NULL if a written quiz)
 * @property {string} response only applicable for written quizzes
 * @property {number} elapsedTime (integer)
 * @property {Date} dateCreated (date and time)
 */
class StudentAnswerBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentAnswer based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentAnswer}
	 */
	static create(genericObject) {
		const newStudentAnswer = new StudentAnswer();
		newStudentAnswer.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentAnswer;
	}

	/**
	 * Instantiates a new array of StudentAnswer based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentAnswer]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentAnswerArray = [];
		genericArray.forEach(genericObject => {
			newStudentAnswerArray.push(StudentAnswer.create(genericObject));
		});
		return newStudentAnswerArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('assessmentId', 'integer'),
	ModelBaseClass.createModelProperty('questionId', 'integer'),
	ModelBaseClass.createModelProperty('answerId', 'integer'),
	ModelBaseClass.createModelProperty('status', 'string'),
	ModelBaseClass.createModelProperty('correctAnswerId', 'integer'),
	ModelBaseClass.createModelProperty('response', 'string'),
	ModelBaseClass.createModelProperty('elapsedTime', 'integer'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
];

export default StudentAnswerBase;
