import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Assessment from "../Assessment";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class AssessmentBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {boolean} legacyFlag
 * @property {Person} student
 * @property {number} courseId (int64)
 * @property {number} subjectId (int64)
 * @property {number} chapterId (integer) only set if this is a quiz
 * @property {Chapter} chapter only set if this is a quiz
 * @property {number} courseAttemptNumber (integer) only set for Quizzes and Final Exams
 * @property {'Quiz'|'ChallengeExam'|'FinalExam'} type
 * @property {'None'|'Standard'|'Video'|'Written'|'LearningInventoryStudentType'|'LearningInventoryLearningStyle'|'CommunicationStyle'|'CareerInventoryInterestProfiler'|'CareerInventoryJobZone'} chapterQuizType
 * @property {number} awardedPoints
 * @property {number} possiblePoints
 * @property {number} assessmentScore only applicable for non-special assessments -- otherwise, this will be null
 * @property {string} assessmentGrade only applicable for non-special assessments -- otherwise, this will be null
 * @property {boolean} passFlag
 * @property {number} elapsedTime (integer)
 * @property {Date} dateCompleted (date and time)
 * @property {boolean} lockoutFlag
 * @property {Date} dateFirstLockout (date and time)
 * @property {Person} unlockedByPerson
 * @property {Date} dateUnlocked (date and time)
 * @property {Date} dateSecondLockout (date and time)
 * @property {Date} dateCreated (date and time)
 * @property {[RenderedQuestion]} renderedStructure only set when calling /student/assessment/create or /student/assessment/get
 * @property {[StudentAnswer]} studentAnswers only set when calling /student/assessment/get
 * @property {string} alertModalTitle only set when calling /student/assessment/get
 * @property {string} alertModalContent only set when calling /student/assessment/get
 * @property {string} specialAssessmentTitle only set when calling /student/assessment/get, and for any quiz that is not (Standard, Written or Video) or for any final assessment that is not (Standard)
 * @property {string} specialAssessmentContentHtml only set when calling /student/assessment/get, and for any quiz that is not (Standard, Written or Video) or for any final assessment that is not (Standard)
 * @property {Person} gradedByPerson only set when calling for writtenQuizAnswer details
 * @property {Date} dateWrittenResponseGraded (date and time) only set when calling for writtenQuizAnswer details
 * @property {boolean} writtenResponseCanBeGradedFlag
 */
class AssessmentBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Assessment based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Assessment}
	 */
	static create(genericObject) {
		const newAssessment = new Assessment();
		newAssessment.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newAssessment;
	}

	/**
	 * Instantiates a new array of Assessment based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Assessment]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newAssessmentArray = [];
		genericArray.forEach(genericObject => {
			newAssessmentArray.push(Assessment.create(genericObject));
		});
		return newAssessmentArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('legacyFlag', 'boolean'),
	ModelBaseClass.createModelProperty('student', 'Person'),
	ModelBaseClass.createModelProperty('courseId', 'integer'),
	ModelBaseClass.createModelProperty('subjectId', 'integer'),
	ModelBaseClass.createModelProperty('chapterId', 'integer'),
	ModelBaseClass.createModelProperty('chapter', 'Chapter'),
	ModelBaseClass.createModelProperty('courseAttemptNumber', 'integer'),
	ModelBaseClass.createModelProperty('type', 'string'),
	ModelBaseClass.createModelProperty('chapterQuizType', 'string'),
	ModelBaseClass.createModelProperty('awardedPoints', 'float'),
	ModelBaseClass.createModelProperty('possiblePoints', 'float'),
	ModelBaseClass.createModelProperty('assessmentScore', 'float'),
	ModelBaseClass.createModelProperty('assessmentGrade', 'string'),
	ModelBaseClass.createModelProperty('passFlag', 'boolean'),
	ModelBaseClass.createModelProperty('elapsedTime', 'integer'),
	ModelBaseClass.createModelProperty('dateCompleted', 'datetime'),
	ModelBaseClass.createModelProperty('lockoutFlag', 'boolean'),
	ModelBaseClass.createModelProperty('dateFirstLockout', 'datetime'),
	ModelBaseClass.createModelProperty('unlockedByPerson', 'Person'),
	ModelBaseClass.createModelProperty('dateUnlocked', 'datetime'),
	ModelBaseClass.createModelProperty('dateSecondLockout', 'datetime'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
	ModelBaseClass.createModelProperty('renderedStructure', '[RenderedQuestion]'),
	ModelBaseClass.createModelProperty('studentAnswers', '[StudentAnswer]'),
	ModelBaseClass.createModelProperty('alertModalTitle', 'string'),
	ModelBaseClass.createModelProperty('alertModalContent', 'string'),
	ModelBaseClass.createModelProperty('specialAssessmentTitle', 'string'),
	ModelBaseClass.createModelProperty('specialAssessmentContentHtml', 'string'),
	ModelBaseClass.createModelProperty('gradedByPerson', 'Person'),
	ModelBaseClass.createModelProperty('dateWrittenResponseGraded', 'datetime'),
	ModelBaseClass.createModelProperty('writtenResponseCanBeGradedFlag', 'boolean'),
];

export default AssessmentBase;
