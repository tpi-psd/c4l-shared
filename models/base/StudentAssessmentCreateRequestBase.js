import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentAssessmentCreateRequest from "../StudentAssessmentCreateRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentAssessmentCreateRequestBase
 * @extends ModelBaseClass
 * @property {number} courseId (int64)
 * @property {number} courseAttemptNumber (integer) required for quizzes and final exam, ignored for challenge exams
 * @property {'Quiz'|'ChallengeExam'|'FinalExam'} type
 * @property {number} chapterId (int64) required for quizzes.  null for challenge or final exams.
 */
class StudentAssessmentCreateRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentAssessmentCreateRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentAssessmentCreateRequest}
	 */
	static create(genericObject) {
		const newStudentAssessmentCreateRequest = new StudentAssessmentCreateRequest();
		newStudentAssessmentCreateRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentAssessmentCreateRequest;
	}

	/**
	 * Instantiates a new array of StudentAssessmentCreateRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentAssessmentCreateRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentAssessmentCreateRequestArray = [];
		genericArray.forEach(genericObject => {
			newStudentAssessmentCreateRequestArray.push(StudentAssessmentCreateRequest.create(genericObject));
		});
		return newStudentAssessmentCreateRequestArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('courseId', 'integer'),
	ModelBaseClass.createModelProperty('courseAttemptNumber', 'integer'),
	ModelBaseClass.createModelProperty('type', 'string'),
	ModelBaseClass.createModelProperty('chapterId', 'integer'),
];

export default StudentAssessmentCreateRequestBase;
