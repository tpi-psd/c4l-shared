import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Issue from "../Issue";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class IssueBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {number} schoolId (int64)
 * @property {number} campusId (int64)
 * @property {Person} student only set if this is a student issue (as opposed to a dashboard issue)
 * @property {number} issueCategoryId (int64)
 * @property {'Open'|'Resolved'|'Closed'} status
 * @property {string} content
 * @property {Person} reportedByPerson
 * @property {Date} dateCreated (date and time)
 */
class IssueBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Issue based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Issue}
	 */
	static create(genericObject) {
		const newIssue = new Issue();
		newIssue.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newIssue;
	}

	/**
	 * Instantiates a new array of Issue based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Issue]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newIssueArray = [];
		genericArray.forEach(genericObject => {
			newIssueArray.push(Issue.create(genericObject));
		});
		return newIssueArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('schoolId', 'integer'),
	ModelBaseClass.createModelProperty('campusId', 'integer'),
	ModelBaseClass.createModelProperty('student', 'Person'),
	ModelBaseClass.createModelProperty('issueCategoryId', 'integer'),
	ModelBaseClass.createModelProperty('status', 'string'),
	ModelBaseClass.createModelProperty('content', 'string'),
	ModelBaseClass.createModelProperty('reportedByPerson', 'Person'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
];

export default IssueBase;
