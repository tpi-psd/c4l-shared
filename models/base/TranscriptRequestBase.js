import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import TranscriptRequest from "../TranscriptRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class TranscriptRequestBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {Person} student
 * @property {'PaymentFailed'|'Pending'|'Printed'|'Sent'} status
 * @property {boolean} shipToStudentFlag
 * @property {string} shipToName
 * @property {string} address1
 * @property {string} address2
 * @property {string} city
 * @property {string} state
 * @property {string} zip
 * @property {string} phone
 * @property {Person} shippedByPerson
 * @property {Date} dateShipped (date and time)
 * @property {Date} dateRequested (date and time)
 */
class TranscriptRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of TranscriptRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {TranscriptRequest}
	 */
	static create(genericObject) {
		const newTranscriptRequest = new TranscriptRequest();
		newTranscriptRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newTranscriptRequest;
	}

	/**
	 * Instantiates a new array of TranscriptRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[TranscriptRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newTranscriptRequestArray = [];
		genericArray.forEach(genericObject => {
			newTranscriptRequestArray.push(TranscriptRequest.create(genericObject));
		});
		return newTranscriptRequestArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('student', 'Person'),
	ModelBaseClass.createModelProperty('status', 'string'),
	ModelBaseClass.createModelProperty('shipToStudentFlag', 'boolean'),
	ModelBaseClass.createModelProperty('shipToName', 'string'),
	ModelBaseClass.createModelProperty('address1', 'string'),
	ModelBaseClass.createModelProperty('address2', 'string'),
	ModelBaseClass.createModelProperty('city', 'string'),
	ModelBaseClass.createModelProperty('state', 'string'),
	ModelBaseClass.createModelProperty('zip', 'string'),
	ModelBaseClass.createModelProperty('phone', 'string'),
	ModelBaseClass.createModelProperty('shippedByPerson', 'Person'),
	ModelBaseClass.createModelProperty('dateShipped', 'datetime'),
	ModelBaseClass.createModelProperty('dateRequested', 'datetime'),
];

export default TranscriptRequestBase;
