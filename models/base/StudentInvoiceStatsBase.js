import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentInvoiceStats from "../StudentInvoiceStats";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentInvoiceStatsBase
 * @extends ModelBaseClass
 * @property {number} schoolId (int64)
 * @property {number} campusId (int64)
 * @property {number} daysActive (integer)
 * @property {number} statusTypeId (integer)
 * @property {Date} dateCreated (date and time)
 */
class StudentInvoiceStatsBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentInvoiceStats based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentInvoiceStats}
	 */
	static create(genericObject) {
		const newStudentInvoiceStats = new StudentInvoiceStats();
		newStudentInvoiceStats.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentInvoiceStats;
	}

	/**
	 * Instantiates a new array of StudentInvoiceStats based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentInvoiceStats]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentInvoiceStatsArray = [];
		genericArray.forEach(genericObject => {
			newStudentInvoiceStatsArray.push(StudentInvoiceStats.create(genericObject));
		});
		return newStudentInvoiceStatsArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('schoolId', 'integer'),
	ModelBaseClass.createModelProperty('campusId', 'integer'),
	ModelBaseClass.createModelProperty('daysActive', 'integer'),
	ModelBaseClass.createModelProperty('statusTypeId', 'integer'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
];

export default StudentInvoiceStatsBase;
