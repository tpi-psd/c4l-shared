import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import PaymentRequest from "../PaymentRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class PaymentRequestBase
 * @extends ModelBaseClass
 * @property {string} nameOnCard
 * @property {string} stripeToken the stripe token for the CC#, CVV, Date Expiration and Zip that was tokenized by Stripe.js
 */
class PaymentRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of PaymentRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {PaymentRequest}
	 */
	static create(genericObject) {
		const newPaymentRequest = new PaymentRequest();
		newPaymentRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newPaymentRequest;
	}

	/**
	 * Instantiates a new array of PaymentRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[PaymentRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newPaymentRequestArray = [];
		genericArray.forEach(genericObject => {
			newPaymentRequestArray.push(PaymentRequest.create(genericObject));
		});
		return newPaymentRequestArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('nameOnCard', 'string'),
	ModelBaseClass.createModelProperty('stripeToken', 'string'),
];

export default PaymentRequestBase;
