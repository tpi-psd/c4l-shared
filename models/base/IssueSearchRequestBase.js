import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import IssueSearchRequest from "../IssueSearchRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class IssueSearchRequestBase
 * @extends ModelBaseClass
 * @property {string} smartFilter can be student first name, student last name, or student's studentIdentifierLabel
 * @property {number} filterByIssueCategoryId (int64)
 * @property {'Open'|'Resolved'|'Closed'|'NotClosed'} filterByStatus
 * @property {number} filterBySchoolId (int64)
 * @property {number} filterByCampusId (int64)
 * @property {ResultParameter} resultParameter [IssueCategory, School, Student, DateCreated, Status]
 */
class IssueSearchRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of IssueSearchRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {IssueSearchRequest}
	 */
	static create(genericObject) {
		const newIssueSearchRequest = new IssueSearchRequest();
		newIssueSearchRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newIssueSearchRequest;
	}

	/**
	 * Instantiates a new array of IssueSearchRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[IssueSearchRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newIssueSearchRequestArray = [];
		genericArray.forEach(genericObject => {
			newIssueSearchRequestArray.push(IssueSearchRequest.create(genericObject));
		});
		return newIssueSearchRequestArray;
	}
}

/**
 * @type {string} OrderByIssueCategory
 */
IssueSearchRequestBase.OrderByIssueCategory = 'issuecategory';

/**
 * @type {string} OrderBySchool
 */
IssueSearchRequestBase.OrderBySchool = 'school';

/**
 * @type {string} OrderByStudent
 */
IssueSearchRequestBase.OrderByStudent = 'student';

/**
 * @type {string} OrderByDateCreated
 */
IssueSearchRequestBase.OrderByDateCreated = 'datecreated';

/**
 * @type {string} OrderByStatus
 */
IssueSearchRequestBase.OrderByStatus = 'status';

const _modelDefinition = [
	ModelBaseClass.createModelProperty('smartFilter', 'string'),
	ModelBaseClass.createModelProperty('filterByIssueCategoryId', 'integer'),
	ModelBaseClass.createModelProperty('filterByStatus', 'string'),
	ModelBaseClass.createModelProperty('filterBySchoolId', 'integer'),
	ModelBaseClass.createModelProperty('filterByCampusId', 'integer'),
	ModelBaseClass.createModelProperty('resultParameter', 'ResultParameter'),
];

export default IssueSearchRequestBase;
