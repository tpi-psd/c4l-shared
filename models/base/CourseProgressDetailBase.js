import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import CourseProgressDetail from "../CourseProgressDetail";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class CourseProgressDetailBase
 * @extends ModelBaseClass
 * @property {[ChapterProgress]} chapterProgressArray
 */
class CourseProgressDetailBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of CourseProgressDetail based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {CourseProgressDetail}
	 */
	static create(genericObject) {
		const newCourseProgressDetail = new CourseProgressDetail();
		newCourseProgressDetail.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newCourseProgressDetail;
	}

	/**
	 * Instantiates a new array of CourseProgressDetail based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[CourseProgressDetail]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newCourseProgressDetailArray = [];
		genericArray.forEach(genericObject => {
			newCourseProgressDetailArray.push(CourseProgressDetail.create(genericObject));
		});
		return newCourseProgressDetailArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('chapterProgressArray', '[ChapterProgress]'),
];

export default CourseProgressDetailBase;
