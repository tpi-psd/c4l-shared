import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import LoginLog from "../LoginLog";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class LoginLogBase
 * @extends ModelBaseClass
 * @property {string} status
 * @property {Date} dateLogged (date and time)
 * @property {string} userAgent
 */
class LoginLogBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of LoginLog based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {LoginLog}
	 */
	static create(genericObject) {
		const newLoginLog = new LoginLog();
		newLoginLog.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newLoginLog;
	}

	/**
	 * Instantiates a new array of LoginLog based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[LoginLog]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newLoginLogArray = [];
		genericArray.forEach(genericObject => {
			newLoginLogArray.push(LoginLog.create(genericObject));
		});
		return newLoginLogArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('status', 'string'),
	ModelBaseClass.createModelProperty('dateLogged', 'datetime'),
	ModelBaseClass.createModelProperty('userAgent', 'string'),
];

export default LoginLogBase;
