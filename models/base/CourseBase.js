import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Course from "../Course";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class CourseBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {number} subjectId (int64)
 * @property {number} requirementId (int64)
 * @property {'None'|'Standard'|'LearningInventoryResults'|'CareerInventoryResults'} assessmentType
 * @property {string} finalExamName
 * @property {string} name
 * @property {number} chapterCount (integer)
 * @property {number} questionCount (integer)
 * @property {boolean} activeFlag
 * @property {Date} dateCreated (date and time)
 */
class CourseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Course based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Course}
	 */
	static create(genericObject) {
		const newCourse = new Course();
		newCourse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newCourse;
	}

	/**
	 * Instantiates a new array of Course based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Course]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newCourseArray = [];
		genericArray.forEach(genericObject => {
			newCourseArray.push(Course.create(genericObject));
		});
		return newCourseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('subjectId', 'integer'),
	ModelBaseClass.createModelProperty('requirementId', 'integer'),
	ModelBaseClass.createModelProperty('assessmentType', 'string'),
	ModelBaseClass.createModelProperty('finalExamName', 'string'),
	ModelBaseClass.createModelProperty('name', 'string'),
	ModelBaseClass.createModelProperty('chapterCount', 'integer'),
	ModelBaseClass.createModelProperty('questionCount', 'integer'),
	ModelBaseClass.createModelProperty('activeFlag', 'boolean'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
];

export default CourseBase;
