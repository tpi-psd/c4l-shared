import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentSearchAssessmentRequest from "../StudentSearchAssessmentRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentSearchAssessmentRequestBase
 * @extends ModelBaseClass
 * @property {number} studentId (int64)
 * @property {number} filterBySubjectId (int64)
 * @property {number} filterByCourseId (int64)
 * @property {['Quiz'|'ChallengeExam'|'FinalExam']} filterByTypeArray
 * @property {boolean} filterByPassFlag
 * @property {boolean} includeSpecialAssessmentsFlag set to TRUE if you want to include any 'special' assessments -- otherwise, by default we will not return any 'special' assessments
 * @property {boolean} includeIncompleteFlag set to TRUE if you want to include any incomplete (not-yet-submitted) assessments -- otherwise, by default we will not return any incomplete assessments
 * @property {ResultParameter} resultParameter [Type, Subject, Course, Chapter, QuizType, Result, DateCreated, DateStarted, DateCompleted]
 */
class StudentSearchAssessmentRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentSearchAssessmentRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentSearchAssessmentRequest}
	 */
	static create(genericObject) {
		const newStudentSearchAssessmentRequest = new StudentSearchAssessmentRequest();
		newStudentSearchAssessmentRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentSearchAssessmentRequest;
	}

	/**
	 * Instantiates a new array of StudentSearchAssessmentRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentSearchAssessmentRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentSearchAssessmentRequestArray = [];
		genericArray.forEach(genericObject => {
			newStudentSearchAssessmentRequestArray.push(StudentSearchAssessmentRequest.create(genericObject));
		});
		return newStudentSearchAssessmentRequestArray;
	}
}

/**
 * @type {string} OrderByType
 */
StudentSearchAssessmentRequestBase.OrderByType = 'type';

/**
 * @type {string} OrderBySubject
 */
StudentSearchAssessmentRequestBase.OrderBySubject = 'subject';

/**
 * @type {string} OrderByCourse
 */
StudentSearchAssessmentRequestBase.OrderByCourse = 'course';

/**
 * @type {string} OrderByChapter
 */
StudentSearchAssessmentRequestBase.OrderByChapter = 'chapter';

/**
 * @type {string} OrderByQuizType
 */
StudentSearchAssessmentRequestBase.OrderByQuizType = 'quiztype';

/**
 * @type {string} OrderByResult
 */
StudentSearchAssessmentRequestBase.OrderByResult = 'result';

/**
 * @type {string} OrderByDateCreated
 */
StudentSearchAssessmentRequestBase.OrderByDateCreated = 'datecreated';

/**
 * @type {string} OrderByDateStarted
 */
StudentSearchAssessmentRequestBase.OrderByDateStarted = 'datestarted';

/**
 * @type {string} OrderByDateCompleted
 */
StudentSearchAssessmentRequestBase.OrderByDateCompleted = 'datecompleted';

const _modelDefinition = [
	ModelBaseClass.createModelProperty('studentId', 'integer'),
	ModelBaseClass.createModelProperty('filterBySubjectId', 'integer'),
	ModelBaseClass.createModelProperty('filterByCourseId', 'integer'),
	ModelBaseClass.createModelProperty('filterByTypeArray', '[string]'),
	ModelBaseClass.createModelProperty('filterByPassFlag', 'boolean'),
	ModelBaseClass.createModelProperty('includeSpecialAssessmentsFlag', 'boolean'),
	ModelBaseClass.createModelProperty('includeIncompleteFlag', 'boolean'),
	ModelBaseClass.createModelProperty('resultParameter', 'ResultParameter'),
];

export default StudentSearchAssessmentRequestBase;
