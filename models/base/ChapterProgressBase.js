import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import ChapterProgress from "../ChapterProgress";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class ChapterProgressBase
 * @extends ModelBaseClass
 * @property {number} chapterId (int64)
 * @property {number} elapsedTime (integer) total time, in seconds
 * @property {number} completionPercentage
 * @property {[SectionProgress]} sectionProgressArray
 */
class ChapterProgressBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of ChapterProgress based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {ChapterProgress}
	 */
	static create(genericObject) {
		const newChapterProgress = new ChapterProgress();
		newChapterProgress.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newChapterProgress;
	}

	/**
	 * Instantiates a new array of ChapterProgress based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[ChapterProgress]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newChapterProgressArray = [];
		genericArray.forEach(genericObject => {
			newChapterProgressArray.push(ChapterProgress.create(genericObject));
		});
		return newChapterProgressArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('chapterId', 'integer'),
	ModelBaseClass.createModelProperty('elapsedTime', 'integer'),
	ModelBaseClass.createModelProperty('completionPercentage', 'float'),
	ModelBaseClass.createModelProperty('sectionProgressArray', '[SectionProgress]'),
];

export default ChapterProgressBase;
