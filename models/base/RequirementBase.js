import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Requirement from "../Requirement";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class RequirementBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {number} subjectId (int64)
 * @property {string} name
 * @property {number} orderNumber (integer)
 * @property {[number]} courseIdArray
 */
class RequirementBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Requirement based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Requirement}
	 */
	static create(genericObject) {
		const newRequirement = new Requirement();
		newRequirement.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newRequirement;
	}

	/**
	 * Instantiates a new array of Requirement based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Requirement]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newRequirementArray = [];
		genericArray.forEach(genericObject => {
			newRequirementArray.push(Requirement.create(genericObject));
		});
		return newRequirementArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('subjectId', 'integer'),
	ModelBaseClass.createModelProperty('name', 'string'),
	ModelBaseClass.createModelProperty('orderNumber', 'integer'),
	ModelBaseClass.createModelProperty('courseIdArray', '[integer]'),
];

export default RequirementBase;
