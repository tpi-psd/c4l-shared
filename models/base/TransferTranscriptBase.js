import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import TransferTranscript from "../TransferTranscript";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class TransferTranscriptBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {number} studentId (int64)
 * @property {FileAsset} fileAsset
 * @property {string} schoolName
 * @property {boolean} verifiedFlag
 * @property {Person} processedByPerson
 * @property {Date} dateProcessed (date and time)
 * @property {string} note
 * @property {Person} uploadedByPerson
 * @property {Date} dateCreated (date and time)
 * @property {[TransferTranscriptAppliedCredit]} appliedCredits
 */
class TransferTranscriptBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of TransferTranscript based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {TransferTranscript}
	 */
	static create(genericObject) {
		const newTransferTranscript = new TransferTranscript();
		newTransferTranscript.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newTransferTranscript;
	}

	/**
	 * Instantiates a new array of TransferTranscript based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[TransferTranscript]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newTransferTranscriptArray = [];
		genericArray.forEach(genericObject => {
			newTransferTranscriptArray.push(TransferTranscript.create(genericObject));
		});
		return newTransferTranscriptArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('studentId', 'integer'),
	ModelBaseClass.createModelProperty('fileAsset', 'FileAsset'),
	ModelBaseClass.createModelProperty('schoolName', 'string'),
	ModelBaseClass.createModelProperty('verifiedFlag', 'boolean'),
	ModelBaseClass.createModelProperty('processedByPerson', 'Person'),
	ModelBaseClass.createModelProperty('dateProcessed', 'datetime'),
	ModelBaseClass.createModelProperty('note', 'string'),
	ModelBaseClass.createModelProperty('uploadedByPerson', 'Person'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
	ModelBaseClass.createModelProperty('appliedCredits', '[TransferTranscriptAppliedCredit]'),
];

export default TransferTranscriptBase;
