import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentRegisterSelfServiceInitiationRequest from "../StudentRegisterSelfServiceInitiationRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentRegisterSelfServiceInitiationRequestBase
 * @extends ModelBaseClass
 * @property {string} email
 * @property {string} firstName
 * @property {string} lastName
 * @property {string} phone
 */
class StudentRegisterSelfServiceInitiationRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentRegisterSelfServiceInitiationRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentRegisterSelfServiceInitiationRequest}
	 */
	static create(genericObject) {
		const newStudentRegisterSelfServiceInitiationRequest = new StudentRegisterSelfServiceInitiationRequest();
		newStudentRegisterSelfServiceInitiationRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentRegisterSelfServiceInitiationRequest;
	}

	/**
	 * Instantiates a new array of StudentRegisterSelfServiceInitiationRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentRegisterSelfServiceInitiationRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentRegisterSelfServiceInitiationRequestArray = [];
		genericArray.forEach(genericObject => {
			newStudentRegisterSelfServiceInitiationRequestArray.push(StudentRegisterSelfServiceInitiationRequest.create(genericObject));
		});
		return newStudentRegisterSelfServiceInitiationRequestArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('email', 'string'),
	ModelBaseClass.createModelProperty('firstName', 'string'),
	ModelBaseClass.createModelProperty('lastName', 'string'),
	ModelBaseClass.createModelProperty('phone', 'string'),
];

export default StudentRegisterSelfServiceInitiationRequestBase;
