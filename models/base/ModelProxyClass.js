import ResultParameter from "../ResultParameter";
import PersonLoginRequest from "../PersonLoginRequest";
import ForgotPasswordRequest from "../ForgotPasswordRequest";
import UpdatePasswordRequest from "../UpdatePasswordRequest";
import PersonDashboardResponse from "../PersonDashboardResponse";
import PersonLoginAsResponse from "../PersonLoginAsResponse";
import PersonAdminSearchRequest from "../PersonAdminSearchRequest";
import PersonAdminSearchResponse from "../PersonAdminSearchResponse";
import StudentRegisterSelfServiceInitiationRequest from "../StudentRegisterSelfServiceInitiationRequest";
import StudentRegisterSelfServiceCompletionRequest from "../StudentRegisterSelfServiceCompletionRequest";
import StudentRegisterCampusAffiliatedInitiationRequest from "../StudentRegisterCampusAffiliatedInitiationRequest";
import StudentRegisterCampusAffiliatedCompletionRequest from "../StudentRegisterCampusAffiliatedCompletionRequest";
import StudentSearchRequest from "../StudentSearchRequest";
import StudentSearchResponse from "../StudentSearchResponse";
import StudentCourseProgressResponse from "../StudentCourseProgressResponse";
import StudentGetCourseWithProgressResponse from "../StudentGetCourseWithProgressResponse";
import StudentSearchAssessmentRequest from "../StudentSearchAssessmentRequest";
import StudentSearchAssessmentResponse from "../StudentSearchAssessmentResponse";
import StudentAssessmentCreateRequest from "../StudentAssessmentCreateRequest";
import InstitutionSchoolSearchRequest from "../InstitutionSchoolSearchRequest";
import InstitutionSchoolSearchResponse from "../InstitutionSchoolSearchResponse";
import InstitutionCampusSearchRequest from "../InstitutionCampusSearchRequest";
import InstitutionCampusSearchResponse from "../InstitutionCampusSearchResponse";
import IssueEditRequest from "../IssueEditRequest";
import IssueSearchRequest from "../IssueSearchRequest";
import IssueSearchResponse from "../IssueSearchResponse";
import WrittenQuizAnswerSearchRequest from "../WrittenQuizAnswerSearchRequest";
import WrittenQuizAnswerSearchResponse from "../WrittenQuizAnswerSearchResponse";
import AutoGradeLockoutSearchRequest from "../AutoGradeLockoutSearchRequest";
import AutoGradeLockoutSearchResponse from "../AutoGradeLockoutSearchResponse";
import TranscriptRequestSearchRequest from "../TranscriptRequestSearchRequest";
import TranscriptRequestSearchResponse from "../TranscriptRequestSearchResponse";
import TranscriptRequestOrderRequest from "../TranscriptRequestOrderRequest";
import CurriculumQuestionSearchRequest from "../CurriculumQuestionSearchRequest";
import CurriculumQuestionSearchResponse from "../CurriculumQuestionSearchResponse";
import CurriculumQuestionSaveRequest from "../CurriculumQuestionSaveRequest";
import SystemSettings from "../SystemSettings";
import SystemBadges from "../SystemBadges";
import Session from "../Session";
import LoginLog from "../LoginLog";
import Person from "../Person";
import InstitutionUser from "../InstitutionUser";
import Student from "../Student";
import Subscription from "../Subscription";
import StudentPreferences from "../StudentPreferences";
import StudentInvoiceStats from "../StudentInvoiceStats";
import School from "../School";
import SchoolPreferences from "../SchoolPreferences";
import Campus from "../Campus";
import TranscriptRequest from "../TranscriptRequest";
import Issue from "../Issue";
import IssueCategory from "../IssueCategory";
import Subject from "../Subject";
import Course from "../Course";
import Requirement from "../Requirement";
import Chapter from "../Chapter";
import SectionName from "../SectionName";
import Section from "../Section";
import Question from "../Question";
import Answer from "../Answer";
import CourseProgress from "../CourseProgress";
import CourseProgressDetail from "../CourseProgressDetail";
import ChapterProgress from "../ChapterProgress";
import SectionProgress from "../SectionProgress";
import SubjectTimeTracking from "../SubjectTimeTracking";
import Assessment from "../Assessment";
import RenderedQuestion from "../RenderedQuestion";
import RenderedAnswerChoice from "../RenderedAnswerChoice";
import StudentAnswer from "../StudentAnswer";
import TransferTranscript from "../TransferTranscript";
import TransferTranscriptAppliedCredit from "../TransferTranscriptAppliedCredit";
import Announcement from "../Announcement";
import PaymentRequest from "../PaymentRequest";
import PaymentMethod from "../PaymentMethod";
import StripeTokenResponse from "../StripeTokenResponse";
import Note from "../Note";
import FileAsset from "../FileAsset";

class ModelProxyClass {
	/**
	 * Constructs a model-based BaseClass subclass based on the className
	 * @param {string} className
	 * @param {object} genericObject
	 * @return {ModelBaseClass}
	 */
	static createByClassName(className, genericObject) {
		switch (className) {
		case 'ResultParameter':
			return ResultParameter.create(genericObject);
		case 'PersonLoginRequest':
			return PersonLoginRequest.create(genericObject);
		case 'ForgotPasswordRequest':
			return ForgotPasswordRequest.create(genericObject);
		case 'UpdatePasswordRequest':
			return UpdatePasswordRequest.create(genericObject);
		case 'PersonDashboardResponse':
			return PersonDashboardResponse.create(genericObject);
		case 'PersonLoginAsResponse':
			return PersonLoginAsResponse.create(genericObject);
		case 'PersonAdminSearchRequest':
			return PersonAdminSearchRequest.create(genericObject);
		case 'PersonAdminSearchResponse':
			return PersonAdminSearchResponse.create(genericObject);
		case 'StudentRegisterSelfServiceInitiationRequest':
			return StudentRegisterSelfServiceInitiationRequest.create(genericObject);
		case 'StudentRegisterSelfServiceCompletionRequest':
			return StudentRegisterSelfServiceCompletionRequest.create(genericObject);
		case 'StudentRegisterCampusAffiliatedInitiationRequest':
			return StudentRegisterCampusAffiliatedInitiationRequest.create(genericObject);
		case 'StudentRegisterCampusAffiliatedCompletionRequest':
			return StudentRegisterCampusAffiliatedCompletionRequest.create(genericObject);
		case 'StudentSearchRequest':
			return StudentSearchRequest.create(genericObject);
		case 'StudentSearchResponse':
			return StudentSearchResponse.create(genericObject);
		case 'StudentCourseProgressResponse':
			return StudentCourseProgressResponse.create(genericObject);
		case 'StudentGetCourseWithProgressResponse':
			return StudentGetCourseWithProgressResponse.create(genericObject);
		case 'StudentSearchAssessmentRequest':
			return StudentSearchAssessmentRequest.create(genericObject);
		case 'StudentSearchAssessmentResponse':
			return StudentSearchAssessmentResponse.create(genericObject);
		case 'StudentAssessmentCreateRequest':
			return StudentAssessmentCreateRequest.create(genericObject);
		case 'InstitutionSchoolSearchRequest':
			return InstitutionSchoolSearchRequest.create(genericObject);
		case 'InstitutionSchoolSearchResponse':
			return InstitutionSchoolSearchResponse.create(genericObject);
		case 'InstitutionCampusSearchRequest':
			return InstitutionCampusSearchRequest.create(genericObject);
		case 'InstitutionCampusSearchResponse':
			return InstitutionCampusSearchResponse.create(genericObject);
		case 'IssueEditRequest':
			return IssueEditRequest.create(genericObject);
		case 'IssueSearchRequest':
			return IssueSearchRequest.create(genericObject);
		case 'IssueSearchResponse':
			return IssueSearchResponse.create(genericObject);
		case 'WrittenQuizAnswerSearchRequest':
			return WrittenQuizAnswerSearchRequest.create(genericObject);
		case 'WrittenQuizAnswerSearchResponse':
			return WrittenQuizAnswerSearchResponse.create(genericObject);
		case 'AutoGradeLockoutSearchRequest':
			return AutoGradeLockoutSearchRequest.create(genericObject);
		case 'AutoGradeLockoutSearchResponse':
			return AutoGradeLockoutSearchResponse.create(genericObject);
		case 'TranscriptRequestSearchRequest':
			return TranscriptRequestSearchRequest.create(genericObject);
		case 'TranscriptRequestSearchResponse':
			return TranscriptRequestSearchResponse.create(genericObject);
		case 'TranscriptRequestOrderRequest':
			return TranscriptRequestOrderRequest.create(genericObject);
		case 'CurriculumQuestionSearchRequest':
			return CurriculumQuestionSearchRequest.create(genericObject);
		case 'CurriculumQuestionSearchResponse':
			return CurriculumQuestionSearchResponse.create(genericObject);
		case 'CurriculumQuestionSaveRequest':
			return CurriculumQuestionSaveRequest.create(genericObject);
		case 'SystemSettings':
			return SystemSettings.create(genericObject);
		case 'SystemBadges':
			return SystemBadges.create(genericObject);
		case 'Session':
			return Session.create(genericObject);
		case 'LoginLog':
			return LoginLog.create(genericObject);
		case 'Person':
			return Person.create(genericObject);
		case 'InstitutionUser':
			return InstitutionUser.create(genericObject);
		case 'Student':
			return Student.create(genericObject);
		case 'Subscription':
			return Subscription.create(genericObject);
		case 'StudentPreferences':
			return StudentPreferences.create(genericObject);
		case 'StudentInvoiceStats':
			return StudentInvoiceStats.create(genericObject);
		case 'School':
			return School.create(genericObject);
		case 'SchoolPreferences':
			return SchoolPreferences.create(genericObject);
		case 'Campus':
			return Campus.create(genericObject);
		case 'TranscriptRequest':
			return TranscriptRequest.create(genericObject);
		case 'Issue':
			return Issue.create(genericObject);
		case 'IssueCategory':
			return IssueCategory.create(genericObject);
		case 'Subject':
			return Subject.create(genericObject);
		case 'Course':
			return Course.create(genericObject);
		case 'Requirement':
			return Requirement.create(genericObject);
		case 'Chapter':
			return Chapter.create(genericObject);
		case 'SectionName':
			return SectionName.create(genericObject);
		case 'Section':
			return Section.create(genericObject);
		case 'Question':
			return Question.create(genericObject);
		case 'Answer':
			return Answer.create(genericObject);
		case 'CourseProgress':
			return CourseProgress.create(genericObject);
		case 'CourseProgressDetail':
			return CourseProgressDetail.create(genericObject);
		case 'ChapterProgress':
			return ChapterProgress.create(genericObject);
		case 'SectionProgress':
			return SectionProgress.create(genericObject);
		case 'SubjectTimeTracking':
			return SubjectTimeTracking.create(genericObject);
		case 'Assessment':
			return Assessment.create(genericObject);
		case 'RenderedQuestion':
			return RenderedQuestion.create(genericObject);
		case 'RenderedAnswerChoice':
			return RenderedAnswerChoice.create(genericObject);
		case 'StudentAnswer':
			return StudentAnswer.create(genericObject);
		case 'TransferTranscript':
			return TransferTranscript.create(genericObject);
		case 'TransferTranscriptAppliedCredit':
			return TransferTranscriptAppliedCredit.create(genericObject);
		case 'Announcement':
			return Announcement.create(genericObject);
		case 'PaymentRequest':
			return PaymentRequest.create(genericObject);
		case 'PaymentMethod':
			return PaymentMethod.create(genericObject);
		case 'StripeTokenResponse':
			return StripeTokenResponse.create(genericObject);
		case 'Note':
			return Note.create(genericObject);
		case 'FileAsset':
			return FileAsset.create(genericObject);
		default:
			throw new Error('Undefined model class: ' + className);
		}
	}
}

export default ModelProxyClass;
