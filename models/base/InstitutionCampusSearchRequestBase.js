import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import InstitutionCampusSearchRequest from "../InstitutionCampusSearchRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class InstitutionCampusSearchRequestBase
 * @extends ModelBaseClass
 * @property {boolean} includeInactiveFlag
 * @property {number} filterBySchoolId (int64)
 * @property {ResultParameter} resultParameter [Id, Name, School, UserCount, StudentActiveCount, DateCreated]
 */
class InstitutionCampusSearchRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of InstitutionCampusSearchRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {InstitutionCampusSearchRequest}
	 */
	static create(genericObject) {
		const newInstitutionCampusSearchRequest = new InstitutionCampusSearchRequest();
		newInstitutionCampusSearchRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newInstitutionCampusSearchRequest;
	}

	/**
	 * Instantiates a new array of InstitutionCampusSearchRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[InstitutionCampusSearchRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newInstitutionCampusSearchRequestArray = [];
		genericArray.forEach(genericObject => {
			newInstitutionCampusSearchRequestArray.push(InstitutionCampusSearchRequest.create(genericObject));
		});
		return newInstitutionCampusSearchRequestArray;
	}
}

/**
 * @type {string} OrderById
 */
InstitutionCampusSearchRequestBase.OrderById = 'id';

/**
 * @type {string} OrderByName
 */
InstitutionCampusSearchRequestBase.OrderByName = 'name';

/**
 * @type {string} OrderBySchool
 */
InstitutionCampusSearchRequestBase.OrderBySchool = 'school';

/**
 * @type {string} OrderByUserCount
 */
InstitutionCampusSearchRequestBase.OrderByUserCount = 'usercount';

/**
 * @type {string} OrderByStudentActiveCount
 */
InstitutionCampusSearchRequestBase.OrderByStudentActiveCount = 'studentactivecount';

/**
 * @type {string} OrderByDateCreated
 */
InstitutionCampusSearchRequestBase.OrderByDateCreated = 'datecreated';

const _modelDefinition = [
	ModelBaseClass.createModelProperty('includeInactiveFlag', 'boolean'),
	ModelBaseClass.createModelProperty('filterBySchoolId', 'integer'),
	ModelBaseClass.createModelProperty('resultParameter', 'ResultParameter'),
];

export default InstitutionCampusSearchRequestBase;
