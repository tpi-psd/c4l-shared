import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Note from "../Note";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class NoteBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {number} entityId (integer)
 * @property {'Person'|'Subject'|'Course'|'Question'|'Announcement'|'Issue'} entityType
 * @property {'EntityCreated'|'EntityModified'|'Note'} noteType
 * @property {string} data
 * @property {string} content
 * @property {number} latitude
 * @property {number} longitude
 * @property {FileAsset} fileAsset
 * @property {string} actionToken
 * @property {Person} person
 * @property {Date} dateCreated (date and time)
 * @property {Person} personEntity only used for creating a new note -- set if this Note is for a Person
 * @property {Subject} subjectEntity only used for creating a new note -- set if this Note is for a Subject
 * @property {Course} courseEntity only used for creating a new note -- set if this Note is for a Course
 * @property {Question} questionEntity only used for creating a new note -- set if this Note is for a Question
 * @property {Announcement} announcementEntity only used for creating a new note -- set if this Note is for a Announcement
 * @property {Issue} issueEntity only used for creating a new note -- set if this Note is for a Issue
 */
class NoteBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Note based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Note}
	 */
	static create(genericObject) {
		const newNote = new Note();
		newNote.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newNote;
	}

	/**
	 * Instantiates a new array of Note based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Note]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newNoteArray = [];
		genericArray.forEach(genericObject => {
			newNoteArray.push(Note.create(genericObject));
		});
		return newNoteArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('entityId', 'integer'),
	ModelBaseClass.createModelProperty('entityType', 'string'),
	ModelBaseClass.createModelProperty('noteType', 'string'),
	ModelBaseClass.createModelProperty('data', 'string'),
	ModelBaseClass.createModelProperty('content', 'string'),
	ModelBaseClass.createModelProperty('latitude', 'float'),
	ModelBaseClass.createModelProperty('longitude', 'float'),
	ModelBaseClass.createModelProperty('fileAsset', 'FileAsset'),
	ModelBaseClass.createModelProperty('actionToken', 'string'),
	ModelBaseClass.createModelProperty('person', 'Person'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
	ModelBaseClass.createModelProperty('personEntity', 'Person'),
	ModelBaseClass.createModelProperty('subjectEntity', 'Subject'),
	ModelBaseClass.createModelProperty('courseEntity', 'Course'),
	ModelBaseClass.createModelProperty('questionEntity', 'Question'),
	ModelBaseClass.createModelProperty('announcementEntity', 'Announcement'),
	ModelBaseClass.createModelProperty('issueEntity', 'Issue'),
];

export default NoteBase;
