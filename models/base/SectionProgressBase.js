import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import SectionProgress from "../SectionProgress";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class SectionProgressBase
 * @extends ModelBaseClass
 * @property {number} sectionId (int64)
 * @property {number} elapsedTime (integer) total time, in seconds
 * @property {number} viewCount
 */
class SectionProgressBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of SectionProgress based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {SectionProgress}
	 */
	static create(genericObject) {
		const newSectionProgress = new SectionProgress();
		newSectionProgress.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newSectionProgress;
	}

	/**
	 * Instantiates a new array of SectionProgress based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[SectionProgress]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newSectionProgressArray = [];
		genericArray.forEach(genericObject => {
			newSectionProgressArray.push(SectionProgress.create(genericObject));
		});
		return newSectionProgressArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('sectionId', 'integer'),
	ModelBaseClass.createModelProperty('elapsedTime', 'integer'),
	ModelBaseClass.createModelProperty('viewCount', 'float'),
];

export default SectionProgressBase;
