import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentRegisterSelfServiceCompletionRequest from "../StudentRegisterSelfServiceCompletionRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentRegisterSelfServiceCompletionRequestBase
 * @extends ModelBaseClass
 * @property {string} password
 * @property {Student} student
 * @property {PaymentRequest} paymentRequest
 */
class StudentRegisterSelfServiceCompletionRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentRegisterSelfServiceCompletionRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentRegisterSelfServiceCompletionRequest}
	 */
	static create(genericObject) {
		const newStudentRegisterSelfServiceCompletionRequest = new StudentRegisterSelfServiceCompletionRequest();
		newStudentRegisterSelfServiceCompletionRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentRegisterSelfServiceCompletionRequest;
	}

	/**
	 * Instantiates a new array of StudentRegisterSelfServiceCompletionRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentRegisterSelfServiceCompletionRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentRegisterSelfServiceCompletionRequestArray = [];
		genericArray.forEach(genericObject => {
			newStudentRegisterSelfServiceCompletionRequestArray.push(StudentRegisterSelfServiceCompletionRequest.create(genericObject));
		});
		return newStudentRegisterSelfServiceCompletionRequestArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('password', 'string'),
	ModelBaseClass.createModelProperty('student', 'Student'),
	ModelBaseClass.createModelProperty('paymentRequest', 'PaymentRequest'),
];

export default StudentRegisterSelfServiceCompletionRequestBase;
