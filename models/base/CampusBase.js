import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Campus from "../Campus";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class CampusBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {number} schoolId (int64)
 * @property {string} name
 * @property {string} address1
 * @property {string} address2
 * @property {string} city
 * @property {string} state
 * @property {string} zip
 * @property {string} phone
 * @property {number} userCount (integer)
 * @property {number} studentTotalCount (integer)
 * @property {number} studentGraduatedCount (integer)
 * @property {number} studentActiveCount (integer)
 * @property {number} studentEnrolledCount (integer)
 * @property {number} studentCancelledCount (integer)
 * @property {number} studentNeverLoggedInCount (integer)
 * @property {number} studentInactiveCount (integer)
 * @property {boolean} activeFlag
 * @property {Date} dateCreated (date and time)
 */
class CampusBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Campus based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Campus}
	 */
	static create(genericObject) {
		const newCampus = new Campus();
		newCampus.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newCampus;
	}

	/**
	 * Instantiates a new array of Campus based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Campus]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newCampusArray = [];
		genericArray.forEach(genericObject => {
			newCampusArray.push(Campus.create(genericObject));
		});
		return newCampusArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('schoolId', 'integer'),
	ModelBaseClass.createModelProperty('name', 'string'),
	ModelBaseClass.createModelProperty('address1', 'string'),
	ModelBaseClass.createModelProperty('address2', 'string'),
	ModelBaseClass.createModelProperty('city', 'string'),
	ModelBaseClass.createModelProperty('state', 'string'),
	ModelBaseClass.createModelProperty('zip', 'string'),
	ModelBaseClass.createModelProperty('phone', 'string'),
	ModelBaseClass.createModelProperty('userCount', 'integer'),
	ModelBaseClass.createModelProperty('studentTotalCount', 'integer'),
	ModelBaseClass.createModelProperty('studentGraduatedCount', 'integer'),
	ModelBaseClass.createModelProperty('studentActiveCount', 'integer'),
	ModelBaseClass.createModelProperty('studentEnrolledCount', 'integer'),
	ModelBaseClass.createModelProperty('studentCancelledCount', 'integer'),
	ModelBaseClass.createModelProperty('studentNeverLoggedInCount', 'integer'),
	ModelBaseClass.createModelProperty('studentInactiveCount', 'integer'),
	ModelBaseClass.createModelProperty('activeFlag', 'boolean'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
];

export default CampusBase;
