import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import IssueCategory from "../IssueCategory";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class IssueCategoryBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {string} name
 * @property {boolean} studentFlag
 * @property {number} orderNumber (integer)
 */
class IssueCategoryBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of IssueCategory based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {IssueCategory}
	 */
	static create(genericObject) {
		const newIssueCategory = new IssueCategory();
		newIssueCategory.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newIssueCategory;
	}

	/**
	 * Instantiates a new array of IssueCategory based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[IssueCategory]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newIssueCategoryArray = [];
		genericArray.forEach(genericObject => {
			newIssueCategoryArray.push(IssueCategory.create(genericObject));
		});
		return newIssueCategoryArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('name', 'string'),
	ModelBaseClass.createModelProperty('studentFlag', 'boolean'),
	ModelBaseClass.createModelProperty('orderNumber', 'integer'),
];

export default IssueCategoryBase;
