import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import CurriculumQuestionSaveRequest from "../CurriculumQuestionSaveRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class CurriculumQuestionSaveRequestBase
 * @extends ModelBaseClass
 * @property {Question} question
 * @property {boolean} resetQuestionStatisticsFlag
 */
class CurriculumQuestionSaveRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of CurriculumQuestionSaveRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {CurriculumQuestionSaveRequest}
	 */
	static create(genericObject) {
		const newCurriculumQuestionSaveRequest = new CurriculumQuestionSaveRequest();
		newCurriculumQuestionSaveRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newCurriculumQuestionSaveRequest;
	}

	/**
	 * Instantiates a new array of CurriculumQuestionSaveRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[CurriculumQuestionSaveRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newCurriculumQuestionSaveRequestArray = [];
		genericArray.forEach(genericObject => {
			newCurriculumQuestionSaveRequestArray.push(CurriculumQuestionSaveRequest.create(genericObject));
		});
		return newCurriculumQuestionSaveRequestArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('question', 'Question'),
	ModelBaseClass.createModelProperty('resetQuestionStatisticsFlag', 'boolean'),
];

export default CurriculumQuestionSaveRequestBase;
