import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import PersonAdminSearchResponse from "../PersonAdminSearchResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class PersonAdminSearchResponseBase
 * @extends ModelBaseClass
 * @property {[Person]} persons
 * @property {number} totalItemCount (int64)
 */
class PersonAdminSearchResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of PersonAdminSearchResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {PersonAdminSearchResponse}
	 */
	static create(genericObject) {
		const newPersonAdminSearchResponse = new PersonAdminSearchResponse();
		newPersonAdminSearchResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newPersonAdminSearchResponse;
	}

	/**
	 * Instantiates a new array of PersonAdminSearchResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[PersonAdminSearchResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newPersonAdminSearchResponseArray = [];
		genericArray.forEach(genericObject => {
			newPersonAdminSearchResponseArray.push(PersonAdminSearchResponse.create(genericObject));
		});
		return newPersonAdminSearchResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('persons', '[Person]'),
	ModelBaseClass.createModelProperty('totalItemCount', 'integer'),
];

export default PersonAdminSearchResponseBase;
