import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Chapter from "../Chapter";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class ChapterBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {number} courseId (int64)
 * @property {string} name
 * @property {number} orderNumber (integer)
 * @property {'None'|'Standard'|'Video'|'Written'|'LearningInventoryStudentType'|'LearningInventoryLearningStyle'|'CommunicationStyle'|'CareerInventoryInterestProfiler'|'CareerInventoryJobZone'} quizType
 * @property {string} quizName
 * @property {boolean} excludedFlag
 * @property {string} chapterNumberLabel
 * @property {number} finalExamBlueprint (integer)
 * @property {number} challengeExamBlueprint (integer)
 * @property {number} sectionCount (integer)
 * @property {number} questionCount (integer)
 * @property {boolean} activeFlag
 * @property {Date} dateCreated (date and time)
 * @property {[SectionName]} studentViewableSectionNames only set for when being queried by a student -- an ordered array of section id/name pairs that are viewable
 * @property {string} courseName only set for when in system settings
 */
class ChapterBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Chapter based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Chapter}
	 */
	static create(genericObject) {
		const newChapter = new Chapter();
		newChapter.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newChapter;
	}

	/**
	 * Instantiates a new array of Chapter based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Chapter]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newChapterArray = [];
		genericArray.forEach(genericObject => {
			newChapterArray.push(Chapter.create(genericObject));
		});
		return newChapterArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('courseId', 'integer'),
	ModelBaseClass.createModelProperty('name', 'string'),
	ModelBaseClass.createModelProperty('orderNumber', 'integer'),
	ModelBaseClass.createModelProperty('quizType', 'string'),
	ModelBaseClass.createModelProperty('quizName', 'string'),
	ModelBaseClass.createModelProperty('excludedFlag', 'boolean'),
	ModelBaseClass.createModelProperty('chapterNumberLabel', 'string'),
	ModelBaseClass.createModelProperty('finalExamBlueprint', 'integer'),
	ModelBaseClass.createModelProperty('challengeExamBlueprint', 'integer'),
	ModelBaseClass.createModelProperty('sectionCount', 'integer'),
	ModelBaseClass.createModelProperty('questionCount', 'integer'),
	ModelBaseClass.createModelProperty('activeFlag', 'boolean'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
	ModelBaseClass.createModelProperty('studentViewableSectionNames', '[SectionName]'),
	ModelBaseClass.createModelProperty('courseName', 'string'),
];

export default ChapterBase;
