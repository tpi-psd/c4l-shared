import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import AutoGradeLockoutSearchResponse from "../AutoGradeLockoutSearchResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class AutoGradeLockoutSearchResponseBase
 * @extends ModelBaseClass
 * @property {[Assessment]} assessments
 * @property {number} totalItemCount (int64)
 */
class AutoGradeLockoutSearchResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of AutoGradeLockoutSearchResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {AutoGradeLockoutSearchResponse}
	 */
	static create(genericObject) {
		const newAutoGradeLockoutSearchResponse = new AutoGradeLockoutSearchResponse();
		newAutoGradeLockoutSearchResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newAutoGradeLockoutSearchResponse;
	}

	/**
	 * Instantiates a new array of AutoGradeLockoutSearchResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[AutoGradeLockoutSearchResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newAutoGradeLockoutSearchResponseArray = [];
		genericArray.forEach(genericObject => {
			newAutoGradeLockoutSearchResponseArray.push(AutoGradeLockoutSearchResponse.create(genericObject));
		});
		return newAutoGradeLockoutSearchResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('assessments', '[Assessment]'),
	ModelBaseClass.createModelProperty('totalItemCount', 'integer'),
];

export default AutoGradeLockoutSearchResponseBase;
