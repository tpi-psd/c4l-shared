import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import TransferTranscriptAppliedCredit from "../TransferTranscriptAppliedCredit";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class TransferTranscriptAppliedCreditBase
 * @extends ModelBaseClass
 * @property {number} transferTranscriptId (int64)
 * @property {number} subjectId (int64)
 * @property {number} courseId (int64)
 * @property {number} studentId (int64)
 * @property {string} grade A+, A, A-, B+, B, B-, C+, C, C-, D+, D, D-, P
 */
class TransferTranscriptAppliedCreditBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of TransferTranscriptAppliedCredit based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {TransferTranscriptAppliedCredit}
	 */
	static create(genericObject) {
		const newTransferTranscriptAppliedCredit = new TransferTranscriptAppliedCredit();
		newTransferTranscriptAppliedCredit.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newTransferTranscriptAppliedCredit;
	}

	/**
	 * Instantiates a new array of TransferTranscriptAppliedCredit based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[TransferTranscriptAppliedCredit]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newTransferTranscriptAppliedCreditArray = [];
		genericArray.forEach(genericObject => {
			newTransferTranscriptAppliedCreditArray.push(TransferTranscriptAppliedCredit.create(genericObject));
		});
		return newTransferTranscriptAppliedCreditArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('transferTranscriptId', 'integer'),
	ModelBaseClass.createModelProperty('subjectId', 'integer'),
	ModelBaseClass.createModelProperty('courseId', 'integer'),
	ModelBaseClass.createModelProperty('studentId', 'integer'),
	ModelBaseClass.createModelProperty('grade', 'string'),
];

export default TransferTranscriptAppliedCreditBase;
