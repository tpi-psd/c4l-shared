import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentCourseProgressResponse from "../StudentCourseProgressResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentCourseProgressResponseBase
 * @extends ModelBaseClass
 * @property {[CourseProgress]} courseProgressArray
 * @property {[SubjectTimeTracking]} subjectTimeTrackingArray
 */
class StudentCourseProgressResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentCourseProgressResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentCourseProgressResponse}
	 */
	static create(genericObject) {
		const newStudentCourseProgressResponse = new StudentCourseProgressResponse();
		newStudentCourseProgressResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentCourseProgressResponse;
	}

	/**
	 * Instantiates a new array of StudentCourseProgressResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentCourseProgressResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentCourseProgressResponseArray = [];
		genericArray.forEach(genericObject => {
			newStudentCourseProgressResponseArray.push(StudentCourseProgressResponse.create(genericObject));
		});
		return newStudentCourseProgressResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('courseProgressArray', '[CourseProgress]'),
	ModelBaseClass.createModelProperty('subjectTimeTrackingArray', '[SubjectTimeTracking]'),
];

export default StudentCourseProgressResponseBase;
