import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import PersonDashboardResponse from "../PersonDashboardResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class PersonDashboardResponseBase
 * @extends ModelBaseClass
 * @property {number} totalActive (integer)
 * @property {number} totalGraduated (integer)
 * @property {number} totalEnrolled (integer)
 * @property {number} totalInactive (integer)
 * @property {[School]} schools
 */
class PersonDashboardResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of PersonDashboardResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {PersonDashboardResponse}
	 */
	static create(genericObject) {
		const newPersonDashboardResponse = new PersonDashboardResponse();
		newPersonDashboardResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newPersonDashboardResponse;
	}

	/**
	 * Instantiates a new array of PersonDashboardResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[PersonDashboardResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newPersonDashboardResponseArray = [];
		genericArray.forEach(genericObject => {
			newPersonDashboardResponseArray.push(PersonDashboardResponse.create(genericObject));
		});
		return newPersonDashboardResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('totalActive', 'integer'),
	ModelBaseClass.createModelProperty('totalGraduated', 'integer'),
	ModelBaseClass.createModelProperty('totalEnrolled', 'integer'),
	ModelBaseClass.createModelProperty('totalInactive', 'integer'),
	ModelBaseClass.createModelProperty('schools', '[School]'),
];

export default PersonDashboardResponseBase;
