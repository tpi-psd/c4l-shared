import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import WrittenQuizAnswerSearchResponse from "../WrittenQuizAnswerSearchResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class WrittenQuizAnswerSearchResponseBase
 * @extends ModelBaseClass
 * @property {[Assessment]} assessments
 * @property {number} totalItemCount (int64)
 */
class WrittenQuizAnswerSearchResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of WrittenQuizAnswerSearchResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {WrittenQuizAnswerSearchResponse}
	 */
	static create(genericObject) {
		const newWrittenQuizAnswerSearchResponse = new WrittenQuizAnswerSearchResponse();
		newWrittenQuizAnswerSearchResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newWrittenQuizAnswerSearchResponse;
	}

	/**
	 * Instantiates a new array of WrittenQuizAnswerSearchResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[WrittenQuizAnswerSearchResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newWrittenQuizAnswerSearchResponseArray = [];
		genericArray.forEach(genericObject => {
			newWrittenQuizAnswerSearchResponseArray.push(WrittenQuizAnswerSearchResponse.create(genericObject));
		});
		return newWrittenQuizAnswerSearchResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('assessments', '[Assessment]'),
	ModelBaseClass.createModelProperty('totalItemCount', 'integer'),
];

export default WrittenQuizAnswerSearchResponseBase;
