import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import TranscriptRequestSearchRequest from "../TranscriptRequestSearchRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class TranscriptRequestSearchRequestBase
 * @extends ModelBaseClass
 * @property {string} smartFilter can be student first name, student last name, or student's studentIdentifierLabel
 * @property {'Pending'|'Printed'|'Sent'|'NotSent'} filterByStatus
 * @property {ResultParameter} resultParameter [StudentIdentifierLabel, Student, DateRequested, Status]
 */
class TranscriptRequestSearchRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of TranscriptRequestSearchRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {TranscriptRequestSearchRequest}
	 */
	static create(genericObject) {
		const newTranscriptRequestSearchRequest = new TranscriptRequestSearchRequest();
		newTranscriptRequestSearchRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newTranscriptRequestSearchRequest;
	}

	/**
	 * Instantiates a new array of TranscriptRequestSearchRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[TranscriptRequestSearchRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newTranscriptRequestSearchRequestArray = [];
		genericArray.forEach(genericObject => {
			newTranscriptRequestSearchRequestArray.push(TranscriptRequestSearchRequest.create(genericObject));
		});
		return newTranscriptRequestSearchRequestArray;
	}
}

/**
 * @type {string} OrderByStudentIdentifierLabel
 */
TranscriptRequestSearchRequestBase.OrderByStudentIdentifierLabel = 'studentidentifierlabel';

/**
 * @type {string} OrderByStudent
 */
TranscriptRequestSearchRequestBase.OrderByStudent = 'student';

/**
 * @type {string} OrderByDateRequested
 */
TranscriptRequestSearchRequestBase.OrderByDateRequested = 'daterequested';

/**
 * @type {string} OrderByStatus
 */
TranscriptRequestSearchRequestBase.OrderByStatus = 'status';

const _modelDefinition = [
	ModelBaseClass.createModelProperty('smartFilter', 'string'),
	ModelBaseClass.createModelProperty('filterByStatus', 'string'),
	ModelBaseClass.createModelProperty('resultParameter', 'ResultParameter'),
];

export default TranscriptRequestSearchRequestBase;
