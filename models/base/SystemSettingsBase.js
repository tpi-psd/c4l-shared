import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import SystemSettings from "../SystemSettings";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class SystemSettingsBase
 * @extends ModelBaseClass
 * @property {[School]} schools only for Admin
 * @property {[IssueCategory]} issueCategories only for Admin
 * @property {[Chapter]} writtenQuizChapters only for Admin
 * @property {[Subject]} subjects
 * @property {number} finalExamTimeLimit (integer) only for student -- the number of minutes you get to take a Final Exam
 * @property {number} challengeExamTimeLimit (integer) only for student -- the number of minutes you get to take a Challenge Exam
 * @property {number} timeoutWarning (integer) only for student -- the number of minutes before the timeout warning should be displayed
 * @property {number} monthlySubscriptionCost the cost of the monthly subscription -- to be used when displaying the cost to a user
 * @property {number} transcriptShippingCost the cost of shipping for a transcript request -- to be used when displaying the cost to a user
 */
class SystemSettingsBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of SystemSettings based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {SystemSettings}
	 */
	static create(genericObject) {
		const newSystemSettings = new SystemSettings();
		newSystemSettings.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newSystemSettings;
	}

	/**
	 * Instantiates a new array of SystemSettings based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[SystemSettings]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newSystemSettingsArray = [];
		genericArray.forEach(genericObject => {
			newSystemSettingsArray.push(SystemSettings.create(genericObject));
		});
		return newSystemSettingsArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('schools', '[School]'),
	ModelBaseClass.createModelProperty('issueCategories', '[IssueCategory]'),
	ModelBaseClass.createModelProperty('writtenQuizChapters', '[Chapter]'),
	ModelBaseClass.createModelProperty('subjects', '[Subject]'),
	ModelBaseClass.createModelProperty('finalExamTimeLimit', 'integer'),
	ModelBaseClass.createModelProperty('challengeExamTimeLimit', 'integer'),
	ModelBaseClass.createModelProperty('timeoutWarning', 'integer'),
	ModelBaseClass.createModelProperty('monthlySubscriptionCost', 'float'),
	ModelBaseClass.createModelProperty('transcriptShippingCost', 'float'),
];

export default SystemSettingsBase;
