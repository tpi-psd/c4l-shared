import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import InstitutionSchoolSearchResponse from "../InstitutionSchoolSearchResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class InstitutionSchoolSearchResponseBase
 * @extends ModelBaseClass
 * @property {[School]} schools
 * @property {number} totalItemCount (int64)
 */
class InstitutionSchoolSearchResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of InstitutionSchoolSearchResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {InstitutionSchoolSearchResponse}
	 */
	static create(genericObject) {
		const newInstitutionSchoolSearchResponse = new InstitutionSchoolSearchResponse();
		newInstitutionSchoolSearchResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newInstitutionSchoolSearchResponse;
	}

	/**
	 * Instantiates a new array of InstitutionSchoolSearchResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[InstitutionSchoolSearchResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newInstitutionSchoolSearchResponseArray = [];
		genericArray.forEach(genericObject => {
			newInstitutionSchoolSearchResponseArray.push(InstitutionSchoolSearchResponse.create(genericObject));
		});
		return newInstitutionSchoolSearchResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('schools', '[School]'),
	ModelBaseClass.createModelProperty('totalItemCount', 'integer'),
];

export default InstitutionSchoolSearchResponseBase;
