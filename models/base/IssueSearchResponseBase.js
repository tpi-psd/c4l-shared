import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import IssueSearchResponse from "../IssueSearchResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class IssueSearchResponseBase
 * @extends ModelBaseClass
 * @property {[Issue]} issues
 * @property {number} totalItemCount (int64)
 */
class IssueSearchResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of IssueSearchResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {IssueSearchResponse}
	 */
	static create(genericObject) {
		const newIssueSearchResponse = new IssueSearchResponse();
		newIssueSearchResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newIssueSearchResponse;
	}

	/**
	 * Instantiates a new array of IssueSearchResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[IssueSearchResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newIssueSearchResponseArray = [];
		genericArray.forEach(genericObject => {
			newIssueSearchResponseArray.push(IssueSearchResponse.create(genericObject));
		});
		return newIssueSearchResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('issues', '[Issue]'),
	ModelBaseClass.createModelProperty('totalItemCount', 'integer'),
];

export default IssueSearchResponseBase;
