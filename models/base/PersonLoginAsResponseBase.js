import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import PersonLoginAsResponse from "../PersonLoginAsResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class PersonLoginAsResponseBase
 * @extends ModelBaseClass
 * @property {string} url the URL to redirect the user to
 */
class PersonLoginAsResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of PersonLoginAsResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {PersonLoginAsResponse}
	 */
	static create(genericObject) {
		const newPersonLoginAsResponse = new PersonLoginAsResponse();
		newPersonLoginAsResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newPersonLoginAsResponse;
	}

	/**
	 * Instantiates a new array of PersonLoginAsResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[PersonLoginAsResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newPersonLoginAsResponseArray = [];
		genericArray.forEach(genericObject => {
			newPersonLoginAsResponseArray.push(PersonLoginAsResponse.create(genericObject));
		});
		return newPersonLoginAsResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('url', 'string'),
];

export default PersonLoginAsResponseBase;
