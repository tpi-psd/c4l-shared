import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentRegisterCampusAffiliatedInitiationRequest from "../StudentRegisterCampusAffiliatedInitiationRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentRegisterCampusAffiliatedInitiationRequestBase
 * @extends ModelBaseClass
 * @property {string} email
 * @property {number} schoolId (int64) optional
 */
class StudentRegisterCampusAffiliatedInitiationRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentRegisterCampusAffiliatedInitiationRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentRegisterCampusAffiliatedInitiationRequest}
	 */
	static create(genericObject) {
		const newStudentRegisterCampusAffiliatedInitiationRequest = new StudentRegisterCampusAffiliatedInitiationRequest();
		newStudentRegisterCampusAffiliatedInitiationRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentRegisterCampusAffiliatedInitiationRequest;
	}

	/**
	 * Instantiates a new array of StudentRegisterCampusAffiliatedInitiationRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentRegisterCampusAffiliatedInitiationRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentRegisterCampusAffiliatedInitiationRequestArray = [];
		genericArray.forEach(genericObject => {
			newStudentRegisterCampusAffiliatedInitiationRequestArray.push(StudentRegisterCampusAffiliatedInitiationRequest.create(genericObject));
		});
		return newStudentRegisterCampusAffiliatedInitiationRequestArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('email', 'string'),
	ModelBaseClass.createModelProperty('schoolId', 'integer'),
];

export default StudentRegisterCampusAffiliatedInitiationRequestBase;
