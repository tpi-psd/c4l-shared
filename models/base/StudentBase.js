import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Student from "../Student";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentBase
 * @extends ModelBaseClass
 * @property {number} schoolId (int64) always set for campus-affiliated students
 * @property {number} campusId (int64) always set for campus-affiliated students
 * @property {number} currentSubscriptionId (int64) always set for self-service students
 * @property {string} schoolStudentIdentifier this is the school-assigned Student ID
 * @property {string} c4lStudentIdentifier this is the c4l-assigned Student ID
 * @property {string} studentIdentifierLabel this will be the appropriate studentIdentifier based on if this is a campus-affiliated or self-service
 * @property {string} address1
 * @property {string} address2
 * @property {string} city
 * @property {string} state
 * @property {string} zip
 * @property {Date} dateOfBirth (date only)
 * @property {'Male'|'Female'|'Nonbinary'|'PreferNotToDisclose'} gender
 * @property {string} ssnLast4
 * @property {boolean} ofInterestFlag
 * @property {boolean} introductionCompleteFlag
 * @property {'Pending'|'Enrolled'|'Active'|'Graduated'|'Cancelled'|'Expired'} status
 * @property {['StudentOfInterest'|'HasOpenIssue'|'HasPendingTranscript']} alerts
 * @property {number} completionPercentage
 * @property {Date} dateStart (date only)
 * @property {Date} dateCancelled (date only)
 * @property {Date} dateGraduated (date only)
 * @property {StudentPreferences} preferences only set when called [via /person/get/{id} or via Session object]
 * @property {Subscription} currentSubscription only set when called [via /person/get/{id} or via Session object] AND if student is a self-service registrant
 * @property {School} school only set when called [via /person/get/{id} or via Session object] AND if student is campus/school affiliated
 * @property {Campus} campus only set when called [via /person/get/{id} or via Session object] AND if student is campus/school affiliated
 * @property {number} courseCompletedCount (integer) only set when called [via /person/get/{id} or via Session object]
 * @property {number} courseRequiredCount (integer) only set when called [via /person/get/{id} or via Session object]
 * @property {number} openIssueId (int64) only set when called [via /person/get/{id} or via Session object] AND there is one (and only one) open issue for this student
 */
class StudentBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Student based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Student}
	 */
	static create(genericObject) {
		const newStudent = new Student();
		newStudent.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudent;
	}

	/**
	 * Instantiates a new array of Student based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Student]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentArray = [];
		genericArray.forEach(genericObject => {
			newStudentArray.push(Student.create(genericObject));
		});
		return newStudentArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('schoolId', 'integer'),
	ModelBaseClass.createModelProperty('campusId', 'integer'),
	ModelBaseClass.createModelProperty('currentSubscriptionId', 'integer'),
	ModelBaseClass.createModelProperty('schoolStudentIdentifier', 'string'),
	ModelBaseClass.createModelProperty('c4lStudentIdentifier', 'string'),
	ModelBaseClass.createModelProperty('studentIdentifierLabel', 'string'),
	ModelBaseClass.createModelProperty('address1', 'string'),
	ModelBaseClass.createModelProperty('address2', 'string'),
	ModelBaseClass.createModelProperty('city', 'string'),
	ModelBaseClass.createModelProperty('state', 'string'),
	ModelBaseClass.createModelProperty('zip', 'string'),
	ModelBaseClass.createModelProperty('dateOfBirth', 'datetime'),
	ModelBaseClass.createModelProperty('gender', 'string'),
	ModelBaseClass.createModelProperty('ssnLast4', 'string'),
	ModelBaseClass.createModelProperty('ofInterestFlag', 'boolean'),
	ModelBaseClass.createModelProperty('introductionCompleteFlag', 'boolean'),
	ModelBaseClass.createModelProperty('status', 'string'),
	ModelBaseClass.createModelProperty('alerts', '[string]'),
	ModelBaseClass.createModelProperty('completionPercentage', 'float'),
	ModelBaseClass.createModelProperty('dateStart', 'datetime'),
	ModelBaseClass.createModelProperty('dateCancelled', 'datetime'),
	ModelBaseClass.createModelProperty('dateGraduated', 'datetime'),
	ModelBaseClass.createModelProperty('preferences', 'StudentPreferences'),
	ModelBaseClass.createModelProperty('currentSubscription', 'Subscription'),
	ModelBaseClass.createModelProperty('school', 'School'),
	ModelBaseClass.createModelProperty('campus', 'Campus'),
	ModelBaseClass.createModelProperty('courseCompletedCount', 'integer'),
	ModelBaseClass.createModelProperty('courseRequiredCount', 'integer'),
	ModelBaseClass.createModelProperty('openIssueId', 'integer'),
];

export default StudentBase;
