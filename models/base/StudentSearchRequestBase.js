import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentSearchRequest from "../StudentSearchRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentSearchRequestBase
 * @extends ModelBaseClass
 * @property {string} smartFilter
 * @property {'NeverLoggedIn'|'StudentOfInterest'|'HasOpenIssue'|'HasPendingTranscript'|'Inactive7Days'} filterByStudentAlertType
 * @property {'Pending'|'Enrolled'|'Active'|'Graduated'|'Cancelled'|'Expired'} filterByStatus
 * @property {number} filterBySchoolId (int64) or -1 to filter for Self-Service students
 * @property {number} filterByCampusId (int64)
 * @property {ResultParameter} resultParameter [DateCreated, DateStart, DateLastLogin, StudentIdentifierLabel, FirstName, LastName, Email, School, Campus, Status, CompletionPercentage]
 */
class StudentSearchRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentSearchRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentSearchRequest}
	 */
	static create(genericObject) {
		const newStudentSearchRequest = new StudentSearchRequest();
		newStudentSearchRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentSearchRequest;
	}

	/**
	 * Instantiates a new array of StudentSearchRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentSearchRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentSearchRequestArray = [];
		genericArray.forEach(genericObject => {
			newStudentSearchRequestArray.push(StudentSearchRequest.create(genericObject));
		});
		return newStudentSearchRequestArray;
	}
}

/**
 * @type {string} OrderByDateCreated
 */
StudentSearchRequestBase.OrderByDateCreated = 'datecreated';

/**
 * @type {string} OrderByDateStart
 */
StudentSearchRequestBase.OrderByDateStart = 'datestart';

/**
 * @type {string} OrderByDateLastLogin
 */
StudentSearchRequestBase.OrderByDateLastLogin = 'datelastlogin';

/**
 * @type {string} OrderByStudentIdentifierLabel
 */
StudentSearchRequestBase.OrderByStudentIdentifierLabel = 'studentidentifierlabel';

/**
 * @type {string} OrderByFirstName
 */
StudentSearchRequestBase.OrderByFirstName = 'firstname';

/**
 * @type {string} OrderByLastName
 */
StudentSearchRequestBase.OrderByLastName = 'lastname';

/**
 * @type {string} OrderByEmail
 */
StudentSearchRequestBase.OrderByEmail = 'email';

/**
 * @type {string} OrderBySchool
 */
StudentSearchRequestBase.OrderBySchool = 'school';

/**
 * @type {string} OrderByCampus
 */
StudentSearchRequestBase.OrderByCampus = 'campus';

/**
 * @type {string} OrderByStatus
 */
StudentSearchRequestBase.OrderByStatus = 'status';

/**
 * @type {string} OrderByCompletionPercentage
 */
StudentSearchRequestBase.OrderByCompletionPercentage = 'completionpercentage';

const _modelDefinition = [
	ModelBaseClass.createModelProperty('smartFilter', 'string'),
	ModelBaseClass.createModelProperty('filterByStudentAlertType', 'string'),
	ModelBaseClass.createModelProperty('filterByStatus', 'string'),
	ModelBaseClass.createModelProperty('filterBySchoolId', 'integer'),
	ModelBaseClass.createModelProperty('filterByCampusId', 'integer'),
	ModelBaseClass.createModelProperty('resultParameter', 'ResultParameter'),
];

export default StudentSearchRequestBase;
