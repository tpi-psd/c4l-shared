import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import PersonLoginRequest from "../PersonLoginRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class PersonLoginRequestBase
 * @extends ModelBaseClass
 * @property {string} email
 * @property {string} password
 */
class PersonLoginRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of PersonLoginRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {PersonLoginRequest}
	 */
	static create(genericObject) {
		const newPersonLoginRequest = new PersonLoginRequest();
		newPersonLoginRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newPersonLoginRequest;
	}

	/**
	 * Instantiates a new array of PersonLoginRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[PersonLoginRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newPersonLoginRequestArray = [];
		genericArray.forEach(genericObject => {
			newPersonLoginRequestArray.push(PersonLoginRequest.create(genericObject));
		});
		return newPersonLoginRequestArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('email', 'string'),
	ModelBaseClass.createModelProperty('password', 'string'),
];

export default PersonLoginRequestBase;
