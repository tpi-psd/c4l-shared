import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Subscription from "../Subscription";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class SubscriptionBase
 * @extends ModelBaseClass
 * @property {PaymentMethod} paymentMethod
 * @property {Person} cancelledByPerson Only set if student's subscription was explicitly cancelled by either a C4L User or by the Student themself.  When a subscription is cancelled, the student status is always set to Expired immediately (if cancelled by C4L), it it will be set to Expired when we hit datePaidUntil.
 * @property {Date} dateCancelled (date only) date of cancellation OR expiration (if payment for next month's subscription failed).  When a subscription is cancelled, the student status is always set to Expired immediately (if cancelled by C4L), it it will be set to Expired when we hit datePaidUntil.
 * @property {Date} datePaidUntil (date only)
 * @property {Date} dateCreated (date and time)
 */
class SubscriptionBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Subscription based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Subscription}
	 */
	static create(genericObject) {
		const newSubscription = new Subscription();
		newSubscription.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newSubscription;
	}

	/**
	 * Instantiates a new array of Subscription based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Subscription]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newSubscriptionArray = [];
		genericArray.forEach(genericObject => {
			newSubscriptionArray.push(Subscription.create(genericObject));
		});
		return newSubscriptionArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('paymentMethod', 'PaymentMethod'),
	ModelBaseClass.createModelProperty('cancelledByPerson', 'Person'),
	ModelBaseClass.createModelProperty('dateCancelled', 'datetime'),
	ModelBaseClass.createModelProperty('datePaidUntil', 'datetime'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
];

export default SubscriptionBase;
