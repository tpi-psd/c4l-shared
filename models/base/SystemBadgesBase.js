import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import SystemBadges from "../SystemBadges";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class SystemBadgesBase
 * @extends ModelBaseClass
 * @property {number} studentBadge (integer)
 * @property {number} issueBadge (integer)
 * @property {number} transcriptRequestBadge (integer)
 * @property {number} writtenQuizAnswerBadge (integer)
 * @property {boolean} studentAnnouncementFlag
 */
class SystemBadgesBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of SystemBadges based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {SystemBadges}
	 */
	static create(genericObject) {
		const newSystemBadges = new SystemBadges();
		newSystemBadges.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newSystemBadges;
	}

	/**
	 * Instantiates a new array of SystemBadges based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[SystemBadges]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newSystemBadgesArray = [];
		genericArray.forEach(genericObject => {
			newSystemBadgesArray.push(SystemBadges.create(genericObject));
		});
		return newSystemBadgesArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('studentBadge', 'integer'),
	ModelBaseClass.createModelProperty('issueBadge', 'integer'),
	ModelBaseClass.createModelProperty('transcriptRequestBadge', 'integer'),
	ModelBaseClass.createModelProperty('writtenQuizAnswerBadge', 'integer'),
	ModelBaseClass.createModelProperty('studentAnnouncementFlag', 'boolean'),
];

export default SystemBadgesBase;
