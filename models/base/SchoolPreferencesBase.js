import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import SchoolPreferences from "../SchoolPreferences";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class SchoolPreferencesBase
 * @extends ModelBaseClass
 * @property {'Full'|'Half'} monthlyBillingType
 * @property {number} firstHalfDays (integer)
 * @property {number} secondHalfDays (integer)
 * @property {number} fullDays (integer)
 */
class SchoolPreferencesBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of SchoolPreferences based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {SchoolPreferences}
	 */
	static create(genericObject) {
		const newSchoolPreferences = new SchoolPreferences();
		newSchoolPreferences.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newSchoolPreferences;
	}

	/**
	 * Instantiates a new array of SchoolPreferences based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[SchoolPreferences]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newSchoolPreferencesArray = [];
		genericArray.forEach(genericObject => {
			newSchoolPreferencesArray.push(SchoolPreferences.create(genericObject));
		});
		return newSchoolPreferencesArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('monthlyBillingType', 'string'),
	ModelBaseClass.createModelProperty('firstHalfDays', 'integer'),
	ModelBaseClass.createModelProperty('secondHalfDays', 'integer'),
	ModelBaseClass.createModelProperty('fullDays', 'integer'),
];

export default SchoolPreferencesBase;
