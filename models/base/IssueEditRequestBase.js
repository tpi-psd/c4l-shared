import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import IssueEditRequest from "../IssueEditRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class IssueEditRequestBase
 * @extends ModelBaseClass
 * @property {number} issueId (int64)
 * @property {'Open'|'Resolved'|'Closed'} status
 * @property {string} note
 */
class IssueEditRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of IssueEditRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {IssueEditRequest}
	 */
	static create(genericObject) {
		const newIssueEditRequest = new IssueEditRequest();
		newIssueEditRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newIssueEditRequest;
	}

	/**
	 * Instantiates a new array of IssueEditRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[IssueEditRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newIssueEditRequestArray = [];
		genericArray.forEach(genericObject => {
			newIssueEditRequestArray.push(IssueEditRequest.create(genericObject));
		});
		return newIssueEditRequestArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('issueId', 'integer'),
	ModelBaseClass.createModelProperty('status', 'string'),
	ModelBaseClass.createModelProperty('note', 'string'),
];

export default IssueEditRequestBase;
