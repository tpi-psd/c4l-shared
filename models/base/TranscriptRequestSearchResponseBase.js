import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import TranscriptRequestSearchResponse from "../TranscriptRequestSearchResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class TranscriptRequestSearchResponseBase
 * @extends ModelBaseClass
 * @property {[TranscriptRequest]} transcriptRequests
 * @property {number} totalItemCount (int64)
 */
class TranscriptRequestSearchResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of TranscriptRequestSearchResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {TranscriptRequestSearchResponse}
	 */
	static create(genericObject) {
		const newTranscriptRequestSearchResponse = new TranscriptRequestSearchResponse();
		newTranscriptRequestSearchResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newTranscriptRequestSearchResponse;
	}

	/**
	 * Instantiates a new array of TranscriptRequestSearchResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[TranscriptRequestSearchResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newTranscriptRequestSearchResponseArray = [];
		genericArray.forEach(genericObject => {
			newTranscriptRequestSearchResponseArray.push(TranscriptRequestSearchResponse.create(genericObject));
		});
		return newTranscriptRequestSearchResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('transcriptRequests', '[TranscriptRequest]'),
	ModelBaseClass.createModelProperty('totalItemCount', 'integer'),
];

export default TranscriptRequestSearchResponseBase;
