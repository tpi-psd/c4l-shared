import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import RenderedQuestion from "../RenderedQuestion";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class RenderedQuestionBase
 * @extends ModelBaseClass
 * @property {number} questionId (int64) artificially set set to a negative number) if migrated
 * @property {boolean} legacyFlag only set and set to true if this is a legacy assessment that was migrated
 * @property {Section} section optional, only set if the question is associated with a specific question and only set when calling /student/assessment/get
 * @property {number} questionNumber (integer)
 * @property {string} content in markdown format
 * @property {StudentAnswer} legacyStudentAnswer only set if migrated
 * @property {[RenderedAnswerChoice]} renderedAnswerChoices not available on written quizzes
 */
class RenderedQuestionBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of RenderedQuestion based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {RenderedQuestion}
	 */
	static create(genericObject) {
		const newRenderedQuestion = new RenderedQuestion();
		newRenderedQuestion.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newRenderedQuestion;
	}

	/**
	 * Instantiates a new array of RenderedQuestion based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[RenderedQuestion]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newRenderedQuestionArray = [];
		genericArray.forEach(genericObject => {
			newRenderedQuestionArray.push(RenderedQuestion.create(genericObject));
		});
		return newRenderedQuestionArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('questionId', 'integer'),
	ModelBaseClass.createModelProperty('legacyFlag', 'boolean'),
	ModelBaseClass.createModelProperty('section', 'Section'),
	ModelBaseClass.createModelProperty('questionNumber', 'integer'),
	ModelBaseClass.createModelProperty('content', 'string'),
	ModelBaseClass.createModelProperty('legacyStudentAnswer', 'StudentAnswer'),
	ModelBaseClass.createModelProperty('renderedAnswerChoices', '[RenderedAnswerChoice]'),
];

export default RenderedQuestionBase;
