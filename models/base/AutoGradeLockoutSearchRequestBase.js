import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import AutoGradeLockoutSearchRequest from "../AutoGradeLockoutSearchRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class AutoGradeLockoutSearchRequestBase
 * @extends ModelBaseClass
 * @property {string} smartFilter can be student first name, student last name, or student's studentIdentifierLabel
 * @property {'Locked'|'Unlocked'|'Relocked'} filterByLockedStatus
 * @property {number} filterBySchoolId (int64)
 * @property {ResultParameter} resultParameter [Student, StudentIdentifier, Campus, Type, Course, LockedStatus, DateLocked, DateUnlocked, UnlockedBy, DateRelocked]
 */
class AutoGradeLockoutSearchRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of AutoGradeLockoutSearchRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {AutoGradeLockoutSearchRequest}
	 */
	static create(genericObject) {
		const newAutoGradeLockoutSearchRequest = new AutoGradeLockoutSearchRequest();
		newAutoGradeLockoutSearchRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newAutoGradeLockoutSearchRequest;
	}

	/**
	 * Instantiates a new array of AutoGradeLockoutSearchRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[AutoGradeLockoutSearchRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newAutoGradeLockoutSearchRequestArray = [];
		genericArray.forEach(genericObject => {
			newAutoGradeLockoutSearchRequestArray.push(AutoGradeLockoutSearchRequest.create(genericObject));
		});
		return newAutoGradeLockoutSearchRequestArray;
	}
}

/**
 * @type {string} OrderByStudent
 */
AutoGradeLockoutSearchRequestBase.OrderByStudent = 'student';

/**
 * @type {string} OrderByStudentIdentifier
 */
AutoGradeLockoutSearchRequestBase.OrderByStudentIdentifier = 'studentidentifier';

/**
 * @type {string} OrderByCampus
 */
AutoGradeLockoutSearchRequestBase.OrderByCampus = 'campus';

/**
 * @type {string} OrderByType
 */
AutoGradeLockoutSearchRequestBase.OrderByType = 'type';

/**
 * @type {string} OrderByCourse
 */
AutoGradeLockoutSearchRequestBase.OrderByCourse = 'course';

/**
 * @type {string} OrderByLockedStatus
 */
AutoGradeLockoutSearchRequestBase.OrderByLockedStatus = 'lockedstatus';

/**
 * @type {string} OrderByDateLocked
 */
AutoGradeLockoutSearchRequestBase.OrderByDateLocked = 'datelocked';

/**
 * @type {string} OrderByDateUnlocked
 */
AutoGradeLockoutSearchRequestBase.OrderByDateUnlocked = 'dateunlocked';

/**
 * @type {string} OrderByUnlockedBy
 */
AutoGradeLockoutSearchRequestBase.OrderByUnlockedBy = 'unlockedby';

/**
 * @type {string} OrderByDateRelocked
 */
AutoGradeLockoutSearchRequestBase.OrderByDateRelocked = 'daterelocked';

const _modelDefinition = [
	ModelBaseClass.createModelProperty('smartFilter', 'string'),
	ModelBaseClass.createModelProperty('filterByLockedStatus', 'string'),
	ModelBaseClass.createModelProperty('filterBySchoolId', 'integer'),
	ModelBaseClass.createModelProperty('resultParameter', 'ResultParameter'),
];

export default AutoGradeLockoutSearchRequestBase;
