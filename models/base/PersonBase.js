import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Person from "../Person";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class PersonBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {'C4LManager'|'C4LStaff'|'InstitutionManager'|'InstitutionStaff'|'Student'} role
 * @property {string} firstName
 * @property {string} lastName
 * @property {string} phone
 * @property {string} email
 * @property {Student} student this will be NULL if the person is a self-service student registration that hasn't finished the signup process
 * @property {InstitutionUser} institutionUser
 * @property {boolean} activeFlag
 * @property {Date} dateCreated (date and time)
 * @property {Date} dateLastLogin (date and time) can be null if this person has never logged in before
 */
class PersonBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Person based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Person}
	 */
	static create(genericObject) {
		const newPerson = new Person();
		newPerson.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newPerson;
	}

	/**
	 * Instantiates a new array of Person based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Person]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newPersonArray = [];
		genericArray.forEach(genericObject => {
			newPersonArray.push(Person.create(genericObject));
		});
		return newPersonArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('role', 'string'),
	ModelBaseClass.createModelProperty('firstName', 'string'),
	ModelBaseClass.createModelProperty('lastName', 'string'),
	ModelBaseClass.createModelProperty('phone', 'string'),
	ModelBaseClass.createModelProperty('email', 'string'),
	ModelBaseClass.createModelProperty('student', 'Student'),
	ModelBaseClass.createModelProperty('institutionUser', 'InstitutionUser'),
	ModelBaseClass.createModelProperty('activeFlag', 'boolean'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
	ModelBaseClass.createModelProperty('dateLastLogin', 'datetime'),
];

export default PersonBase;
