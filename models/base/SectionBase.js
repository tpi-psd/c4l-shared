import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Section from "../Section";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class SectionBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {number} chapterId (int64)
 * @property {string} name
 * @property {number} orderNumber (integer)
 * @property {number} sectionNumberLabel (integer)
 * @property {string} content in markdown format
 * @property {boolean} activeFlag
 * @property {Date} dateCreated (date and time)
 * @property {string} saveNote only used when saving a section, the change/save note is specified here
 * @property {Section} previousSection only set when calling /student/course/view_section/{section_id}
 * @property {Section} nextSection only set when calling /student/course/view_section/{section_id}
 * @property {Chapter} chapter only set when calling /student/course/view_section/{section_id}
 */
class SectionBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Section based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Section}
	 */
	static create(genericObject) {
		const newSection = new Section();
		newSection.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newSection;
	}

	/**
	 * Instantiates a new array of Section based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Section]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newSectionArray = [];
		genericArray.forEach(genericObject => {
			newSectionArray.push(Section.create(genericObject));
		});
		return newSectionArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('chapterId', 'integer'),
	ModelBaseClass.createModelProperty('name', 'string'),
	ModelBaseClass.createModelProperty('orderNumber', 'integer'),
	ModelBaseClass.createModelProperty('sectionNumberLabel', 'integer'),
	ModelBaseClass.createModelProperty('content', 'string'),
	ModelBaseClass.createModelProperty('activeFlag', 'boolean'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
	ModelBaseClass.createModelProperty('saveNote', 'string'),
	ModelBaseClass.createModelProperty('previousSection', 'Section'),
	ModelBaseClass.createModelProperty('nextSection', 'Section'),
	ModelBaseClass.createModelProperty('chapter', 'Chapter'),
];

export default SectionBase;
