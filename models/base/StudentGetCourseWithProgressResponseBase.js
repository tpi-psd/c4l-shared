import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentGetCourseWithProgressResponse from "../StudentGetCourseWithProgressResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentGetCourseWithProgressResponseBase
 * @extends ModelBaseClass
 * @property {[Chapter]} chapters will return ordered array of chapters that are viewable by the student, and for each chapter, will include studentViewableSections
 * @property {CourseProgress} courseProgress
 * @property {[Assessment]} assessments Only completed exams and quizzes will be returned, including score information, but no structured data.
 */
class StudentGetCourseWithProgressResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentGetCourseWithProgressResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentGetCourseWithProgressResponse}
	 */
	static create(genericObject) {
		const newStudentGetCourseWithProgressResponse = new StudentGetCourseWithProgressResponse();
		newStudentGetCourseWithProgressResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentGetCourseWithProgressResponse;
	}

	/**
	 * Instantiates a new array of StudentGetCourseWithProgressResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentGetCourseWithProgressResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentGetCourseWithProgressResponseArray = [];
		genericArray.forEach(genericObject => {
			newStudentGetCourseWithProgressResponseArray.push(StudentGetCourseWithProgressResponse.create(genericObject));
		});
		return newStudentGetCourseWithProgressResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('chapters', '[Chapter]'),
	ModelBaseClass.createModelProperty('courseProgress', 'CourseProgress'),
	ModelBaseClass.createModelProperty('assessments', '[Assessment]'),
];

export default StudentGetCourseWithProgressResponseBase;
