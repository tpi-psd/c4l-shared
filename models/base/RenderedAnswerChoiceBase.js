import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import RenderedAnswerChoice from "../RenderedAnswerChoice";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class RenderedAnswerChoiceBase
 * @extends ModelBaseClass
 * @property {number} answerId (int64)
 * @property {string} choice e.g. A, B, C, D or E
 * @property {string} content
 */
class RenderedAnswerChoiceBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of RenderedAnswerChoice based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {RenderedAnswerChoice}
	 */
	static create(genericObject) {
		const newRenderedAnswerChoice = new RenderedAnswerChoice();
		newRenderedAnswerChoice.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newRenderedAnswerChoice;
	}

	/**
	 * Instantiates a new array of RenderedAnswerChoice based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[RenderedAnswerChoice]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newRenderedAnswerChoiceArray = [];
		genericArray.forEach(genericObject => {
			newRenderedAnswerChoiceArray.push(RenderedAnswerChoice.create(genericObject));
		});
		return newRenderedAnswerChoiceArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('answerId', 'integer'),
	ModelBaseClass.createModelProperty('choice', 'string'),
	ModelBaseClass.createModelProperty('content', 'string'),
];

export default RenderedAnswerChoiceBase;
