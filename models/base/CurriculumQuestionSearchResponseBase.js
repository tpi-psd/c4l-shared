import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import CurriculumQuestionSearchResponse from "../CurriculumQuestionSearchResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class CurriculumQuestionSearchResponseBase
 * @extends ModelBaseClass
 * @property {[Question]} questions
 * @property {number} totalItemCount (int64)
 */
class CurriculumQuestionSearchResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of CurriculumQuestionSearchResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {CurriculumQuestionSearchResponse}
	 */
	static create(genericObject) {
		const newCurriculumQuestionSearchResponse = new CurriculumQuestionSearchResponse();
		newCurriculumQuestionSearchResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newCurriculumQuestionSearchResponse;
	}

	/**
	 * Instantiates a new array of CurriculumQuestionSearchResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[CurriculumQuestionSearchResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newCurriculumQuestionSearchResponseArray = [];
		genericArray.forEach(genericObject => {
			newCurriculumQuestionSearchResponseArray.push(CurriculumQuestionSearchResponse.create(genericObject));
		});
		return newCurriculumQuestionSearchResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('questions', '[Question]'),
	ModelBaseClass.createModelProperty('totalItemCount', 'integer'),
];

export default CurriculumQuestionSearchResponseBase;
