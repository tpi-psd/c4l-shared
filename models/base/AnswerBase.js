import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Answer from "../Answer";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class AnswerBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {number} questionId (int64)
 * @property {string} content
 * @property {number} orderNumber (integer)
 * @property {number} responseCount (integer)
 */
class AnswerBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Answer based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Answer}
	 */
	static create(genericObject) {
		const newAnswer = new Answer();
		newAnswer.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newAnswer;
	}

	/**
	 * Instantiates a new array of Answer based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Answer]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newAnswerArray = [];
		genericArray.forEach(genericObject => {
			newAnswerArray.push(Answer.create(genericObject));
		});
		return newAnswerArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('questionId', 'integer'),
	ModelBaseClass.createModelProperty('content', 'string'),
	ModelBaseClass.createModelProperty('orderNumber', 'integer'),
	ModelBaseClass.createModelProperty('responseCount', 'integer'),
];

export default AnswerBase;
