import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import PaymentMethod from "../PaymentMethod";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class PaymentMethodBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {string} nameOnCard
 * @property {string} description
 * @property {Date} dateExpiration (date only)
 * @property {Date} dateCreated (date and time)
 */
class PaymentMethodBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of PaymentMethod based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {PaymentMethod}
	 */
	static create(genericObject) {
		const newPaymentMethod = new PaymentMethod();
		newPaymentMethod.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newPaymentMethod;
	}

	/**
	 * Instantiates a new array of PaymentMethod based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[PaymentMethod]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newPaymentMethodArray = [];
		genericArray.forEach(genericObject => {
			newPaymentMethodArray.push(PaymentMethod.create(genericObject));
		});
		return newPaymentMethodArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('nameOnCard', 'string'),
	ModelBaseClass.createModelProperty('description', 'string'),
	ModelBaseClass.createModelProperty('dateExpiration', 'datetime'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
];

export default PaymentMethodBase;
