import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import WrittenQuizAnswerSearchRequest from "../WrittenQuizAnswerSearchRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class WrittenQuizAnswerSearchRequestBase
 * @extends ModelBaseClass
 * @property {string} smartFilter can be student first name, student last name, or student's studentIdentifierLabel
 * @property {boolean} filterByGradedStatus set to true for only Graded ones.  set to false for only Ungraded ones.
 * @property {number} filterBySchoolId (int64)
 * @property {number} filterByChapterId (int64)
 * @property {ResultParameter} resultParameter [Student, StudentIdentifier, Email, School, Chapter, DateCompleted, DateGraded, Score, GradedFlag]
 */
class WrittenQuizAnswerSearchRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of WrittenQuizAnswerSearchRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {WrittenQuizAnswerSearchRequest}
	 */
	static create(genericObject) {
		const newWrittenQuizAnswerSearchRequest = new WrittenQuizAnswerSearchRequest();
		newWrittenQuizAnswerSearchRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newWrittenQuizAnswerSearchRequest;
	}

	/**
	 * Instantiates a new array of WrittenQuizAnswerSearchRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[WrittenQuizAnswerSearchRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newWrittenQuizAnswerSearchRequestArray = [];
		genericArray.forEach(genericObject => {
			newWrittenQuizAnswerSearchRequestArray.push(WrittenQuizAnswerSearchRequest.create(genericObject));
		});
		return newWrittenQuizAnswerSearchRequestArray;
	}
}

/**
 * @type {string} OrderByStudent
 */
WrittenQuizAnswerSearchRequestBase.OrderByStudent = 'student';

/**
 * @type {string} OrderByStudentIdentifier
 */
WrittenQuizAnswerSearchRequestBase.OrderByStudentIdentifier = 'studentidentifier';

/**
 * @type {string} OrderByEmail
 */
WrittenQuizAnswerSearchRequestBase.OrderByEmail = 'email';

/**
 * @type {string} OrderBySchool
 */
WrittenQuizAnswerSearchRequestBase.OrderBySchool = 'school';

/**
 * @type {string} OrderByChapter
 */
WrittenQuizAnswerSearchRequestBase.OrderByChapter = 'chapter';

/**
 * @type {string} OrderByDateCompleted
 */
WrittenQuizAnswerSearchRequestBase.OrderByDateCompleted = 'datecompleted';

/**
 * @type {string} OrderByDateGraded
 */
WrittenQuizAnswerSearchRequestBase.OrderByDateGraded = 'dategraded';

/**
 * @type {string} OrderByScore
 */
WrittenQuizAnswerSearchRequestBase.OrderByScore = 'score';

/**
 * @type {string} OrderByGradedFlag
 */
WrittenQuizAnswerSearchRequestBase.OrderByGradedFlag = 'gradedflag';

const _modelDefinition = [
	ModelBaseClass.createModelProperty('smartFilter', 'string'),
	ModelBaseClass.createModelProperty('filterByGradedStatus', 'boolean'),
	ModelBaseClass.createModelProperty('filterBySchoolId', 'integer'),
	ModelBaseClass.createModelProperty('filterByChapterId', 'integer'),
	ModelBaseClass.createModelProperty('resultParameter', 'ResultParameter'),
];

export default WrittenQuizAnswerSearchRequestBase;
