import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import CurriculumQuestionSearchRequest from "../CurriculumQuestionSearchRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class CurriculumQuestionSearchRequestBase
 * @extends ModelBaseClass
 * @property {number} chapterId (int64) this is required
 * @property {string} smartFilter can be text content in the question or the 2-letter identifier
 * @property {boolean} includeInactiveFlag
 * @property {number} filterBySectionId (int64)
 * @property {boolean} instantiateAnswersFlag instantiate answers for each question -- only use if needed, since this will require additional processing
 * @property {ResultParameter} resultParameter [QuestionIdentifier, Section, DateLastModified]
 */
class CurriculumQuestionSearchRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of CurriculumQuestionSearchRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {CurriculumQuestionSearchRequest}
	 */
	static create(genericObject) {
		const newCurriculumQuestionSearchRequest = new CurriculumQuestionSearchRequest();
		newCurriculumQuestionSearchRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newCurriculumQuestionSearchRequest;
	}

	/**
	 * Instantiates a new array of CurriculumQuestionSearchRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[CurriculumQuestionSearchRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newCurriculumQuestionSearchRequestArray = [];
		genericArray.forEach(genericObject => {
			newCurriculumQuestionSearchRequestArray.push(CurriculumQuestionSearchRequest.create(genericObject));
		});
		return newCurriculumQuestionSearchRequestArray;
	}
}

/**
 * @type {string} OrderByQuestionIdentifier
 */
CurriculumQuestionSearchRequestBase.OrderByQuestionIdentifier = 'questionidentifier';

/**
 * @type {string} OrderBySection
 */
CurriculumQuestionSearchRequestBase.OrderBySection = 'section';

/**
 * @type {string} OrderByDateLastModified
 */
CurriculumQuestionSearchRequestBase.OrderByDateLastModified = 'datelastmodified';

const _modelDefinition = [
	ModelBaseClass.createModelProperty('chapterId', 'integer'),
	ModelBaseClass.createModelProperty('smartFilter', 'string'),
	ModelBaseClass.createModelProperty('includeInactiveFlag', 'boolean'),
	ModelBaseClass.createModelProperty('filterBySectionId', 'integer'),
	ModelBaseClass.createModelProperty('instantiateAnswersFlag', 'boolean'),
	ModelBaseClass.createModelProperty('resultParameter', 'ResultParameter'),
];

export default CurriculumQuestionSearchRequestBase;
