import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import PersonAdminSearchRequest from "../PersonAdminSearchRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class PersonAdminSearchRequestBase
 * @extends ModelBaseClass
 * @property {string} smartFilter
 * @property {boolean} includeInactiveFlag
 * @property {['C4LManager'|'C4LStaff'|'InstitutionManager'|'InstitutionStaff']} filterByRoles
 * @property {number} filterBySchoolId (int64)
 * @property {number} filterByCampusId (int64)
 * @property {ResultParameter} resultParameter [Email, FirstName, LastName, Role, School, Campus, DateCreated]
 */
class PersonAdminSearchRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of PersonAdminSearchRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {PersonAdminSearchRequest}
	 */
	static create(genericObject) {
		const newPersonAdminSearchRequest = new PersonAdminSearchRequest();
		newPersonAdminSearchRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newPersonAdminSearchRequest;
	}

	/**
	 * Instantiates a new array of PersonAdminSearchRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[PersonAdminSearchRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newPersonAdminSearchRequestArray = [];
		genericArray.forEach(genericObject => {
			newPersonAdminSearchRequestArray.push(PersonAdminSearchRequest.create(genericObject));
		});
		return newPersonAdminSearchRequestArray;
	}
}

/**
 * @type {string} OrderByEmail
 */
PersonAdminSearchRequestBase.OrderByEmail = 'email';

/**
 * @type {string} OrderByFirstName
 */
PersonAdminSearchRequestBase.OrderByFirstName = 'firstname';

/**
 * @type {string} OrderByLastName
 */
PersonAdminSearchRequestBase.OrderByLastName = 'lastname';

/**
 * @type {string} OrderByRole
 */
PersonAdminSearchRequestBase.OrderByRole = 'role';

/**
 * @type {string} OrderBySchool
 */
PersonAdminSearchRequestBase.OrderBySchool = 'school';

/**
 * @type {string} OrderByCampus
 */
PersonAdminSearchRequestBase.OrderByCampus = 'campus';

/**
 * @type {string} OrderByDateCreated
 */
PersonAdminSearchRequestBase.OrderByDateCreated = 'datecreated';

const _modelDefinition = [
	ModelBaseClass.createModelProperty('smartFilter', 'string'),
	ModelBaseClass.createModelProperty('includeInactiveFlag', 'boolean'),
	ModelBaseClass.createModelProperty('filterByRoles', '[string]'),
	ModelBaseClass.createModelProperty('filterBySchoolId', 'integer'),
	ModelBaseClass.createModelProperty('filterByCampusId', 'integer'),
	ModelBaseClass.createModelProperty('resultParameter', 'ResultParameter'),
];

export default PersonAdminSearchRequestBase;
