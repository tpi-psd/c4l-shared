import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Question from "../Question";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class QuestionBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {number} chapterId (int64)
 * @property {number} sectionId (int64)
 * @property {string} content in markdown format
 * @property {string} identifierLabel e.g. AA, AB, AC, etc.
 * @property {Answer} correctAnswer
 * @property {[Answer]} incorrectAnswers
 * @property {Date} dateLastModified (date and time)
 * @property {boolean} activeFlag
 * @property {Date} dateCreated (date and time)
 */
class QuestionBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Question based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Question}
	 */
	static create(genericObject) {
		const newQuestion = new Question();
		newQuestion.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newQuestion;
	}

	/**
	 * Instantiates a new array of Question based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Question]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newQuestionArray = [];
		genericArray.forEach(genericObject => {
			newQuestionArray.push(Question.create(genericObject));
		});
		return newQuestionArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('chapterId', 'integer'),
	ModelBaseClass.createModelProperty('sectionId', 'integer'),
	ModelBaseClass.createModelProperty('content', 'string'),
	ModelBaseClass.createModelProperty('identifierLabel', 'string'),
	ModelBaseClass.createModelProperty('correctAnswer', 'Answer'),
	ModelBaseClass.createModelProperty('incorrectAnswers', '[Answer]'),
	ModelBaseClass.createModelProperty('dateLastModified', 'datetime'),
	ModelBaseClass.createModelProperty('activeFlag', 'boolean'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
];

export default QuestionBase;
