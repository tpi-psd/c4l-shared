import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentSearchResponse from "../StudentSearchResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentSearchResponseBase
 * @extends ModelBaseClass
 * @property {[Person]} persons
 * @property {number} totalItemCount (int64)
 */
class StudentSearchResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentSearchResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentSearchResponse}
	 */
	static create(genericObject) {
		const newStudentSearchResponse = new StudentSearchResponse();
		newStudentSearchResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentSearchResponse;
	}

	/**
	 * Instantiates a new array of StudentSearchResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentSearchResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentSearchResponseArray = [];
		genericArray.forEach(genericObject => {
			newStudentSearchResponseArray.push(StudentSearchResponse.create(genericObject));
		});
		return newStudentSearchResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('persons', '[Person]'),
	ModelBaseClass.createModelProperty('totalItemCount', 'integer'),
];

export default StudentSearchResponseBase;
