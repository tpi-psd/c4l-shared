import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import TranscriptRequestOrderRequest from "../TranscriptRequestOrderRequest";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class TranscriptRequestOrderRequestBase
 * @extends ModelBaseClass
 * @property {TranscriptRequest} transcriptRequest
 * @property {boolean} usePaymentOnFileFlag is only available for self-service students
 * @property {PaymentRequest} paymentRequest must be provided if usePaymentOnFileFlag is false or this is not a self-service student
 */
class TranscriptRequestOrderRequestBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of TranscriptRequestOrderRequest based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {TranscriptRequestOrderRequest}
	 */
	static create(genericObject) {
		const newTranscriptRequestOrderRequest = new TranscriptRequestOrderRequest();
		newTranscriptRequestOrderRequest.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newTranscriptRequestOrderRequest;
	}

	/**
	 * Instantiates a new array of TranscriptRequestOrderRequest based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[TranscriptRequestOrderRequest]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newTranscriptRequestOrderRequestArray = [];
		genericArray.forEach(genericObject => {
			newTranscriptRequestOrderRequestArray.push(TranscriptRequestOrderRequest.create(genericObject));
		});
		return newTranscriptRequestOrderRequestArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('transcriptRequest', 'TranscriptRequest'),
	ModelBaseClass.createModelProperty('usePaymentOnFileFlag', 'boolean'),
	ModelBaseClass.createModelProperty('paymentRequest', 'PaymentRequest'),
];

export default TranscriptRequestOrderRequestBase;
