import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import CourseProgress from "../CourseProgress";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class CourseProgressBase
 * @extends ModelBaseClass
 * @property {number} subjectId (int64)
 * @property {number} courseId (int64)
 * @property {number} requirementId (int64)
 * @property {'NotYetStarted'|'InProgress'|'PendingUnlockedExam'|'GradePending'|'PassedViaTransfer'|'PassedViaChallengeExam'|'Passed'|'Failed'} status
 * @property {'Available'|'Unavailable'|'Passed'|'Failed'} challengeExamStatus
 * @property {Date} dateCreated (date and time)
 * @property {string} finalGrade
 * @property {number} finalScore
 * @property {Date} dateCompleted (date and time)
 * @property {Date} dateAttemptStarted (date and time)
 * @property {number} attemptNumber (integer)
 * @property {number} completionPercentage
 * @property {boolean} finalExamAvailable
 * @property {number} elapsedTime (integer) total time, in seconds
 * @property {CourseProgressDetail} courseProgressDetail
 */
class CourseProgressBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of CourseProgress based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {CourseProgress}
	 */
	static create(genericObject) {
		const newCourseProgress = new CourseProgress();
		newCourseProgress.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newCourseProgress;
	}

	/**
	 * Instantiates a new array of CourseProgress based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[CourseProgress]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newCourseProgressArray = [];
		genericArray.forEach(genericObject => {
			newCourseProgressArray.push(CourseProgress.create(genericObject));
		});
		return newCourseProgressArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('subjectId', 'integer'),
	ModelBaseClass.createModelProperty('courseId', 'integer'),
	ModelBaseClass.createModelProperty('requirementId', 'integer'),
	ModelBaseClass.createModelProperty('status', 'string'),
	ModelBaseClass.createModelProperty('challengeExamStatus', 'string'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
	ModelBaseClass.createModelProperty('finalGrade', 'string'),
	ModelBaseClass.createModelProperty('finalScore', 'float'),
	ModelBaseClass.createModelProperty('dateCompleted', 'datetime'),
	ModelBaseClass.createModelProperty('dateAttemptStarted', 'datetime'),
	ModelBaseClass.createModelProperty('attemptNumber', 'integer'),
	ModelBaseClass.createModelProperty('completionPercentage', 'float'),
	ModelBaseClass.createModelProperty('finalExamAvailable', 'boolean'),
	ModelBaseClass.createModelProperty('elapsedTime', 'integer'),
	ModelBaseClass.createModelProperty('courseProgressDetail', 'CourseProgressDetail'),
];

export default CourseProgressBase;
