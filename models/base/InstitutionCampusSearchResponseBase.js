import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import InstitutionCampusSearchResponse from "../InstitutionCampusSearchResponse";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class InstitutionCampusSearchResponseBase
 * @extends ModelBaseClass
 * @property {[Campus]} campuses
 * @property {number} totalItemCount (int64)
 */
class InstitutionCampusSearchResponseBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of InstitutionCampusSearchResponse based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {InstitutionCampusSearchResponse}
	 */
	static create(genericObject) {
		const newInstitutionCampusSearchResponse = new InstitutionCampusSearchResponse();
		newInstitutionCampusSearchResponse.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newInstitutionCampusSearchResponse;
	}

	/**
	 * Instantiates a new array of InstitutionCampusSearchResponse based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[InstitutionCampusSearchResponse]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newInstitutionCampusSearchResponseArray = [];
		genericArray.forEach(genericObject => {
			newInstitutionCampusSearchResponseArray.push(InstitutionCampusSearchResponse.create(genericObject));
		});
		return newInstitutionCampusSearchResponseArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('campuses', '[Campus]'),
	ModelBaseClass.createModelProperty('totalItemCount', 'integer'),
];

export default InstitutionCampusSearchResponseBase;
