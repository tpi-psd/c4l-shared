import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import StudentPreferences from "../StudentPreferences";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class StudentPreferencesBase
 * @extends ModelBaseClass
 * @property {string} maritalStatus
 * @property {string} homeLanguage
 * @property {string} ethnicity
 * @property {string} numberOfDependents
 * @property {string} employmentStatus
 * @property {string} extraHelpReading
 * @property {string} extraHelpMath
 * @property {string} awareAccessibe
 * @property {string} note
 * @property {boolean} finalTranscriptUploadedFlag
 * @property {Date} dateSurveyOnFirstClicked (date and time)
 * @property {Date} dateSurveyOnHalfClicked (date and time)
 * @property {Date} dateSurveyOnLastClicked (date and time)
 * @property {StudentInvoiceStats} invoiceStatsFirstHalf
 * @property {StudentInvoiceStats} invoiceStatsSecondHalf
 * @property {StudentInvoiceStats} invoiceStatsFull
 */
class StudentPreferencesBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of StudentPreferences based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {StudentPreferences}
	 */
	static create(genericObject) {
		const newStudentPreferences = new StudentPreferences();
		newStudentPreferences.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newStudentPreferences;
	}

	/**
	 * Instantiates a new array of StudentPreferences based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[StudentPreferences]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newStudentPreferencesArray = [];
		genericArray.forEach(genericObject => {
			newStudentPreferencesArray.push(StudentPreferences.create(genericObject));
		});
		return newStudentPreferencesArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('maritalStatus', 'string'),
	ModelBaseClass.createModelProperty('homeLanguage', 'string'),
	ModelBaseClass.createModelProperty('ethnicity', 'string'),
	ModelBaseClass.createModelProperty('numberOfDependents', 'string'),
	ModelBaseClass.createModelProperty('employmentStatus', 'string'),
	ModelBaseClass.createModelProperty('extraHelpReading', 'string'),
	ModelBaseClass.createModelProperty('extraHelpMath', 'string'),
	ModelBaseClass.createModelProperty('awareAccessibe', 'string'),
	ModelBaseClass.createModelProperty('note', 'string'),
	ModelBaseClass.createModelProperty('finalTranscriptUploadedFlag', 'boolean'),
	ModelBaseClass.createModelProperty('dateSurveyOnFirstClicked', 'datetime'),
	ModelBaseClass.createModelProperty('dateSurveyOnHalfClicked', 'datetime'),
	ModelBaseClass.createModelProperty('dateSurveyOnLastClicked', 'datetime'),
	ModelBaseClass.createModelProperty('invoiceStatsFirstHalf', 'StudentInvoiceStats'),
	ModelBaseClass.createModelProperty('invoiceStatsSecondHalf', 'StudentInvoiceStats'),
	ModelBaseClass.createModelProperty('invoiceStatsFull', 'StudentInvoiceStats'),
];

export default StudentPreferencesBase;
