import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import SectionName from "../SectionName";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class SectionNameBase
 * @extends ModelBaseClass
 * @property {number} id (int64)
 * @property {string} name
 * @property {number} sectionNumberLabel (integer)
 */
class SectionNameBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of SectionName based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {SectionName}
	 */
	static create(genericObject) {
		const newSectionName = new SectionName();
		newSectionName.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newSectionName;
	}

	/**
	 * Instantiates a new array of SectionName based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[SectionName]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newSectionNameArray = [];
		genericArray.forEach(genericObject => {
			newSectionNameArray.push(SectionName.create(genericObject));
		});
		return newSectionNameArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('id', 'integer'),
	ModelBaseClass.createModelProperty('name', 'string'),
	ModelBaseClass.createModelProperty('sectionNumberLabel', 'integer'),
];

export default SectionNameBase;
