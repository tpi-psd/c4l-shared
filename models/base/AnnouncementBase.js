import ModelBaseClass from "@quasidea/oas-client-react/lib/ModelBaseClass";
import Announcement from "../Announcement";
import ModelProxyClass from "./ModelProxyClass";

/**
 * @class AnnouncementBase
 * @extends ModelBaseClass
 * @property {string} identifier
 * @property {string} title
 * @property {string} linkUrl
 * @property {string} body
 * @property {Date} dateStart (date only)
 * @property {Date} dateEnd (date only)
 * @property {boolean} activeFlag set to null if dateStart and dateEnd are filled out.  Otherwise, set to true or false.
 * @property {Date} dateLastModified (date and time)
 * @property {Date} dateCreated (date and time)
 */
class AnnouncementBase extends ModelBaseClass {

	/**
	 * Instantiates a new instance of Announcement based on the generic object being passed in (typically from a JSON object)
	 * @param {object} genericObject
	 * @return {Announcement}
	 */
	static create(genericObject) {
		const newAnnouncement = new Announcement();
		newAnnouncement.instantiate(_modelDefinition, genericObject, ModelProxyClass.createByClassName);
		return newAnnouncement;
	}

	/**
	 * Instantiates a new array of Announcement based on the generic array being passed in (typically from a JSON array)
	 * @param {[object]} genericArray
	 * @return {[Announcement]}
	 */
	static createArray(genericArray) {
		if (genericArray === null) {
			return null;
		}

		const newAnnouncementArray = [];
		genericArray.forEach(genericObject => {
			newAnnouncementArray.push(Announcement.create(genericObject));
		});
		return newAnnouncementArray;
	}
}

const _modelDefinition = [
	ModelBaseClass.createModelProperty('identifier', 'string'),
	ModelBaseClass.createModelProperty('title', 'string'),
	ModelBaseClass.createModelProperty('linkUrl', 'string'),
	ModelBaseClass.createModelProperty('body', 'string'),
	ModelBaseClass.createModelProperty('dateStart', 'datetime'),
	ModelBaseClass.createModelProperty('dateEnd', 'datetime'),
	ModelBaseClass.createModelProperty('activeFlag', 'boolean'),
	ModelBaseClass.createModelProperty('dateLastModified', 'datetime'),
	ModelBaseClass.createModelProperty('dateCreated', 'datetime'),
];

export default AnnouncementBase;
