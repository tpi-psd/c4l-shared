import TranscriptRequestSearchResponseBase from './base/TranscriptRequestSearchResponseBase';

/**
 * @class TranscriptRequestSearchResponse
 * @extends TranscriptRequestSearchResponseBase
 */
class TranscriptRequestSearchResponse extends TranscriptRequestSearchResponseBase {

}

export default TranscriptRequestSearchResponse;
