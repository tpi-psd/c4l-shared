import NoteBase from './base/NoteBase';

/**
 * @class Note
 * @extends NoteBase
 */
class Note extends NoteBase {
	displayResponse() {
		return '[' + this.content + '] - [' + this.actionToken + ']';
	}
}

export default Note;
