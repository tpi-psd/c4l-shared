import SectionBase from './base/SectionBase';

/**
 * @class Section
 * @extends SectionBase
 */
class Section extends SectionBase {

}

export default Section;
