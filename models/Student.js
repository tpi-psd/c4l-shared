import StudentBase from './base/StudentBase';

/**
 * @class Student
 * @extends StudentBase
 */
class Student extends StudentBase {
	isSelfServiceStudent() {
		return !!this.currentSubscriptionId;
	}
	isSchoolStudent() {
		return !!this.schoolId;
	}
	get creditCardInfo() {
		return this.currentSubscription
			? {
				nameOnCard: this.currentSubscription.paymentMethod.nameOnCard,
				description: this.currentSubscription.paymentMethod.description,
				dateExpiration: this.currentSubscription.paymentMethod.dateExpiration
			  }
			: null;
	}
	get schoolName() {
		return this.school?.name || '';
	}
	get campusName() {
		return this.campus?.name || '';
	}
}

export default Student;
