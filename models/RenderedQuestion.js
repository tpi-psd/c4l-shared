import RenderedQuestionBase from './base/RenderedQuestionBase';

/**
 * @class RenderedQuestion
 * @extends RenderedQuestionBase
 */
class RenderedQuestion extends RenderedQuestionBase {

}

export default RenderedQuestion;
