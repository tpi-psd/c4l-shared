import StudentInvoiceStatsBase from './base/StudentInvoiceStatsBase';

/**
 * @class StudentInvoiceStats
 * @extends StudentInvoiceStatsBase
 */
class StudentInvoiceStats extends StudentInvoiceStatsBase {

}

export default StudentInvoiceStats;
