import CurriculumQuestionSearchResponseBase from './base/CurriculumQuestionSearchResponseBase';

/**
 * @class CurriculumQuestionSearchResponse
 * @extends CurriculumQuestionSearchResponseBase
 */
class CurriculumQuestionSearchResponse extends CurriculumQuestionSearchResponseBase {

}

export default CurriculumQuestionSearchResponse;
