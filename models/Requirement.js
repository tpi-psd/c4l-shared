import RequirementBase from './base/RequirementBase';

/**
 * @class Requirement
 * @extends RequirementBase
 */
class Requirement extends RequirementBase {

}

export default Requirement;
