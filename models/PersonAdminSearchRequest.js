import PersonAdminSearchRequestBase from './base/PersonAdminSearchRequestBase';

/**
 * @class PersonAdminSearchRequest
 * @extends PersonAdminSearchRequestBase
 */
class PersonAdminSearchRequest extends PersonAdminSearchRequestBase {

}

export default PersonAdminSearchRequest;
