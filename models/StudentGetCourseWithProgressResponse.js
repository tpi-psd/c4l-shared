import StudentGetCourseWithProgressResponseBase from './base/StudentGetCourseWithProgressResponseBase';

/**
 * @class StudentGetCourseWithProgressResponse
 * @extends StudentGetCourseWithProgressResponseBase
 */
class StudentGetCourseWithProgressResponse extends StudentGetCourseWithProgressResponseBase {

}

export default StudentGetCourseWithProgressResponse;
