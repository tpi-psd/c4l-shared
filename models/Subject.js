import SubjectBase from './base/SubjectBase';

/**
 * @class Subject
 * @extends SubjectBase
 */
class Subject extends SubjectBase {

}

export default Subject;
