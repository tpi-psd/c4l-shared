import StudentRegisterCampusAffiliatedCompletionRequestBase from './base/StudentRegisterCampusAffiliatedCompletionRequestBase';

/**
 * @class StudentRegisterCampusAffiliatedCompletionRequest
 * @extends StudentRegisterCampusAffiliatedCompletionRequestBase
 */
class StudentRegisterCampusAffiliatedCompletionRequest extends StudentRegisterCampusAffiliatedCompletionRequestBase {

}

export default StudentRegisterCampusAffiliatedCompletionRequest;
