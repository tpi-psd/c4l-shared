import PaymentMethodBase from './base/PaymentMethodBase';

/**
 * @class PaymentMethod
 * @extends PaymentMethodBase
 */
class PaymentMethod extends PaymentMethodBase {

}

export default PaymentMethod;
