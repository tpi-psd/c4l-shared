import ChapterBase from './base/ChapterBase';
import ChapterQuizTypeEnum from './enum/ChapterQuizTypeEnum';

/**
 * @class Chapter
 * @extends ChapterBase
 */
class Chapter extends ChapterBase {
	isSpecialAssessment() {
		return ![
			ChapterQuizTypeEnum.NONE,
			ChapterQuizTypeEnum.STANDARD,
			ChapterQuizTypeEnum.VIDEO,
			ChapterQuizTypeEnum.WRITTEN,
		].includes(this.quizType);
	}
}

export default Chapter;
