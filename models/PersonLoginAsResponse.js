import PersonLoginAsResponseBase from './base/PersonLoginAsResponseBase';

/**
 * @class PersonLoginAsResponse
 * @extends PersonLoginAsResponseBase
 */
class PersonLoginAsResponse extends PersonLoginAsResponseBase {

}

export default PersonLoginAsResponse;
