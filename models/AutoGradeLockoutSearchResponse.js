import AutoGradeLockoutSearchResponseBase from './base/AutoGradeLockoutSearchResponseBase';

/**
 * @class AutoGradeLockoutSearchResponse
 * @extends AutoGradeLockoutSearchResponseBase
 */
class AutoGradeLockoutSearchResponse extends AutoGradeLockoutSearchResponseBase {

}

export default AutoGradeLockoutSearchResponse;
