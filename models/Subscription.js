import SubscriptionBase from './base/SubscriptionBase';

/**
 * @class Subscription
 * @extends SubscriptionBase
 */
class Subscription extends SubscriptionBase {

}

export default Subscription;
