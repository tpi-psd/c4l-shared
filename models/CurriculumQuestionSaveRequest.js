import CurriculumQuestionSaveRequestBase from './base/CurriculumQuestionSaveRequestBase';

/**
 * @class CurriculumQuestionSaveRequest
 * @extends CurriculumQuestionSaveRequestBase
 */
class CurriculumQuestionSaveRequest extends CurriculumQuestionSaveRequestBase {

}

export default CurriculumQuestionSaveRequest;
