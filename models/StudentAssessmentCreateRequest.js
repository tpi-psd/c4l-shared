import StudentAssessmentCreateRequestBase from './base/StudentAssessmentCreateRequestBase';

/**
 * @class StudentAssessmentCreateRequest
 * @extends StudentAssessmentCreateRequestBase
 */
class StudentAssessmentCreateRequest extends StudentAssessmentCreateRequestBase {

}

export default StudentAssessmentCreateRequest;
