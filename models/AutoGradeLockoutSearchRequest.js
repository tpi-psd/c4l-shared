import AutoGradeLockoutSearchRequestBase from './base/AutoGradeLockoutSearchRequestBase';

/**
 * @class AutoGradeLockoutSearchRequest
 * @extends AutoGradeLockoutSearchRequestBase
 */
class AutoGradeLockoutSearchRequest extends AutoGradeLockoutSearchRequestBase {

}

export default AutoGradeLockoutSearchRequest;
