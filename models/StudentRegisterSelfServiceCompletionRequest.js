import StudentRegisterSelfServiceCompletionRequestBase from './base/StudentRegisterSelfServiceCompletionRequestBase';

/**
 * @class StudentRegisterSelfServiceCompletionRequest
 * @extends StudentRegisterSelfServiceCompletionRequestBase
 */
class StudentRegisterSelfServiceCompletionRequest extends StudentRegisterSelfServiceCompletionRequestBase {

}

export default StudentRegisterSelfServiceCompletionRequest;
