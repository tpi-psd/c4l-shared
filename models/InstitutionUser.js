import InstitutionUserBase from './base/InstitutionUserBase';

/**
 * @class InstitutionUser
 * @extends InstitutionUserBase
 */
class InstitutionUser extends InstitutionUserBase {

}

export default InstitutionUser;
