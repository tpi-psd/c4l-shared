import WrittenQuizAnswerSearchRequestBase from './base/WrittenQuizAnswerSearchRequestBase';

/**
 * @class WrittenQuizAnswerSearchRequest
 * @extends WrittenQuizAnswerSearchRequestBase
 */
class WrittenQuizAnswerSearchRequest extends WrittenQuizAnswerSearchRequestBase {

}

export default WrittenQuizAnswerSearchRequest;
