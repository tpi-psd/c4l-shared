import TranscriptRequestSearchRequestBase from './base/TranscriptRequestSearchRequestBase';

/**
 * @class TranscriptRequestSearchRequest
 * @extends TranscriptRequestSearchRequestBase
 */
class TranscriptRequestSearchRequest extends TranscriptRequestSearchRequestBase {

}

export default TranscriptRequestSearchRequest;
