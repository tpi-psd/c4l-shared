import TranscriptRequestOrderRequestBase from './base/TranscriptRequestOrderRequestBase';

/**
 * @class TranscriptRequestOrderRequest
 * @extends TranscriptRequestOrderRequestBase
 */
class TranscriptRequestOrderRequest extends TranscriptRequestOrderRequestBase {

}

export default TranscriptRequestOrderRequest;
