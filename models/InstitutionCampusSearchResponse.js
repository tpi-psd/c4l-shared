import InstitutionCampusSearchResponseBase from './base/InstitutionCampusSearchResponseBase';

/**
 * @class InstitutionCampusSearchResponse
 * @extends InstitutionCampusSearchResponseBase
 */
class InstitutionCampusSearchResponse extends InstitutionCampusSearchResponseBase {

}

export default InstitutionCampusSearchResponse;
