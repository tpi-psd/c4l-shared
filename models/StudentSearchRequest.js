import StudentSearchRequestBase from './base/StudentSearchRequestBase';

/**
 * @class StudentSearchRequest
 * @extends StudentSearchRequestBase
 */
class StudentSearchRequest extends StudentSearchRequestBase {

}

export default StudentSearchRequest;
