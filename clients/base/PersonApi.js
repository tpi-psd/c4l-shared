import ClientBaseClass from "./ClientBaseClass";
import Session from "../../models/Session";
import PersonDashboardResponse from "../../models/PersonDashboardResponse";
import PersonLoginAsResponse from "../../models/PersonLoginAsResponse";
import LoginLog from "../../models/LoginLog";
import PersonAdminSearchResponse from "../../models/PersonAdminSearchResponse";
import Person from "../../models/Person";

export default class PersonApi extends ClientBaseClass {
	/**
	 * [C4L/Institution] Logs a C4L or Institution person into the Admin Portal
	 * @param {PersonLoginRequest} request
	 * @param {{status200: function(Session), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	loginAdmin(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/admin/login';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Session.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] Updates the person's profile -- requires id to be set to the currently logged in user's id
	 * @param {Person} request
	 * @param {{status200: function(string), status403: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	updateAdminProfile(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/admin/profile';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student] Logs a Student person into the Student Portal
	 * @param {PersonLoginRequest} request
	 * @param {{status200: function(Session), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	loginStudent(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/student/login';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Session.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Updates the user's password -- when calling within a session that was initiated by Registration or Forgot Password, currentPassword is ignored.  Otherwise, currentPassword is required.
	 * @param {UpdatePasswordRequest} request
	 * @param {{status200: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	updatePassword(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/update_password';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Does a Person account exist with the specified email address
	 * @param {string} emailAddress
	 * @param {{status200: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	exists(emailAddress, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/exists/' +
			(emailAddress ? encodeURI(emailAddress) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Attempts to trigger a Forgot / Reset Password workflow.  IF the Email exists, this will send out an email with instructions on how to reset their password.  Otherwise, this is a no-op.  Either way, this respond 200, so that this cannot be used to try and reverse-engineer for email address registrations.
	 * @param {ForgotPasswordRequest} request
	 * @param {{status200: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	forgotPassword(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/forgot_password';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Logs the person out and cleans up / clears the associated session object
	 * @param {{status200: function(string), status401: function(string), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	logout(responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/logout';

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 401:
					if (responseHandler.status401) {
						response.text()
							.then(responseText => {
								responseHandler.status401(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Retrieves the session object associated with the specified sessionToken
	 * @param {string} sessionToken
	 * @param {{status200: function(Session), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getSession(sessionToken, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/session/' +
			(sessionToken ? encodeURI(sessionToken) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Session.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Creates a NEW session object for the alternative login workflow (e.g. Forgot Password or Welcome).  This ID, HASH and CHECKSUM are generally created by the server, and sent via email to the user in a specially formulated link.
	 * @param {string} id
	 * @param {string} hash
	 * @param {string} checksum
	 * @param {{status200: function(Session), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	createSessionForAlternativeLoginWorkflow(id, hash, checksum, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/create_session/' +
			(id ? encodeURI(id) : '') + '/' +
			(hash ? encodeURI(hash) : '') + '/' +
			(checksum ? encodeURI(checksum) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Session.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] Gets the components for the Main Admin Dashboard for the currently logged in user.
	 * @param {{status200: function(PersonDashboardResponse), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getAdminDashboard(responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/admin/dashboard';

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(PersonDashboardResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] Allows a C4L user to login-as a student or institution user
	 * @param {string} personId
	 * @param {{status200: function(PersonLoginAsResponse), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	loginAs(personId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/login_as/' +
			(personId ? encodeURI(personId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(PersonLoginAsResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] Gets all login log items of a student user in reverse cron order
	 * @param {string} personId
	 * @param {{status200: function(LoginLog[]), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getLoginLog(personId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/login_log/' +
			(personId ? encodeURI(personId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(LoginLog.createArray(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] to search for list of Admin Site users
	 * @param {PersonAdminSearchRequest} request
	 * @param {{status200: function(PersonAdminSearchResponse), error: function(error), else: function(integer, string)}} responseHandler
	 */
	searchAdmin(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/admin/search';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(PersonAdminSearchResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager / Institution Manager] creates a new Admin Site user.  Fields that are applicable: firstName, lastName, email, phone, role.  For Institution Managers specifically -- institutionUser.schoolId is required, and either institutionUser.allCampusFlag is set OR institutionUser.campusIdArray is set.
	 * @param {Person} request
	 * @param {{status200: function(string), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	createAdmin(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/admin/create';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager / Institution Manager] Edits an existing Admin User.  id MUST be set.  Only set fields that are to be updated.  Fields that are updatable by anyone: firstName, lastName, email, phone, role.  When editing institution users specifically -- if person doing the action is C4L Manager, then institutionUser.schoolId is updatable.  For both C4L Manager and Institution Managers, institutionUser.allCampusFlag and institutionUser.campusIdArray are updatable.
	 * @param {Person} request
	 * @param {{status200: function(string), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	editAdmin(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/admin/edit';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager / Institution Manager] Activates/Deactivates an Admin User
	 * @param {string} personId
	 * @param {string} flag
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	setActivationAdmin(personId, flag, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/admin/activation/' +
			(personId ? encodeURI(personId) : '') + '/' +
			(flag ? encodeURI(flag) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Gets a Person record
	 * @param {string} personId
	 * @param {{status200: function(Person), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	get(personId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/person/get/' +
			(personId ? encodeURI(personId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Person.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

}
