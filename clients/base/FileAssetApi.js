import ClientBaseClass from "./ClientBaseClass";
import FileAsset from "../../models/FileAsset";

export default class FileAssetApi extends ClientBaseClass {
	/**
	 * This should never be called directly -- this is merely a placeholder so that the viewUrl in FileAsset will work correctly.
	 * @param {string} type
	 * @param {string} fileAssetIdentifier
	 * @param {string} sessionToken
	 * @param {string} filename
	 * @param {{status200: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	get(type, fileAssetIdentifier, sessionToken, filename, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/file_asset/' +
			(type ? encodeURI(type) : '') + '/' +
			(fileAssetIdentifier ? encodeURI(fileAssetIdentifier) : '') + '/' +
			(sessionToken ? encodeURI(sessionToken) : '') + '/' +
			(filename ? encodeURI(filename) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Creates a new File Asset for when a uploading an image using the Section or Question WYSIWYG editor.  Type should be SectionImage or QuestionImage.  uploadBase64Data, filename and mimeType must be set.  This will return a fileAsset, where you can save the .id field in the section/question content WYSIWYG editor.
	 * @param {FileAsset} request
	 * @param {{status200: function(FileAsset), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	createForContentImage(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/file_asset/create_content_image';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(FileAsset.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Student] Downloads / Views the PDF of the billing statement for the given year
	 * @param {string} personId
	 * @param {string} sessionToken
	 * @param {string} year
	 * @param {string} filename
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getBillingStatementForYear(personId, sessionToken, year, filename, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/pdf/billing_statement/' +
			(personId ? encodeURI(personId) : '') + '/' +
			(sessionToken ? encodeURI(sessionToken) : '') + '/' +
			(year ? encodeURI(year) : '') + '/' +
			(filename ? encodeURI(filename) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Downloads / Views the PDF of the student's unofficial transcript
	 * @param {string} personId
	 * @param {string} sessionToken
	 * @param {string} filename
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getUnofficialTranscript(personId, sessionToken, filename, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/pdf/transcript/' +
			(personId ? encodeURI(personId) : '') + '/' +
			(sessionToken ? encodeURI(sessionToken) : '') + '/' +
			(filename ? encodeURI(filename) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] Downloads / Views the PDF of the student's OFFICIAL transcript
	 * @param {string} personId
	 * @param {string} sessionToken
	 * @param {string} filename
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getOfficialTranscript(personId, sessionToken, filename, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/pdf/transcript_official/' +
			(personId ? encodeURI(personId) : '') + '/' +
			(sessionToken ? encodeURI(sessionToken) : '') + '/' +
			(filename ? encodeURI(filename) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] Downloads / Views the PDF of the course's curriculum report
	 * @param {string} courseId
	 * @param {string} sessionToken
	 * @param {string} filename
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getCourseReport(courseId, sessionToken, filename, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/pdf/course_report/' +
			(courseId ? encodeURI(courseId) : '') + '/' +
			(sessionToken ? encodeURI(sessionToken) : '') + '/' +
			(filename ? encodeURI(filename) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] Downloads / Views the CSV of the requested report
	 * @param {'school_invoice'|'report_b'|'report_c'} type
	 * @param {string} sessionToken
	 * @param {string} filename
	 * @param {string} datefrom
	 * @param {string} dateto
	 * @param {string} schoolid
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getCsvReport(type, sessionToken, filename, datefrom, dateto, schoolid, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const queryArray = [];
		if ((datefrom !== undefined) && (datefrom.length)) queryArray.push('dateFrom=' + encodeURI(datefrom));
		if ((dateto !== undefined) && (dateto.length)) queryArray.push('dateTo=' + encodeURI(dateto));
		if ((schoolid !== undefined) && (schoolid.length)) queryArray.push('schoolId=' + encodeURI(schoolid));

		const url = '/csv/report/' +
			(type ? encodeURI(type) : '') + '/' +
			(sessionToken ? encodeURI(sessionToken) : '') + '/' +
			(filename ? encodeURI(filename) : '') +
			(queryArray.length ? '?' + queryArray.join('&') : null);

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

}
