import ClientBaseClass from "./ClientBaseClass";
import TranscriptRequest from "../../models/TranscriptRequest";
import TranscriptRequestSearchResponse from "../../models/TranscriptRequestSearchResponse";

export default class TranscriptRequestApi extends ClientBaseClass {
	/**
	 * [C4L] updates an existing transcript request.  id is required, and the only field that can be set is status.
	 * @param {TranscriptRequest} request
	 * @param {{status200: function(TranscriptRequest), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	edit(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/transcript_request/edit';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(TranscriptRequest.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] to search for list of Transcript Requests
	 * @param {TranscriptRequestSearchRequest} request
	 * @param {{status200: function(TranscriptRequestSearchResponse), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	search(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/transcript_request/search';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(TranscriptRequestSearchResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] Gets an Transcript Request record
	 * @param {string} transcriptRequestId
	 * @param {{status200: function(TranscriptRequest), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	get(transcriptRequestId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/transcript_request/get/' +
			(transcriptRequestId ? encodeURI(transcriptRequestId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(TranscriptRequest.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student] order an official transcript.  transcriptRequest.shipToStudentFlag must be set, and if false, address, city, state and zip must be specified as well.  usePaymentOnFileFlag is only settable for self-service students.  If false, or if not a self-service student, paymentRequest must be provided.
	 * @param {TranscriptRequestOrderRequest} request
	 * @param {{status200: function(string), status402: function(string), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	order(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/transcript_request/order';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 402:
					if (responseHandler.status402) {
						response.text()
							.then(responseText => {
								responseHandler.status402(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

}
