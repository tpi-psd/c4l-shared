import ClientBaseClass from "./ClientBaseClass";
import WrittenQuizAnswerSearchResponse from "../../models/WrittenQuizAnswerSearchResponse";

export default class WrittenQuizAnswerApi extends ClientBaseClass {
	/**
	 * [C4L] grades an writtenQuizAnswer.  id is required, and the only fields that can be set is awardedPoints and possiblePoints.
	 * @param {Assessment} request
	 * @param {{status200: function(string), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	edit(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/written_quiz_answer/edit';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] to search for Written Quiz Answers
	 * @param {WrittenQuizAnswerSearchRequest} request
	 * @param {{status200: function(WrittenQuizAnswerSearchResponse), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	search(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/written_quiz_answer/search';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(WrittenQuizAnswerSearchResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

}
