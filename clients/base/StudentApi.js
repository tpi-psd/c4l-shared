import ClientBaseClass from "./ClientBaseClass";
import Person from "../../models/Person";
import School from "../../models/School";
import Session from "../../models/Session";
import StudentSearchResponse from "../../models/StudentSearchResponse";
import StudentCourseProgressResponse from "../../models/StudentCourseProgressResponse";
import StudentGetCourseWithProgressResponse from "../../models/StudentGetCourseWithProgressResponse";
import Section from "../../models/Section";
import StudentSearchAssessmentResponse from "../../models/StudentSearchAssessmentResponse";
import Assessment from "../../models/Assessment";
import StudentAnswer from "../../models/StudentAnswer";
import TransferTranscript from "../../models/TransferTranscript";

export default class StudentApi extends ClientBaseClass {
	/**
	 * [Student, Self Service] Initiates a self-service student registration process (e.g. Step 1)
	 * @param {StudentRegisterSelfServiceInitiationRequest} request
	 * @param {{status200: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	registerSelfServiceInitiation(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/register/self_service/initiation';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student, Self-Service] Completes the self-service student registration process (e.g. Step 2 and 3), including submitting payment
	 * @param {StudentRegisterSelfServiceCompletionRequest} request
	 * @param {{status200: function(string), status402: function(string), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	registerSelfServiceCompletion(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/register/self_service/completion';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 402:
					if (responseHandler.status402) {
						response.text()
							.then(responseText => {
								responseHandler.status402(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student, Campus-Affiliated] Initiates a campus-affiliated student registration process (e.g. Step 1)
	 * @param {StudentRegisterCampusAffiliatedInitiationRequest} request
	 * @param {{status200: function(Person), status404: function(string), status409: function(School[]), status410: function(string), status420: function(string), status425: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	registerCampusAffiliatedInitiation(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/register/campus_affiliated/initiation';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Person.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.json()
							.then(responseJson => {
								responseHandler.status409(School.createArray(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 410:
					if (responseHandler.status410) {
						response.text()
							.then(responseText => {
								responseHandler.status410(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 420:
					if (responseHandler.status420) {
						response.text()
							.then(responseText => {
								responseHandler.status420(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 425:
					if (responseHandler.status425) {
						response.text()
							.then(responseText => {
								responseHandler.status425(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student, Campus-Affiliated] Completes the campus-affiliated student registration process (e.g. Step 2 and 3).  If successful, this will log the user in and return the session object.
	 * @param {StudentRegisterCampusAffiliatedCompletionRequest} request
	 * @param {{status200: function(Session), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	registerCampusAffiliatedCompletion(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/register/campus_affiliated/completion';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Session.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] to search for list of Students
	 * @param {StudentSearchRequest} request
	 * @param {{status200: function(StudentSearchResponse), error: function(error), else: function(integer, string)}} responseHandler
	 */
	search(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/search';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(StudentSearchResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] to search for list of Students (CSV Version)
	 * @param {string} sessionToken
	 * @param {string} studentSearchRequest
	 * @param {string} filename
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	searchCsv(sessionToken, studentSearchRequest, filename, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/search/' +
			(sessionToken ? encodeURI(sessionToken) : '') + '/' +
			(studentSearchRequest ? encodeURI(studentSearchRequest) : '') + '/' +
			(filename ? encodeURI(filename) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] creates a new, campus-affiliated Student.  Fields that are applicable: firstName, lastName, dob, gender, ssnLast4, schoolStudentIdentifier, dateStart, email, address1, address2, city, state, zip, phone.  Note that schoolId and campusId are required.
	 * @param {Person} request
	 * @param {{status200: function(Person), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	create(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/create';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Person.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Edits an existing Student.  id MUST be set.  Only set fields that are to be updated.  Fields that are updatable by anyone: firstName, lastName, dob, gender, ssnLast4, address1, address2, city, state, zip, email, phone.  Additional fields that are updatable by C4L or Institution: startDate, schoolIdentifier, preferences.note.  Additional fields that are updatable by C4L Only: schoolId, campusId.
	 * @param {Person} request
	 * @param {{status200: function(Person), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	edit(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/edit';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Person.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * the student has successfully dismissed the current takeover announcement
	 * @param {string} announcementIdentifier
	 * @param {{status200: function(Session), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	dismissAnnouncement(announcementIdentifier, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/takeover_announcement/dismiss/' +
			(announcementIdentifier ? encodeURI(announcementIdentifier) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Session.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Retrieves the Course Progress and Subject Time Tracking for all courses and subjects for the student.  Students cna retrieve for themselves.  Institution users can retrieve for any student within their institution.  C4L users can retrieve for any student.
	 * @param {string} personId
	 * @param {{status200: function(StudentCourseProgressResponse), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getCourseProgress(personId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/course_progress/' +
			(personId ? encodeURI(personId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(StudentCourseProgressResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student] Registers the student for a course -- either to take it for the first time, or to retake it (if they have failed previously)
	 * @param {string} courseId
	 * @param {string} retakeFlag
	 * @param {{status200: function(string), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	registerCourse(courseId, retakeFlag, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/course/register/' +
			(courseId ? encodeURI(courseId) : '') + '/' +
			(retakeFlag ? encodeURI(retakeFlag) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student] Gets the course information as well as all of the course progress information
	 * @param {string} courseId
	 * @param {{status200: function(StudentGetCourseWithProgressResponse), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getCourseWithCourseProgress(courseId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/course/get/' +
			(courseId ? encodeURI(courseId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(StudentGetCourseWithProgressResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student] Gets the section the student wants to currently view.  Will log the date/time when the student is viewing.
	 * @param {string} sectionId
	 * @param {{status200: function(Section), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	viewCourseSection(sectionId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/course/view_section/' +
			(sectionId ? encodeURI(sectionId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Section.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] to search for Exams or Quizzes for a Student.  studentId is required.
	 * @param {StudentSearchAssessmentRequest} request
	 * @param {{status200: function(StudentSearchAssessmentResponse), error: function(error), else: function(integer, string)}} responseHandler
	 */
	searchAssessments(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/assessment/search';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(StudentSearchAssessmentResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] to search for Exams or Quizzes for a Student.  studentId is required. (CSV Version)
	 * @param {string} sessionToken
	 * @param {string} studentSearchAssessmentRequest
	 * @param {string} filename
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	searchAssessmentsCsv(sessionToken, studentSearchAssessmentRequest, filename, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/assessment/search/' +
			(sessionToken ? encodeURI(sessionToken) : '') + '/' +
			(studentSearchAssessmentRequest ? encodeURI(studentSearchAssessmentRequest) : '') + '/' +
			(filename ? encodeURI(filename) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student] If Exam: must either not have been taken yet, or must be an unlocked Challenge or Final exam.  If Quiz: must not have been completed yet (can be not-yet-created, or created but not yet completed).
	 * @param {StudentAssessmentCreateRequest} request
	 * @param {{status200: function(Assessment), status403: function(string), status404: function(string), status409: function(string), status410: function(Assessment), error: function(error), else: function(integer, string)}} responseHandler
	 */
	createAssessment(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/assessment/create';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Assessment.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 410:
					if (responseHandler.status410) {
						response.json()
							.then(responseJson => {
								responseHandler.status410(Assessment.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student] sets the answer to a question within the Quiz or Exam that is currently being taken, but not yet completed.  assessmentId and questionId and elapsedTime are required.  response is required for Written quizzes, answerId is required for everything else.  for elapsedTime, provide the TOTAL elapsed time.  So for example, if the student spend 10 seconds to answer the question.  But then later on during the same exam, the student went back to the question and spent another 10 seconds to re-answer the question, elapsedTime should then be 20
	 * @param {StudentAnswer} request
	 * @param {{status200: function(string), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	setAssessmentStudentAnswer(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/assessment/student_answer/set';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student] gets the answer (if already answered) to a question within the Quiz or Exam that is currently being taken, but not yet completed.
	 * @param {string} assessmentId
	 * @param {string} questionId
	 * @param {{status200: function(StudentAnswer), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getAssessmentStudentAnswer(assessmentId, questionId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/assessment/student_answer/get/' +
			(assessmentId ? encodeURI(assessmentId) : '') + '/' +
			(questionId ? encodeURI(questionId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(StudentAnswer.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student] gets the summary/array of all answered questions within the Quiz or Exam that is currently being taken, but not yet completed.
	 * @param {string} assessmentId
	 * @param {{status200: function(StudentAnswer[]), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getAssessmentStudentAnswerArray(assessmentId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/assessment/student_answer/get_summary/' +
			(assessmentId ? encodeURI(assessmentId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(StudentAnswer.createArray(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student] marks the Quiz or Exam that is currently being taken as being completed.
	 * @param {string} assessmentId
	 * @param {{status200: function(Assessment), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	completeAssessment(assessmentId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/assessment/complete/' +
			(assessmentId ? encodeURI(assessmentId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Assessment.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student] marks the Exam that is currently being taken as being completed via a timeout (time expired).
	 * @param {string} assessmentId
	 * @param {{status200: function(Assessment), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	timeoutExamAssessment(assessmentId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/assessment/timeout_exam/' +
			(assessmentId ? encodeURI(assessmentId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Assessment.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Retrieves the Assessment, including the rendered structure and all of the answers and results of each answer.  If user is a Student, can only be called on Exams that have been completed.
	 * @param {string} assessmentId
	 * @param {{status200: function(Assessment), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getAssessment(assessmentId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/assessment/get/' +
			(assessmentId ? encodeURI(assessmentId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Assessment.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Gets the list of Transfer Transcripts for this student.
	 * @param {string} personId
	 * @param {{status200: function(TransferTranscript[]), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getTransferTranscripts(personId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/transfer_transcripts/' +
			(personId ? encodeURI(personId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(TransferTranscript.createArray(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Creates a new Transfer Transcript entry.  studentId, fileAsset.uploaded64Data, schoolName are required.  Notes and Verified are optional.  If credits are to be applied, make a subsequent call to Edit to apply credits.
	 * @param {TransferTranscript} request
	 * @param {{status200: function(TransferTranscript), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	transferTranscriptCreate(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/transfer_transcript/create';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(TransferTranscript.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] Edits an existing Transfer Transcript.  Edits an existing TransferTranscript.  id MUST be set.  verifiedFlag, schoolName, notes and appliedCredits can be updated.
	 * @param {TransferTranscript} request
	 * @param {{status200: function(TransferTranscript), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	transferTranscriptEdit(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/transfer_transcript/edit';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(TransferTranscript.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] Deletes a Transfer Transcript
	 * @param {string} transferTranscriptId
	 * @param {{status200: function(string), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	transferTranscriptDelete(transferTranscriptId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/transfer_transcript/delete/' +
			(transferTranscriptId ? encodeURI(transferTranscriptId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'delete')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] Allows toggling of the 'Of Interest' for this student.
	 * @param {string} personId
	 * @param {string} flag
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	toggleOfInterestFlag(personId, flag, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/toggle_of_interest/' +
			(personId ? encodeURI(personId) : '') + '/' +
			(flag ? encodeURI(flag) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L or Student] Allows for cancelling of a subscription -- only applicable to self-service students.  If C4L cancels, the student account is immediately set to Expired. If Student cancels, the subscription is cancelled, but the student account will continue to be Active until the end of the current pay period (after that point, it will be set to Expired).
	 * @param {string} personId
	 * @param {{status200: function(Session), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	subscriptionCancel(personId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/subscription/cancel/' +
			(personId ? encodeURI(personId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Session.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student, Self-Service] Updates the payment for the student.  If student is currently expired, this will charge right away.  Otherwise, if student is currently Active, then this will simply update the payment method for this student subscriber.  (If the subscription is cancelled but the student is still Active, this will undo the cancellation).
	 * @param {PaymentRequest} request
	 * @param {{status200: function(Session), status402: function(string), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	subscriptionUpdatePayment(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/subscription/payment';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Session.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 402:
					if (responseHandler.status402) {
						response.text()
							.then(responseText => {
								responseHandler.status402(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] Allows for cancelling of a Campus-affiliated student's account -- only applicable to campus-affiliated students.  If C4L cancels, the student account is immediately set to Cancelled.
	 * @param {string} personId
	 * @param {{status200: function(string), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	campusStudentCancel(personId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/campus/cancel/' +
			(personId ? encodeURI(personId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] Allows for reinstating cancelled Campus-affiliated students -- only applicable to campus-affiliated students.  Regardless if student is cancelled explicitly or due to 60-day expiration, the student account is immediately reinstated.
	 * @param {string} personId
	 * @param {{status200: function(string), status403: function(string), status404: function(string), status409: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	campusStudentReinstate(personId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/student/campus/reinstate/' +
			(personId ? encodeURI(personId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 409:
					if (responseHandler.status409) {
						response.text()
							.then(responseText => {
								responseHandler.status409(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

}
