import ClientBaseClass from "./ClientBaseClass";
import Note from "../../models/Note";

export default class NoteApi extends ClientBaseClass {
	/**
	 * Gets the notes/audit log for a given entity
	 * @param {'Person'|'Subject'|'Course'|'Question'|'Announcement'|'Issue'} entity
	 * @param {string} entityId
	 * @param {{status200: function(Note[]), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	get(entity, entityId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/note/get/' +
			(entity ? encodeURI(entity) : '') + '/' +
			(entityId ? encodeURI(entityId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Note.createArray(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Creates a New Freeform Note. Must set ONLY one of (PersonEntity, SubjectEntity, CourseEntity, QuestionEntity, AnnouncementEntity or IssueEntity). Content must be set. Latitude, Longitude, FileAsset and ActionToken are all optional. Any other fields are ignored.
	 * @param {Note} request
	 * @param {{status200: function(Note), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	create(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/note/create';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Note.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

}
