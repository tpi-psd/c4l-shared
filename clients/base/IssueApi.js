import ClientBaseClass from "./ClientBaseClass";
import Issue from "../../models/Issue";
import IssueSearchResponse from "../../models/IssueSearchResponse";

export default class IssueApi extends ClientBaseClass {
	/**
	 * [C4L / Institution] creates a new Issue.  Fields that are required: schoolId, campusId, issueCategoryId, content.  student.id is also required for student issues.
	 * @param {Issue} request
	 * @param {{status200: function(Issue), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	create(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/issue/create';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Issue.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] updates an existing issue.  All fields required.
	 * @param {IssueEditRequest} request
	 * @param {{status200: function(Issue), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	edit(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/issue/edit';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Issue.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] to search for list of Issues
	 * @param {IssueSearchRequest} request
	 * @param {{status200: function(IssueSearchResponse), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	search(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/issue/search';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(IssueSearchResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] Gets an Issue record
	 * @param {string} issueId
	 * @param {{status200: function(Issue), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	get(issueId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/issue/get/' +
			(issueId ? encodeURI(issueId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Issue.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

}
