import ClientBaseClass from "./ClientBaseClass";
import SystemBadges from "../../models/SystemBadges";
import Announcement from "../../models/Announcement";
import SystemSettings from "../../models/SystemSettings";
import StripeTokenResponse from "../../models/StripeTokenResponse";

export default class UtilityApi extends ClientBaseClass {
	/**
	 * [C4L / Institution] Gets the Badges to display in the Admin Navbar
	 * @param {{status200: function(SystemBadges), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getAdminNavbarBadges(responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/utility/admin_badges';

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(SystemBadges.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] Gets the current Announcement
	 * @param {{status200: function(Announcement), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getAnnouncement(responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/utility/announcement/get';

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Announcement.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] Updates the current Announcement.  Title, linkUrl, activeFlag, dateStart, dateEnd and body.
	 * @param {Announcement} request
	 * @param {{status200: function(Announcement), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	updateAnnouncement(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/utility/announcement/edit';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Announcement.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L / Institution] Gets the Settings for the entire system
	 * @param {{status200: function(SystemSettings), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getSystemSettingsForAdminSite(responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/utility/system_settings/admin';

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(SystemSettings.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [Student] Gets the Settings for the entire system
	 * @param {{status200: function(SystemSettings), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getSystemSettingsForStudentSite(responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/utility/system_settings/student';

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(SystemSettings.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * Returns the Stripe publishable key
	 * @param {{status200: function(StripeTokenResponse), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	stripeToken(responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/utility/stripe_token';

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(StripeTokenResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

}
