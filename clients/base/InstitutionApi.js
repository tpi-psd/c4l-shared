import ClientBaseClass from "./ClientBaseClass";
import InstitutionSchoolSearchResponse from "../../models/InstitutionSchoolSearchResponse";
import SystemSettings from "../../models/SystemSettings";
import InstitutionCampusSearchResponse from "../../models/InstitutionCampusSearchResponse";

export default class InstitutionApi extends ClientBaseClass {
	/**
	 * [C4L] to search for list of Schools
	 * @param {InstitutionSchoolSearchRequest} request
	 * @param {{status200: function(InstitutionSchoolSearchResponse), error: function(error), else: function(integer, string)}} responseHandler
	 */
	searchSchools(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/institution/school/search';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(InstitutionSchoolSearchResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] creates a new School.  Fields that are applicable: name, address1, address2, city, state, zip, phone, preferences.monthlyBillingType, and depending on the billing type, firstHalfDays and secondHalfDays or fullDays.
	 * @param {School} request
	 * @param {{status200: function(SystemSettings), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	createSchool(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/institution/school/create';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(SystemSettings.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Edits an existing School.  id MUST be set.  Only set fields that are to be updated.  Fields that are applicable: name, address1, address2, city, state, zip, phone, preferences.monthlyBillingType, and depending on the billing type, firstHalfDays and secondHalfDays or fullDays.
	 * @param {School} request
	 * @param {{status200: function(SystemSettings), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	editSchool(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/institution/school/edit';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(SystemSettings.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Activates/Deactivates a School
	 * @param {string} schoolId
	 * @param {string} flag
	 * @param {{status200: function(SystemSettings), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	setActivationSchool(schoolId, flag, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/institution/school/activation/' +
			(schoolId ? encodeURI(schoolId) : '') + '/' +
			(flag ? encodeURI(flag) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(SystemSettings.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L] to search for list of Campuses
	 * @param {InstitutionCampusSearchRequest} request
	 * @param {{status200: function(InstitutionCampusSearchResponse), error: function(error), else: function(integer, string)}} responseHandler
	 */
	searchCampuses(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/institution/campus/search';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(InstitutionCampusSearchResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] creates a new Campus.  Fields that are applicable: schoolId, name, address1, address2, city, state, zip, phone.
	 * @param {Campus} request
	 * @param {{status200: function(SystemSettings), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	createCampus(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/institution/campus/create';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(SystemSettings.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Edits an existing Campus.  id MUST be set.  Only set fields that are to be updated.  Fields that are applicable: name, address1, address2, city, state, zip, phone.
	 * @param {Campus} request
	 * @param {{status200: function(SystemSettings), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	editCampus(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/institution/campus/edit';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(SystemSettings.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Activates/Deactivates a Campus
	 * @param {string} campusId
	 * @param {string} flag
	 * @param {{status200: function(SystemSettings), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	setActivationCampus(campusId, flag, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/institution/campus/activation/' +
			(campusId ? encodeURI(campusId) : '') + '/' +
			(flag ? encodeURI(flag) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(SystemSettings.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

}
