import PersonApi from "./PersonApi";
import StudentApi from "./StudentApi";
import InstitutionApi from "./InstitutionApi";
import IssueApi from "./IssueApi";
import TranscriptRequestApi from "./TranscriptRequestApi";
import WrittenQuizAnswerApi from "./WrittenQuizAnswerApi";
import AutoGradeLockoutApi from "./AutoGradeLockoutApi";
import CurriculumApi from "./CurriculumApi";
import UtilityApi from "./UtilityApi";
import FileAssetApi from "./FileAssetApi";
import NoteApi from "./NoteApi";

/**
 * Use globally to access any of the API Client Methods for the WebService
 */
export default class Client {
}

/**
 * Use in a responseHandler if you want to ignore a given/specific response
 */
export function ignoreResponse() {
}

Client.PersonApi = new PersonApi();
Client.StudentApi = new StudentApi();
Client.InstitutionApi = new InstitutionApi();
Client.IssueApi = new IssueApi();
Client.TranscriptRequestApi = new TranscriptRequestApi();
Client.WrittenQuizAnswerApi = new WrittenQuizAnswerApi();
Client.AutoGradeLockoutApi = new AutoGradeLockoutApi();
Client.CurriculumApi = new CurriculumApi();
Client.UtilityApi = new UtilityApi();
Client.FileAssetApi = new FileAssetApi();
Client.NoteApi = new NoteApi();
