import ClientBaseClass from "./ClientBaseClass";
import SystemSettings from "../../models/SystemSettings";
import Chapter from "../../models/Chapter";
import CurriculumQuestionSearchResponse from "../../models/CurriculumQuestionSearchResponse";
import Question from "../../models/Question";
import Section from "../../models/Section";

export default class CurriculumApi extends ClientBaseClass {
	/**
	 * [C4L Manager] creates a new or edits an existing subject.  id must be set to edit.  If id is null, then a new subject will be created.  Applicable fields are name, activeFlag, and requirements.  Within each requirement, if id is set, the requirement will be updated.  If id is not set, the requirement will be created.  Applicable requirement fields are name, orderNumber and courseIdArray.
	 * @param {Subject} request
	 * @param {{status200: function(SystemSettings), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	saveSubject(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/subject';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(SystemSettings.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] reorders the subjects.  For each subject in the request array, id and orderNumber must be set.  All other fields are ignored.
	 * @param {[Subject]} request
	 * @param {{status200: function(SystemSettings), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	reorderSubjects(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/subject/reorder';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(SystemSettings.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] creates a new or edits an existing course.  id must be set to edit.  If id is null, then a new course will be created.  Applicable fields are name, subjectId, activeFlag.
	 * @param {Course} request
	 * @param {{status200: function(SystemSettings), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	saveCourse(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(SystemSettings.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Gets a list of all chapters for a given Course
	 * @param {string} courseId
	 * @param {string} includeInactiveFlag
	 * @param {{status200: function(Chapter[]), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getCourseChapters(courseId, includeInactiveFlag, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapters/' +
			(courseId ? encodeURI(courseId) : '') + '/' +
			(includeInactiveFlag ? encodeURI(includeInactiveFlag) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Chapter.createArray(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] creates a new Chapter.  courseId is required, name, quizType, excludedFlag, activeFlag are required.  All other fields are ignored
	 * @param {Chapter} request
	 * @param {{status200: function(Chapter), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	courseChapterCreate(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/create';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Chapter.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Edits an existing Chapter.  id MUST be set.  Only set fields that are to be updated.  Fields that are applicable: name, activeFlag, excludedFlag
	 * @param {Chapter} request
	 * @param {{status200: function(Chapter), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	courseChapterEdit(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/edit';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Chapter.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Deletes a Chapter
	 * @param {string} chapterId
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	deleteChapter(chapterId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/delete/' +
			(chapterId ? encodeURI(chapterId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'delete')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Gets a Chapter
	 * @param {string} chapterId
	 * @param {{status200: function(Chapter), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getChapter(chapterId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/get/' +
			(chapterId ? encodeURI(chapterId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Chapter.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] reorders the chapters within a course.  For each chapter in the request array, id and orderNumber must be set.  All other fields are ignored.
	 * @param {string} courseId
	 * @param {[Chapter]} request
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	reorderCourseChapters(courseId, request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/reorder/' +
			(courseId ? encodeURI(courseId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] updates the exam blueprint information for the chapters within a course.  For each chapter in the request array, id and finalExamBlueprint and challengeExamBlueprint must be set.  All other fields are ignored.
	 * @param {string} courseId
	 * @param {[Chapter]} request
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	saveCourseChaptersExamBlueprint(courseId, request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/exam_blueprint/' +
			(courseId ? encodeURI(courseId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] to search for list of Questions for a chapter.  chapterId is required.
	 * @param {CurriculumQuestionSearchRequest} request
	 * @param {{status200: function(CurriculumQuestionSearchResponse), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	questionSearch(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/question/search';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(CurriculumQuestionSearchResponse.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] to search for list of Questions for a chapter.  chapterId is required. (CSV Version)
	 * @param {string} sessionToken
	 * @param {string} curriculumQuestionSearchRequest
	 * @param {string} filename
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	questionSearchCsv(sessionToken, curriculumQuestionSearchRequest, filename, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/question/search/' +
			(sessionToken ? encodeURI(sessionToken) : '') + '/' +
			(curriculumQuestionSearchRequest ? encodeURI(curriculumQuestionSearchRequest) : '') + '/' +
			(filename ? encodeURI(filename) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Gets a question by ID
	 * @param {string} questionId
	 * @param {{status200: function(Question), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getQuestion(questionId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/question/get/' +
			(questionId ? encodeURI(questionId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Question.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] creates a new or edits an existing question.  If editing, id must be set.  If creating new, id must be null and chapterId is required.  Applicable fields for all quiz types are sectionId, content, activeFlag.  Additional fields for Standard and Video quiz types are correctAnswer and incorrectAnswers.  For correctAnswer, only content is applicable.  For incorrectAnswer: content and orderNumber are applicable.  If editing, id is required.  If creating new, id is null.  If deleting, do not include it in the incorrectAnswer array.
	 * @param {CurriculumQuestionSaveRequest} request
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	saveQuestion(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/question';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Deletes a Question
	 * @param {string} questionId
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	deleteQuestion(questionId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/question/delete/' +
			(questionId ? encodeURI(questionId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'delete')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Gets a list of all sections for a given Chapter
	 * @param {string} chapterId
	 * @param {string} includeInactiveFlag
	 * @param {{status200: function(Section[]), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getCourseChapterSections(chapterId, includeInactiveFlag, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/sections/' +
			(chapterId ? encodeURI(chapterId) : '') + '/' +
			(includeInactiveFlag ? encodeURI(includeInactiveFlag) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Section.createArray(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] creates a new Section.  chapterId is require, name, content, activeFlag are required.  saveNote is optional.  All other fields are ignored
	 * @param {Section} request
	 * @param {{status200: function(Section), status403: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	courseChapterSectionCreate(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/section/create';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Section.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Edits an existing Section.  id and saveNote MUST be set.  Only set fields that are to be updated.  Fields that are applicable: name, activeFlag, content.
	 * @param {Section} request
	 * @param {{status200: function(Section), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	courseChapterSectionEdit(request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/section/edit';

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Section.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Gets a Section
	 * @param {string} sectionId
	 * @param {{status200: function(Section), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	getSection(sectionId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/section/get/' +
			(sectionId ? encodeURI(sectionId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'get')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.json()
							.then(responseJson => {
								responseHandler.status200(Section.create(responseJson));
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] Deletes a Section
	 * @param {string} sectionId
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	deleteSection(sectionId, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/section/delete/' +
			(sectionId ? encodeURI(sectionId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'delete')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

	/**
	 * [C4L Manager] reorders the sections within a chapter.  For each section in the request array, id and orderNumber must be set.  All other fields are ignored.
	 * @param {string} chapterId
	 * @param {[Section]} request
	 * @param {{status200: function(string), status403: function(string), status404: function(string), error: function(error), else: function(integer, string)}} responseHandler
	 */
	reorderSections(chapterId, request, responseHandler) {
		responseHandler = this.generateResponseHandler(responseHandler);

		const url = '/curriculum/course/chapter/section/reorder/' +
			(chapterId ? encodeURI(chapterId) : '');

		// noinspection Duplicates
		this.executeApiCall(url, 'post', request, 'json')
			.then(response => {
				switch (response.status) {
				case 200:
					if (responseHandler.status200) {
						response.text()
							.then(responseText => {
								responseHandler.status200(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 403:
					if (responseHandler.status403) {
						response.text()
							.then(responseText => {
								responseHandler.status403(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				case 404:
					if (responseHandler.status404) {
						response.text()
							.then(responseText => {
								responseHandler.status404(responseText);
							})
							.catch(responseHandler.error);
						return;
					}
					break;
				}

				// If we are here, we basically have a response statusCode that we were npt expecting or are not set to handle
				// Go ahead and fall back to the catch-all
				this.handleUnhandledResponse(response, responseHandler);
			})
			.catch(error => {
				responseHandler.error(error);
			});
	}

}
