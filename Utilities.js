/* eslint-disable no-prototype-builtins */
import React from 'react';
import FileAsset from './models/FileAsset';
import {
	AiFillFileExcel,
	AiFillFilePpt,
	AiFillFileWord,
	AiFillFileImage,
	AiFillFilePdf,
	AiFillFileText,
	AiFillFile
} from 'react-icons/ai';
import moment from 'moment';
import ClientMiddleware from './clients/ClientMiddleware';
import { Buffer } from 'buffer';

/**
 * Get Random Id Number
 * @returns {string}
 */
export const randomId = () => {
	let id = '';
	let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	for (var i = 0; i < 12; i++) {
		id += characters.charAt(Math.floor(Math.random() * 36));
	}
	return id;
};

/**
 * @param {File} file
 * @returns {FileAsset}
 */
export const getFileAsset = file =>
	new Promise((resolve, reject) => {
		const fileAsset = new FileAsset();
		// setup
		const reader = new FileReader();
		reader.onload = () => {
			fileAsset.uploadBase64Data = reader.result.substring(reader.result.indexOf(',') + 1);
			fileAsset.filename = file.name;
			fileAsset.mimeType = file.type;
			resolve(fileAsset);
		};
		reader.onerror = error => reject(error);

		// Go
		reader.readAsDataURL(file);
	});

export const capitalize = string => {
	if (typeof string !== 'string') return '';
	return string.charAt(0).toUpperCase() + string.slice(1);
};

/**
 * @param {Date} date
 * @returns {string}
 */
export const getDateInputString = date => {
	if (!date) return '';
	return moment(date).format('YYYY-MM-DD');
};

/**
 * @param {string} mimeType
 */
export const getDocTypeIcon = mimeType => {
	// text
	if (mimeType === 'text/plain') return <AiFillFileText />;
	// pdf
	if (mimeType === 'application/pdf') return <AiFillFilePdf />;
	// images
	if (mimeType.startsWith('image')) return <AiFillFileImage />;
	// ppt
	if (
		mimeType === 'application/vnd.ms-powerpoint' ||
		mimeType === 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
	)
		return <AiFillFilePpt />;
	// excel
	if (
		mimeType === 'application/vnd.ms-excel' ||
		mimeType === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
	)
		return <AiFillFileExcel />;
	// word
	if (
		mimeType === 'application/msword' ||
		mimeType === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	)
		return <AiFillFileWord />;
	return <AiFillFile />;
};

/**
 * @param {string | number} number
 */
export const numberWithCommas = number => {
	const x = `${number}`;
	const parts = x.split('.');
	const reg = new RegExp('\\B(?=(\\d{3})+(?!\\d))', 'g');
	parts[0] = parts[0].replace(reg, ',');
	return parts.join('.');
};

export const deepClone = inObject => {
	let outObject, value, key;
	if (typeof inObject !== 'object' || inObject === null) {
		return inObject;
	} else if (inObject instanceof Date) return inObject;
	outObject = Array.isArray(inObject) ? [] : {};
	for (key in inObject) {
		value = inObject[key];
		outObject[key] = deepClone(value);
	}
	return outObject;
};

export const downloadFile = (url, fileName) => {
	if (!url) return;
	try {
		const link = document.createElement('a');
		link.href = ClientMiddleware.getEndpointUrl() + url;
		link.setAttribute('download', fileName);
		document.body.appendChild(link);
		link.click();
		link.parentNode.removeChild(link);
	} catch (error) {
		console.error(error);
	}
};

export const sortAscendingOrder = (a, b) => a.orderNumber - b.orderNumber;

/**
 * @param {Array} array
 * @param {string} key
 * @returns {object}
 */
export const arrayToOject = (array, key) => {
	return (
		array?.reduce(
			(a, v) => ({
				...a,
				[v[key]]: v
			}),
			{}
		) || {}
	);
};

export const sentenceCaseFromCamelCase = camelCase => {
	if (!camelCase) return '';
	const result = camelCase.replace(/([A-Z]|\d+)/g, ' $1');
	return result.trim();
};

export const openNewTab = url => {
	window.open(url, '_blank').focus();
};

/**
 * @param {number} number
 * @param {number} decimal
 * @returns {string}
 */
export const displayDecimalNumber = (number, decimal) => {
	if (!decimal || decimal === 0) return `${Math.round(number)}`;
	try {
		return parseFloat(+number.toFixed(decimal));
	} catch (error) {
		return `${number}`;
	}
};

/**
 * @param {Date | string} date
 * @returns {string}
 */
export const displayDate = date => {
	return moment(date).format('MMM D[,] YYYY');
};

/**
 * @param {Date} date
 * @return {string}
 */
export const displayDateTime = date => {
	if (!date) return '';
	return (
		date.toLocaleDateString() +
		' ' +
		date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })
	);
};

export const getObjectDifference = (obj1, obj2, withPreviousValue) => {
	// Make sure an object to compare is provided
	if (!obj2 || Object.prototype.toString.call(obj2) !== '[object Object]') {
		return obj1;
	}

	// Variables
	var diffs = {};
	var key;

	// Methods
	/**
	 * Check if two arrays are equal
	 * @param  {Array}   arr1 The first array
	 * @param  {Array}   arr2 The second array
	 * @return {Boolean}      If true, both arrays are equal
	 */
	var arraysMatch = function(arr1, arr2) {
		// Check if the arrays are the same length
		if (arr1.length !== arr2.length) return false;

		// Check if all items exist and are in the same order
		for (var i = 0; i < arr1.length; i++) {
			if (arr1[i] !== arr2[i]) return false;
		}

		// Otherwise, return true
		return true;
	};

	/**
	 * Compare two items and push non-matches to object
	 * @param  {*}      item1 The first item
	 * @param  {*}      item2 The second item
	 * @param  {String} key   The key in our object
	 */
	var compare = function(item1, item2, key) {
		// Get the object type
		var type1 = Object.prototype.toString.call(item1);
		var type2 = Object.prototype.toString.call(item2);

		// If type1 and type2 is undefined, do not log
		if (type1 === '[object Undefined]' || type2 === '[object Undefined]') {
			return;
		}

		// If items are different types
		if (type1 !== type2) {
			diffs[key] = item1;
			return;
		}

		// If an object, compare recursively
		if (type1 === '[object Object]') {
			var objDiff = getObjectDifference(item1, item2, withPreviousValue);
			if (Object.keys(objDiff).length > 0) {
				for (let objKey in objDiff) {
					diffs[objKey] = objDiff[objKey];
				}
			}
			return;
		}

		// If an array, compare
		if (type1 === '[object Array]') {
			if (!arraysMatch(item1, item2)) {
				diffs[key] = item1;
			}
			return;
		}

		// Else if it's a function, convert to a string and compare
		// Otherwise, just compare
		if (type1 === '[object Function]') {
			if (item1.toString() !== item2.toString()) {
				diffs[key] = item1;
			}
		} else {
			if (item1 !== item2) {
				diffs[key] = `${withPreviousValue ? `${item2} > ` : ''}${item1}`;
			}
		}
	};

	// Compare our objects
	// Loop through the first object
	for (key in obj1) {
		if (obj1.hasOwnProperty(key)) {
			compare(obj1[key], obj2[key], key);
		}
	}

	// Loop through the second object and find missing items
	// for (key in obj2) {
	// 	if (obj2.hasOwnProperty(key)) {
	// 		if (!obj1[key] && obj1[key] !== obj2[key]) {
	// 			diffs[key] = obj2[key];
	// 		}
	// 	}
	// }

	return diffs;
};

export const camelCaseSplitBy = (str, seperator) => {
	if (!str) return '';

	if (seperator)
		return str
			.split(seperator)
			.map(x => x.charAt(0).toUpperCase() + x.slice(1))
			.join(' ');
	else return str.charAt(0).toUpperCase() + str.slice(1);
};

export const base64ToString = base64 => {
	if (!base64) return;

	try {
		// URL-safe Base64 Decode
		base64 = base64.replace('-', '+').replace('_', '/');
		return new Buffer.from(base64, 'base64').toString('ascii');
	} catch (error) {
		console.error(error);
		return '';
	}
};
